#!/bin/bash
#################################
# Author: Eunchong Kim, Arisa Kubota
# Email: eunchong.kim at cern.ch, arisa.kubota at cern.ch
# Date: Oct 2019
# Project: Local Database for YARR
# Description: Setup archive tool for local use
#################################
set -e

ITSNAME="[LDB]"

# Usage
function usage {
    cat <<EOF

Usage:
    ./setup_archive_tool.sh

EOF
}

if [ `echo ${0} | grep bash` ]; then
    echo -e "${ITSNAME} DO NOT 'source'"
    usage
    return
fi

TOOLS_DIR=$(cd $(dirname $0); pwd)

# Start
echo -e "$ITSNAME Welcome!"
echo -e "$ITSNAME"

# Copy bin
cp -r ${TOOLS_DIR}/src/bin ${TOOLS_DIR}/
chmod +x ${TOOLS_DIR}/bin/*

# Set editor command
if [ -z "$EDITOR" ]; then
    read -p "$ITSNAME Set editor command ... > " ANSWER
    while [ -z $ANSWER ];
    do
        read -p "$ITSNAME Set editor command ... > " ANSWER
    done
    EDITOR=$ANSWER
fi
echo -e "$ITSNAME"

# Copy yml configure
if [ ! -f ${TOOLS_DIR}/my_archive_configure.yml ]; then
    cp ${TOOLS_DIR}/src/etc/localdbtools/archive.yml ${TOOLS_DIR}/my_archive_configure.yml
fi
$EDITOR ${TOOLS_DIR}/my_archive_configure.yml

# Enable bash completion
source src/share/bash-completion/completions/localdbtool-archive
complete -F _localdbtool_archive ./bin/localdbtool-archive.sh

echo -e "$ITSNAME"
echo -e "$ITSNAME Finish!"
echo -e "$ITSNAME"
echo -e "$ITSNAME Enable bash completion by ..."
echo -e "$ITSNAME   source ${TOOLS_DIR}/src/share/bash-completion/completions/localdbtool-archive"
echo -e "$ITSNAME"
echo -e "$ITSNAME Start to archive by"
echo -e "$ITSNAME   cd ${TOOLS_DIR}"
echo -e "$ITSNAME   ./bin/localdbtool-archive.sh -f my_archive_configure.yml"
echo -e "$ITSNAME"
echo -e "$ITSNAME More information: https://localdb-docs.readthedocs.io/en/master/"
echo -e ""
