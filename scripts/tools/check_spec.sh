#!/bin/bash

db_port=27017

# cpu
cat /proc/cpuinfo > spec.log
echo " " >> spec.log

# memory
cat /proc/meminfo >> spec.log
echo " " >> spec.log

# disk
df -h >> spec.log
echo " " >> spec.log
lsblk -o name,rota >> spec.log
echo " " >> spec.log

# MongoDB
mongo --port ${db_port} <<EOF >> spec.log
use localdb
db.printCollectionStats()
EOF
echo " " >> spec.log

# tar speed
test_file="spec-test-file"
tar_file="spec-test-file.tar.gz"
test_size=1000 #1GB
`dd if=/dev/zero of=${test_file} bs=1M count=${test_size} &> /dev/null`
SECONDS=0
TIME="`date +"%Y-%m-%d %H:%M:%S"`"
message="ARCHIVE FAILED!"
tar zcvf ${tar_file} ${test_file} \
    && (echo $? && echo -e "$TIME, succeed : $SECONDS s" >> spec.log )\
    || (echo -e "$RED $message_type $NC $message" \
        && echo -e "$TIME, $message_type $message : $SECONDS s" >> spec.log \
        && exit 1
    )
echo " " >> spec.log
rm ${test_file}
rm ${tar_file}
