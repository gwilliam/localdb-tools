#!/usr/bin/env python3
#################################
# Author: Arisa Kubota
# Email: arisa.kubota at cern.ch
# Date: July 2019
# Project: Local Database for YARR
#################################

### Common
import os, sys, argparse, yaml, gridfs, time
from getpass import getpass
from pymongo import MongoClient, errors, DESCENDING
from bson.objectid import ObjectId
from datetime import datetime

### log
import logging
from logging import (
    getLogger,
    StreamHandler,
    DEBUG,
    Formatter,
    FileHandler,
    getLoggerClass,
    INFO,
    CRITICAL,
)

logfile = "speedtest.log"
datafile = "speedtest.dat"
formatter = Formatter("%(asctime)s - %(levelname)s: %(message)s")

logger = getLogger("Log")
logger.setLevel(INFO)

handler1 = StreamHandler()
handler1.setLevel(INFO)
handler1.setFormatter(formatter)

handler2 = FileHandler(filename=logfile)
handler2.setLevel(CRITICAL)
handler2.setFormatter(formatter)

logdata = getLogger("Data")
formatter = Formatter("%(message)s")
handler3 = FileHandler(filename=datafile)
handler3.setLevel(CRITICAL)
handler3.setFormatter(formatter)
logdata.addHandler(handler3)

logger.addHandler(handler1)
logger.addHandler(handler2)

### global veriable
global testdb

#######################
### local functions ###
#######################

###############
# get arguments
def getArgs():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--host",
        help="Set server to connect to (default 127.0.0.1)",
        type=str,
        default="127.0.0.1",
    )
    parser.add_argument(
        "--port",
        help="Set port to connect to (default. 27017)",
        type=str,
        default="27017",
    )
    parser.add_argument(
        "--db",
        help="Set db name to connect to (default. speedtestdb)",
        type=str,
        default="speedtestdb",
    )
    parser.add_argument(
        "--size", help="Set file size to upload (MB)", type=float, default="1"
    )
    parser.add_argument("--cnt", help="Set Cnt", type=int, default="1")
    args = parser.parse_args()

    return args


############
# Initialize
# Check the connection to Local DB
def __init(host_ip, host_port, db_name):
    global testdb

    url = "mongodb://{0}:{1}".format(host_ip, host_port)
    logger.info("[Connection Test] DB Server: {0}/{1}".format(url, db_name))

    ##################
    # Connection check
    max_server_delay = 1
    username = None
    password = None
    authSource = db_name
    ### tls/ssl ###
    ###############
    client = MongoClient(url, serverSelectionTimeoutMS=max_server_delay)
    testdb = client[db_name]
    try:
        testdb.list_collection_names()
        logger.info("---> Connection is GOOD.")
    except errors.ServerSelectionTimeoutError as err:
        logger.error("---> Connection is BAD.")
        logger.error("     {}".format(err))
        return False
    except errors.OperationFailure as err:
        ### Need user authentication
        through = False
        while through == False:
            if not username or not password:
                logger.warning("Need users auhtenticated.")
                user = input("User name > ")
                pwd = getpass("Password > ")
            else:
                user = username
                pwd = password
            try:
                testdb.authenticate(user, pwd)
                logger.info("---> Connection is GOOD.")
                through = True
                authentication = True
            except errors.OperationFailure as err:
                logger.error("Authentication failed.")
                answer = input("Try again? [y/n]\n> ")
                print("")
                if answer.lower() == "y":
                    username = None
                    password = None
                else:
                    return False

    if not testdb.fs.chunks.count_documents({}) == 0:
        logger.error('DB "{}" already has chunks data.'.format(db_name))
        sys.exit(1)

    return True


def __download_data(filename, oid):
    localfs = gridfs.GridFS(testdb)
    binary = localfs.get(ObjectId(oid)).read()
    with open(filename, "wb") as f:
        f.write(binary)


def __upload_data(filename):
    localfs = gridfs.GridFS(testdb)
    with open(filename, "rb") as f:
        binary = f.read()
    oid = str(localfs.put(binary, filename=filename))
    return oid


def main():
    args = getArgs()
    size = (1024 ** 2) * args.size
    cnt = args.cnt

    host_ip = args.host
    host_port = args.port
    db_name = args.db

    if not __init(host_ip, host_port, db_name):
        logger.error("No communication to DB")
        sys.exit(1)

    logger.info("START!")

    filename = "{}.bin".format(datetime.now().strftime("%s"))
    if os.path.isfile(filename):
        logger.error('File "{}" already exists.'.format(filename))
        sys.exit(1)
    logger.info("    Create a file: {0} ({1} MB)".format(filename, args.size))

    f = open(filename, "wb")
    f.seek(int(size) - 1)
    f.write(b"\0")
    f.close()

    logger.info("    Testing...")
    upload_time = 0
    download_time = 0
    logdata.critical(
        "size {0} cnt {1} host {2} port {3} db {4}".format(
            args.size, cnt, host_ip, host_port, db_name
        )
    )
    for i in range(cnt):
        # Upload
        s = time.time()
        oid = __upload_data(filename)
        u_time = time.time() - s
        upload_time = upload_time + u_time
        # Download
        s = time.time()
        __download_data(filename, oid)
        d_time = time.time() - s
        download_time = download_time + d_time

        data_size = testdb.command("dbstats")["dataSize"]
        storage_size = testdb.command("dbstats")["storageSize"]

        logdata.critical(
            "upload {0} download {1} data {2} storage {3}".format(
                u_time, d_time, data_size, storage_size
            )
        )
        if i % 10 == 9:
            logger.info("    Counts: {}".format(i + 1))
    logger.info("    Done.")

    upload_speed = float(args.size) / float(upload_time / cnt)
    download_speed = float(args.size) / float(download_time / cnt)

    logger.info("")
    logger.info("    Remove a file: {0} ({1} MB)".format(filename, args.size))

    testdb.drop_collection("fs.files")
    testdb.drop_collection("fs.chunks")
    os.remove(filename)

    logger.info("FINISH!")

    logger.critical("")
    logger.critical("File Size: {0} MB, Total Count: {1}".format(args.size, cnt))
    logger.critical("")
    logger.critical("Upload Speed:   {} Mbps".format(upload_speed))
    logger.critical("Download Speed: {} Mbps".format(download_speed))


if __name__ == "__main__":
    main()
