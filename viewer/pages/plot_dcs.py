from functions.imports import *

plot_dcs_api = Blueprint("plot_dcs_api", __name__)
DCScandidates = [
    "vdda_voltage",
    "vdda_current",
    "vddd_voltage",
    "vddd_current",
    "hv_voltage",
    "hv_current",
    "temprature",
]


#################
@plot_dcs_api.route("/dcs", methods=["GET", "POST"])
def show_dcs():
    initPage()

    tr_oid = request.args.get("runId")
    dcs = {}
    dcs = setDCS(tr_oid)
    component = {"dcs": dcs}

    return render_template(
        "dcs_data.html", component=component, timezones=pytz.all_timezones
    )


##########################
# make Graph from DCS_data
@plot_dcs_api.route("/make_dcsGraph", methods=["GET", "POST"])
def make_dcsGraph():
    initPage()
    # get from form
    session["dcsplotType"] = request.form.get("dcsplotType")
    if not session.get("dcsStat"):
        session["dcsStat"] = {}

    if session["dcsplotType"] == "make" or session["dcsplotType"] == "make":
        if request.form.get("dataType") == "iv":
            target = [request.form.get("key_v"), request.form.get("key_i")]
            session["dcsStat"].update(
                {
                    target[0]: {
                        "min": request.form.get("v_min"),
                        "max": request.form.get("v_max"),
                        "step": request.form.get("v_step"),
                    }
                }
            )
            session["dcsStat"].update(
                {
                    target[1]: {
                        "min": request.form.get("i_min"),
                        "max": request.form.get("i_max"),
                        "step": request.form.get("i_step"),
                    }
                }
            )
        elif request.form.get("dataType") == "other":
            target = request.form.get("dcsType")
            session["dcsStat"].update(
                {
                    target: {
                        "min": request.form.get("min"),
                        "max": request.form.get("max"),
                        "step": request.form.get("step"),
                    }
                }
            )
    if session["dcsplotType"] == "make_TimeRange":
        start_timezone = request.form.get("start_timezone")
        start_year = request.form.get("start_year")
        start_month = request.form.get("start_month")
        start_day = request.form.get("start_day")
        start_hour = request.form.get("start_hour")
        start_min = request.form.get("start_minute")
        start_sec = request.form.get("start_second")

        end_timezone = request.form.get("end_timezone")
        end_year = request.form.get("end_year")
        end_month = request.form.get("end_month")
        end_day = request.form.get("end_day")
        end_hour = request.form.get("end_hour")
        end_min = request.form.get("end_minute")
        end_sec = request.form.get("end_second")

        start = datetime.strptime(
            "{0}-{1}-{2}T{3}:{4}:{5} {6}".format(
                start_year,
                start_month,
                start_day,
                start_hour,
                start_min,
                start_sec,
                start_timezone,
            ),
            "%Y-%m-%dT%H:%M:%S %z",
        ).astimezone(pytz.timezone("UTC"))
        #        start=datetime.strptime(setTime(start),'%Y/%m/%d %H:%M:%S')
        end = datetime.strptime(
            "{0}-{1}-{2}T{3}:{4}:{5} {6}".format(
                end_year, end_month, end_day, end_hour, end_min, end_sec, end_timezone
            ),
            "%Y-%m-%dT%H:%M:%S %z",
        ).astimezone(pytz.timezone("UTC"))
        #        end=datetime.strptime(setTime(end),'%Y/%m/%d %H:%M:%S')

        session["dcsStat"].update(
            {
                "timeRange": [
                    time.mktime(start.timetuple()),
                    time.mktime(end.timetuple()),
                ]
            }
        )
    elif session["dcsplotType"] == "set_defaultTimeRange":
        session["dcsStat"].pop("timeRange", None)
    # get from args                                                                                                      \

    componentId = request.args.get("id")
    runId = request.args.get("runId")

    return redirect(url_for("plot_dcs_api.show_dcs", id=componentId, runId=runId))


def setDCS(i_tr_oid):
    dcs_data = {}
    session["dcsList"] = {}
    DCS_DIR = "{0}/{1}/dcs".format(TMP_DIR, session["uuid"])
    DCS_dat_DIR = DCS_DIR + "/dat"
    DCS_plot_DIR = DCS_DIR + "/plot"

    cleanDir(DCS_DIR)
    cleanDir(DCS_dat_DIR)
    cleanDir(DCS_plot_DIR)

    if not i_tr_oid:
        return dcs_data

    query = {"_id": ObjectId(i_tr_oid)}
    this_run = localdb.testRun.find_one(query)
    if this_run.get("environment", "...") == "...":
        dcs_data.update({"dcs_data_exist": False})
        return dcs_data

    if not session.get("dcsStat"):
        session["dcsStat"] = {}
    if not session["dcsStat"].get("runId") == i_tr_oid:
        session["dcsStat"] = {}
        session["dcsStat"].update({"runId": i_tr_oid})
    results = {}
    url = {}
    results["iv"] = []
    results["other"] = []
    results["stat"] = {}

    dcs_status = plot_dcs.make_dcsplot(i_tr_oid)
    if session.get("dcsPlot"):
        for dcsType in session.get("dcsPlot"):
            if session["dcsPlot"][dcsType].get("file_num"):
                file_num = session["dcsPlot"][dcsType].get("file_num")
                fileList = session["dcsPlot"][dcsType].get("filename")
                if file_num == 2:
                    for i, filename in zip(["1", "2"], fileList):
                        if os.path.isfile(filename):
                            binary_image = open(filename, "rb")
                            code_base64 = base64.b64encode(binary_image.read()).decode()
                            binary_image.close()
                            url.update({i: bin2image("png", code_base64)})
                    results["iv"].append(
                        {
                            "dcsType": dcsType,
                            "keyName": session["dcsPlot"][dcsType].get("keyName"),
                            "runId": i_tr_oid,
                            "url_v": url.get("1"),
                            "url_i": url.get("2"),
                            "sortkey": "{}0".format(dcsType),
                            "v_min": session["dcsPlot"][dcsType].get("v_min"),
                            "v_max": session["dcsPlot"][dcsType].get("v_max"),
                            "v_step": session["dcsPlot"][dcsType].get("v_step"),
                            "i_min": session["dcsPlot"][dcsType].get("i_min"),
                            "i_max": session["dcsPlot"][dcsType].get("i_max"),
                            "i_step": session["dcsPlot"][dcsType].get("i_step"),
                        }
                    )
                if file_num == 1:
                    for filename in fileList:
                        if os.path.isfile(filename):
                            binary_image = open(filename, "rb")
                            code_base64 = base64.b64encode(binary_image.read()).decode()
                            binary_image.close()
                            i = "1"
                            url.update({i: bin2image("png", code_base64)})
                    results["other"].append(
                        {
                            "dcsType": dcsType,
                            "keyName": dcsType,
                            "runId": i_tr_oid,
                            "url": url.get("1"),
                            "sortkey": "{}0".format(dcsType),
                            "min": session["dcsPlot"][dcsType].get("min"),
                            "max": session["dcsPlot"][dcsType].get("max"),
                            "step": session["dcsPlot"][dcsType].get("step"),
                        }
                    )
    timeRange = []
    timeRange.append(
        setDatetime(datetime.fromtimestamp(session["dcsStat"].get("timeRange")[0]))
    )
    timeRange.append(
        setDatetime(datetime.fromtimestamp(session["dcsStat"].get("timeRange")[1]))
    )

    results["stat"] = {
        "RunTime": {
            "start": setDatetime(
                datetime.fromtimestamp(session["dcsStat"]["RunTime"][0])
            ),
            "finish": setDatetime(
                datetime.fromtimestamp(session["dcsStat"]["RunTime"][1])
            ),
        },
        "runId": i_tr_oid,
        "timeRange": {
            "start": {
                "year": timeRange[0].strftime("%Y"),
                "month": timeRange[0].strftime("%m"),
                "day": timeRange[0].strftime("%d"),
                "hour": timeRange[0].strftime("%H"),
                "minute": timeRange[0].strftime("%M"),
                "second": timeRange[0].strftime("%S"),
                "tzone": timeRange[0].strftime("%z"),
            },
            "end": {
                "year": timeRange[1].strftime("%Y"),
                "month": timeRange[1].strftime("%m"),
                "day": timeRange[1].strftime("%d"),
                "hour": timeRange[1].strftime("%H"),
                "minute": timeRange[1].strftime("%M"),
                "second": timeRange[1].strftime("%S"),
                "tzone": timeRange[0].strftime("%z"),
            },
        },
        "unixtimeR": {
            "start": setDatetime(
                datetime.fromtimestamp(session["dcsStat"].get("timeRange")[0])
            ),
            "end": setDatetime(
                datetime.fromtimestamp(session["dcsStat"].get("timeRange")[1])
            ),
        },
    }
    dcs_data.update({"dcs_data_exist": True, "results": results})
    return dcs_data
