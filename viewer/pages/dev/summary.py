from functions.imports import *

summary_api = Blueprint("summary_api", __name__)


@summary_api.route("/add_summary", methods=["GET", "POST"])
def add_summary():

    componentId = request.args.get("id")
    query = {"_id": ObjectId(componentId)}
    this_cmp = localdb.component.find_one(query)

    for scan in session["summaryList"]["before"]:

        if (
            session["summaryList"]["before"][scan]["runId"]
            == session["summaryList"]["after"][scan]["runId"]
        ):
            continue

        if session["summaryList"]["after"][scan]["runId"]:

            # make plot
            makePlot(session["summaryList"]["after"][scan]["runId"])

            # insert testRun and componentTestRun
            query = {"testRun": session["summaryList"]["after"][scan]["runId"]}
            this_ctr = localdb.componentTestRun.find_one(query)

            if this_ctr["component"] == componentId:
                runoid = session["summaryList"]["after"][scan]["runId"]

            else:
                thistime = datetime.datetime.utcnow()

                moduleComponentTestRun = this_ctr
                query = {
                    "_id": ObjectId(session["summaryList"]["after"][scan]["runId"])
                }
                moduleTestRun = localdb.testRun.find_one(query)
                moduleComponentTestRun.pop("_id", None)
                moduleTestRun.pop("_id", None)

                moduleTestRun.update({"attachments": []})
                moduleTestRun.update(
                    {"sys": {"rev": 0, "cts": thistime, "mts": thistime}}
                )
                runoid = str(localdb.testRun.insert(moduleTestRun))
                moduleComponentTestRun.update(
                    {
                        "component": componentId,
                        "testRun": runoid,
                        "sys": {"rev": 0, "cts": thistime, "mts": thistime},
                    }
                )
                localdb.componentTestRun.insert(moduleComponentTestRun)

            # add attachments into module TestRun
            query = {"component": componentId, "testRun": runoid}
            this_ctr = localdb.componentTestRun.find_one(query)
            query = {"_id": ObjectId(runoid)}
            thisRun = localdb.testRun.find_one(query)

            for mapType in session.get("plotList"):
                if session["plotList"][mapType]["HistoType"] == 1:
                    continue
                url = {}
                path = {}
                datadict = {"1": "_Dist", "2": ""}
                for i in datadict:
                    filename = "{0}_{1}{2}".format(
                        this_cmp["serialNumber"], mapType, datadict[i]
                    )
                    for attachment in thisRun["attachments"]:
                        if filename == attachment.get("filename"):
                            fs.delete(ObjectId(attachment.get("code")))
                            localdb.testRun.update(
                                query,
                                {
                                    "$pull": {
                                        "attachments": {"code": attachment.get("code")}
                                    }
                                },
                            )

                    filepath = "{0}/{1}/plot/{2}_{3}_{4}.png".format(
                        TMP_DIR,
                        str(session.get("uuid")),
                        str(thisRun["testType"]),
                        str(mapType),
                        i,
                    )
                    if os.path.isfile(filepath):
                        binary_image = open(filepath, "rb")
                        image = fs.put(
                            binary_image.read(), filename="{}.png".format(filename)
                        )
                        binary_image.close()
                        localdb.testRun.update(
                            query,
                            {
                                "$push": {
                                    "attachments": {
                                        "code": str(image),
                                        "dateTime": datetime.datetime.utcnow(),
                                        "title": "title",
                                        "description": "describe",
                                        "contentType": "png",
                                        "filename": filename,
                                    }
                                }
                            },
                        )
        # remove 'summary: True' in current summary run
        if session["summaryList"]["before"][scan]["runId"]:
            query = {"_id": ObjectId(session["summaryList"]["before"][scan]["runId"])}
            thisRun = localdb.testRun.find_one(query)

            keys = ["runNumber", "institution", "userIdentity", "testType"]
            query_id = dict([(key, thisRun[key]) for key in keys])
            localdb.testRun.update(query_id, {"$set": {"summary": False}}, multi=True)

            localdb.testRun.update(
                query_id,
                {
                    "$push": {
                        "comments": {
                            "userIdentity": session["userIdentity"],
                            "userid": session["uuid"],
                            "comment": session["summaryList"]["after"][scan]["comment"],
                            "after": session["summaryList"]["after"][scan]["runId"],
                            "datetime": datetime.datetime.utcnow(),
                            "institution": session["institution"],
                            "description": "add_summary",
                        }
                    }
                },
                multi=True,
            )
            updateData("testRun", query_id)

        query = {"component": componentId, "stage": session["stage"], "testType": scan}
        entries = localdb.componentTestRun.find(query)
        run_ids = []
        for entry in entries:
            run_ids.append(entry["testRun"])
        for run_id in run_ids:
            query = {"_id": ObjectId(run_id)}
            thisRun = localdb.testRun.find_one(query)
            keys = ["runNumber", "institution", "userIdentity", "testType"]
            query_id = dict([(key, thisRun[key]) for key in keys])
            run_entries = localdb.testRun.find(query_id)
            for run in run_entries:
                if run.get("summary"):
                    query = {"_id": run["_id"]}
                    localdb.testRun.update(query, {"$set": {"summary": False}})
                    updateData("testRun", query)

        # add 'summary: True' in selected run
        if session["summaryList"]["after"][scan]["runId"]:
            query = {"_id": ObjectId(session["summaryList"]["after"][scan]["runId"])}
            thisRun = localdb.testRun.find_one(query)

            keys = ["runNumber", "institution", "userIdentity", "testType"]
            query_id = dict([(key, thisRun[key]) for key in keys])

            localdb.testRun.update(query_id, {"$set": {"summary": True}}, multi=True)
            updateData("testRun", query_id)

    # pop session
    session.pop("testType", None)
    session.pop("runId", None)
    session.pop("summaryList", None)
    session.pop("stage", None)
    session.pop("step", None)
    cleanDir(THUMBNAIL_DIR)

    return redirect(url_for("show_component", id=componentId))


# select page
@summary_api.route("/select_summary", methods=["GET", "POST"])
def select_summary():

    session.pop("testType", None)
    session.pop("runId", None)
    session.pop("code", None)
    session.pop("plotList", None)

    # get from args
    session["this"] = request.args.get("id")
    # this component
    query = {"_id": ObjectId(session["this"])}

    if (
        not str(localdb.component.find_one(query)["componentType"]).lower()
        == "Module".lower()
    ):
        query = {"child": session["this"]}
        session["this"] = localdb.childParentRelation.find_one(query)["parent"]

    query = {"_id": ObjectId(session["this"])}
    this_cmp = localdb.component.find_one(query)

    unit = "module"

    # get from form
    if request.form.get("step"):
        session["step"] = int(request.form.get("step"))

    if session.get("step") == 4:
        return redirect(url_for("add_summary", id=request.args.get("id")))

    if not request.form.get("stage") == session.get("stage") or not session.get(
        "stage"
    ):
        session["summaryList"] = {"after": {}, "before": {}}
        session["step"] = 1

    session["stage"] = request.form.get("stage")

    if request.form.get("testType"):
        if session["step"] == 1:
            session["summaryList"]["after"][request.form.get("testType")].update(
                {"runId": request.form.get("runId")}
            )
            session["testType"] = request.form.get("testType")
        else:
            session["summaryList"]["after"][request.form.get("testType")].update(
                {"comment": str(request.form.get("comment"))}
            )

    # chips and parent
    chips = []
    query = [{"parent": session["this"]}, {"child": session["this"]}]
    child_entries = localdb.childParentRelation.find({"$or": query})
    for child in child_entries:
        chips.append({"component": child["child"]})
        parent_id = child["parent"]

    # this module
    query = {"_id": ObjectId(parent_id)}
    this_module = localdb.component.find_one(query)

    # chips of module
    query = {"parent": parent_id}
    child_entries = localdb.childParentRelation.find(query)
    chip_ids = []
    for child in child_entries:
        chip_ids.append(child["child"])

    # set chip and module information
    component = {}
    component_chips = []
    for chip_id in chip_ids:
        query = {"_id": ObjectId(chip_id)}
        this_chip = localdb.component.find_one(query)
        # str(component['componentType']).lower() = str(this_chip['componentType']).lower()
        component_chips.append(
            {"_id": chip_id, "serialNumber": this_chip["serialNumber"]}
        )
    module = {"_id": parent_id, "serialNumber": this_module["serialNumber"]}

    query = {"$or": chips}
    run_entries = localdb.componentTestRun.find(query)
    stages = []
    for run in run_entries:
        stages.append(run.get("stage"))
    stages = list(set(stages))

    # set summary
    summary = setSummaryTest()

    # set result index
    result_index = setResultIndex()

    component.update(
        {
            "_id": session["this"],
            "serialNumber": this_cmp["serialNumber"],
            "module": module,
            "chips": component_chips,
            "unit": unit,
            "summary": summary,
            "stages": stages,
            "step": session["step"],
            "comments": listset.summary_comment,
            "resultIndex": result_index,
            "stage": session["stage"],
        }
    )

    return render_template(
        "add_summary.html", component=component, timezones=setTimezone()
    )


# show summary plot ( in add function )
@summary_api.route("/show_summary_selected", methods=["GET"])
def show_summary_selected():
    # get from args
    runoid = request.args.get("runId")
    histo = request.args.get("histo")
    mapType = request.args.get("mapType")

    query = {"_id": ObjectId(runoid)}
    thisRun = localdb.testRun.find_one(query)

    makePlot(runoid)

    url = ""
    filename = (
        TMP_DIR
        + "/"
        + str(session.get("uuid"))
        + "/plot/"
        + str(thisRun["testType"])
        + "_"
        + str(mapType)
        + "_{}.png".format(histo)
    )
    if os.path.isfile(filename):
        binary_image = open(filename, "rb")
        code_base64 = base64.b64encode(binary_image.read()).decode()
        binary_image.close()
        url = bin2image("png", code_base64)

    return redirect(url)


## summary plot in add summary function page
# def setSummaryTest():
#
#    summary_index = {}
#    scanList = ['digitalscan', 'analogscan', 'thresholdscan', 'totscan', 'noisescan', 'selftrigger']
#
#    if not session.get('stage'): return summary_index
#
#    stage = session['stage']
#    query = {'_id': ObjectId(session.get('this'))}
#    this_cmp = localdb.component.find_one(query)
#
#    # first step in add summary function: make current summary plots as thumbnail
#    if not session['summaryList']['before']:
#
#        after_dir  = '{0}/{1}/after'.format(TMP_DIR, session.get('uuid','localuser'))
#        cleanDir(after_dir)
#
#        before_dir = '{0}/{1}/before'.format(TMP_DIR, session.get('uuid','localuser'))
#        cleanDir(before_dir)
#
#        for scan in scanList:
#            session['summaryList']['before'].update({scan: {'runId': None}})
#            session['summaryList']['after'].update({scan: {'runId': None}})
#
#            query = {'component': session.get('this'), 'stage': stage, 'testType': scan}
#            run_entries = localdb.componentTestRun.find(query)
#            for componentTestRun in run_entries:
#                query = {'_id': ObjectId(componentTestRun['testRun'])}
#                this_run = localdb.testRun.find_one(query)
#                if this_run.get('summary'):
#                    session['summaryList']['before'][scan].update({'runId': str(this_run['_id'])})
#                    session['summaryList']['after'][scan].update({'runId': str(this_run['_id'])})
#
#                    makePlot(str(this_run['_id']))
#
#                    for maptype in session.get('plotList'):
#                        if not session['plotList'][maptype].get('HistoType') == 2: continue
#                        url = {}
#                        path = {}
#                        datadict = {'1': '_Dist', '2': ''}
#                        for i in datadict:
#                            filepath = '{0}/{1}/plot/{2}_{3}_{4}.png'.format(TMP_DIR, str(session.get('uuid','localuser')), str(this_run['testType']), str(maptype), i)
#                            if os.path.isfile(filepath):
#                                binary_file = open(filepath, 'rb')
#                                binary = binary_file.read()
#                                binary_file.close()
#
#                                image_bin = io.BytesIO(binary)
#                                image = Image.open(image_bin)
#                                image.thumbnail((int(image.width/4),int(image.height/4)))
#                                filename_before = '{0}/{1}_{2}_{3}_{4}{5}.png'.format(before_dir, stage, scan, this_cmp['serialNumber'], maptype, datadict[i])
#                                image.save(filename_before)
#                                filename_after  = '{0}/{1}_{2}_{3}_{4}{5}.png'.format(after_dir,  stage, scan, this_cmp['serialNumber'], maptype, datadict[i])
#                                image.save(filename_after)
#
#    # remove/replace summary plot: make replaced summary plots as thumbnail
#    elif session['step'] == 1:
#        after_dir  = '{0}/{1}/after'.format(TMP_DIR, session.get('uuid','localuser'))
#
#        for scan in scanList:
#            if not session.get('testType') == scan: continue
#
#            for r in glob.glob('{0}/{1}_{2}*'.format(after_dir, stage, scan)): os.remove(r)
#
#            if session['summaryList']['after'][scan]['runId']:
#                query = {'_id': ObjectId(session['summaryList']['after'][scan]['runId'])}
#                this_run = localdb.testRun.find_one(query)
#
#                makePlot(str(this_run['_id']))
#
#                for maptype in session.get('plotList'):
#                    if not session['plotList'][maptype].get('HistoType') == 2: continue
#                    url = {}
#                    path = {}
#                    datadict = {'1': '_Dist', '2': ''}
#                    for i in datadict:
#                        filepath = '{0}/{1}/plot/{2}_{3}_{4}.png'.format(TMP_DIR, str(session.get('uuid','localuser')), str(this_run['testType']), str(maptype), i)
#                        if os.path.isfile( filepath ):
#                            binary_file = open(filepath, 'rb')
#                            binary = binary_file.read()
#                            binary_file.close()
#
#                            image_bin = io.BytesIO(binary)
#                            image = Image.open(image_bin)
#                            image.thumbnail((int(image.width/4),int(image.height/4)))
#                            filename_after = '{0}/{1}_{2}_{3}_{4}{5}.png'.format(after_dir, stage, scan, this_cmp['serialNumber'], maptype, datadict[i])
#                            image.save(filename_after)
#
#    # check path to thumbnails
#    scandict = {'before': {},
#                'after': {}}
#    total = 0
#    submit = True
#    for scan in scanList:
#
#        abType = {'before': '{0}/{1}/before'.format(TMP_DIR,session.get('uuid','localuser')),
#                  'after': '{0}/{1}/after'.format(TMP_DIR,session.get('uuid','localuser'))}
#
#        for ab in abType:
#
#            scandict[ab].update({scan: {}})
#            maplist = []
#
#            for maptype in listset.scan[scan]:
#
#                mapdict = {'mapType': maptype[0]}
#
#                total += 1
#
#                if session['summaryList'][ab][scan]['runId']:
#
#                    query = {'_id': ObjectId(session['summaryList'][ab][scan]['runId'])}
#                    this_run = localdb.testRun.find_one(query)
#                    query = {'testRun': session['summaryList'][ab][scan]['runId']}
#                    this_ctr = localdb.componentTestRun.find_one(query)
#                    env_dict = setEnv(this_ctr)
#
#                    datadict = {'1': '_Dist', '2': ''}
#                    for i in datadict:
#
#                        filename = '{0}/{1}_{2}_{3}_{4}{5}.png'.format(abType[ab], stage, scan, this_cmp['serialNumber'], maptype[0], datadict[i])
#                        if os.path.isfile(filename):
#                            binary_image = open(filename, 'rb')
#                            code_base64 = base64.b64encode(binary_image.read()).decode()
#                            binary_image.close()
#                            url = bin2image('png', code_base64)
#                            mapdict.update({'url{}Dthum'.format(i): url})
#
#                    scandict[ab][scan].update({'runNumber': this_run['runNumber'],
#                                               'runId': str(this_run['_id']),
#                                               'institution': this_run['institution'],
#                                               'userIdentity': this_run['userIdentity'],
#                                               'environment': env_dict})
#                maplist.append(mapdict)
#
#            # put suitable comment for each run
#            comment = '...'
#            if session['summaryList']['before'][scan]['runId'] == session['summaryList']['after'][scan]['runId']:
#                comment = None
#            elif session['summaryList']['after'][scan].get('comment') in listset.summary_comment:
#                comment = session['summaryList']['after'][scan]['comment']
#            elif not session['summaryList']['before'][scan]['runId']:
#                comment = 'add'
#            else:
#                submit =  False
#
#            scandict[ab][scan].update({'map': maplist,
#                                       'num': len(maplist),
#                                       'comment': comment})
#
#    if not scandict == {}:
#        summary_index.update({'stage': stage,
#                             'scan': scandict,
#                             'total': total,
#                             'submit': submit})
#
#    return summary_index
#
