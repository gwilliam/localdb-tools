from functions.imports import *

toppage_api = Blueprint("toppage_api", __name__)

###############
# Show top page
@toppage_api.route("/", methods=["GET"])
def show_toppage():
    initPage()

    user_dir = "{0}/{1}".format(TMP_DIR, session["uuid"])
    if os.path.isdir(user_dir):
        shutil.rmtree(user_dir)

    session.pop("signup", None)
    #    session.pop('keywords', None)
    #    session.pop('match', None)
    table_docs = {"page": None}

    ############
    ## for admin
#    try:
#        if os.path.exists('../config.yml'):
#            with open('../config.yml') as f:
#                l_strip = [s.strip() for s in f.readlines() if ' KeyFile' in s]
#                path_to_adminfile = l_strip[0].split(':')[1].split()[0]
#        else:
#            with open('admin_conf.yml') as f:
#                l_strip = [s.strip() for s in f.readlines() if ' KeyFile' in s]
#                path_to_adminfile = l_strip[0].split(':')[1].split()[0]
#        admin_file = open(path_to_adminfile, "r")
#        admin_info = admin_file.readline().replace( '\n' , '' )
#    except:
#        admin_info = ''

    try:
        query = {"auth" : "adminViewer"}
        admin_info = client.localdbtools.viewer.user.find_one(query, sort = [('_id', -1)])
        admin_username = admin_info["name"]
    except:
        admin_username = ""


    try:
        user_name = session['username']
    except:
        user_name = ""

    if admin_username == user_name:
        table_docs["admin"] = "admin"
    else:
        table_docs["admin"] = "shomin"

    mail = args.fmail

    return render_template(
        "toppage.html", table_docs=table_docs, mail=mail, timezones=pytz.all_timezones
    )


#####################
# Show component list
@toppage_api.route("/components", methods=["GET", "POST"])
def show_comps():
    initPage()

    table_docs = {"components": {}}

    # get keywords
    if not request.form.get("keywords") == None:
        table_docs["keywords"] = request.form.get("keywords", "")
        table_docs["match"] = request.form.get("match", "None")
    else:
        table_docs["keywords"] = request.args.get("keywords", "")
        table_docs["match"] = request.args.get("match", "None")

    query = {"dbVersion": dbv}
    entries = localdb.component.find(query)
    cmp_ids = []
    for entry in entries:
        cmp_ids.append(str(entry["_id"]))

    for cmp_id in cmp_ids:
        query = {"_id": ObjectId(cmp_id)}
        this_cmp = localdb.component.find_one(query)
        chip_type = this_cmp["chipType"]
        if not chip_type in table_docs["components"]:
            table_docs["components"].update(
                {
                    chip_type: {
                        "modules": [],
                        "module_num": 0,
                        "chips": [],
                        "chip_num": 0,
                    }
                }
            )

        if this_cmp.get("componentType", "front-end_chip").lower() == "module":
            key = "modules"
            id_key = "child"
            query = {"parent": cmp_id}
        elif (
            this_cmp.get("componentType", "front-end_chip").lower() == "front-end_chip"
        ):
            key = "chips"
            id_key = "parent"
            query = {"child": cmp_id}
        ### cpr entries
        entries = localdb.childParentRelation.find(query)
        cpr_ids = []
        for entry in entries:
            cpr_ids.append(entry[id_key])
        cps = []
        for cpr_id in cpr_ids:
            query = {"_id": ObjectId(cpr_id)}
            this_cp = localdb.component.find_one(query)
            if this_cp:
                try:
                    name = this_cp["name"]
                except:
                    name = this_cp["serialNumber"]
                cps.append(
                    {"_id": cpr_id, "collection": "component", "name": name, "grade": {}}
                )
        ### Latest Scan
        query = {"component": cmp_id}
        run_entries = (
            localdb.QC.result.find(query).sort([("$natural", -1)]).limit(1)
        )
        result = {
            "stage": None,
            "runId": None,
            "datetime": None,
            "testType": None,
            "user": None,
            "site": None,
        }
        for this_run in run_entries:
            result.update(
                {
                    "stage": this_run["currentStage"],
                    "runId": str(this_run["_id"]),
                    "datetime": setTime(
                        this_run["sys"]["cts"],
                        session.get("timezone", str(get_localzone())),
                    ),
                    "testType": this_run["testType"],
                    "user": this_run["user"],
                    "site": this_run["address"],
                }
            )

        ### tags
        query = {}
        module_tag_candidates = userdb.viewer.tag.categories.find(query)
        tag_candidate = []
        for module_tag in module_tag_candidates:
            tag_candidate.append(module_tag)
        ### put tags
        query = {"componentid": cmp_id}
        module_tags = userdb.viewer.tag.docs.find(query)
        tag = []
        for module_tag in module_tags:
            tag.append(module_tag)

        try:
            name = this_cmp["name"]
        except:
            name = this_cmp["serialNumber"]
        # table_docs['components'][chip_type][key].append({
        module_data = {
            "_id": cmp_id,
            "collection": "component",
            "name": name,
            "cps": cps,
            "grade": {},
            "stage": result["stage"],
            "runId": result["runId"],
            "datetime": result["datetime"],
            "testType": result["testType"],
            "user": result["user"],
            "site": result["site"],
            "proDB": this_cmp.get("proDB", False),
            "component_tag_candidate": tag_candidate,
            "component_tag": tag,
        }

        query_targets = []
        query_targets.append(module_data["name"])
        query_targets.append(module_data["stage"])
        query_targets.append(module_data["user"])
        query_targets.append(module_data["site"])
        query_targets.append(module_data["datetime"])
        for tag in module_data["component_tag"]:
            query_targets.append(tag["name"])
        for component in module_data["cps"]:
            query_targets.append(component["name"])
        flug = query_keywords(
            table_docs["keywords"], query_targets, table_docs["match"]
        )
        if flug == 1:
            table_docs["components"][chip_type][key].append(module_data)

    for chip_type in table_docs["components"]:
        table_docs["components"][chip_type].update(
            {"module_num": len(table_docs["components"][chip_type]["modules"])}
        )
        module = sorted(
            table_docs["components"][chip_type]["modules"],
            key=lambda x: x["name"],
            reverse=True,
        )
        table_docs["components"][chip_type]["modules"] = module
        table_docs["components"][chip_type].update(
            {"chip_num": len(table_docs["components"][chip_type]["chips"])}
        )
        chip = sorted(
            table_docs["components"][chip_type]["chips"],
            key=lambda x: x["name"],
            reverse=True,
        )
        table_docs["components"][chip_type]["chips"] = chip

    table_docs["page"] = "components"
    table_docs["title"] = "Component List"
    table_docs["download_module"] = os.path.exists(IF_DIR+"/doing_download.txt")

    return render_template(
        "toppage.html", table_docs=table_docs, timezones=pytz.all_timezones
    )


####################
# Show test run list
@toppage_api.route("/scan", methods=["GET", "POST"])
def show_scans():

    initPage()
    check_database_update()

    max_num = 15
    sort_cnt = 0
    if not request.args.get("p", 0) == "":
        sort_cnt = int(request.args.get("p", 0))

    table_docs = {"run": []}
    # get keywords
    if not request.form.get("keywords") == None:
        table_docs["keywords"] = request.form.get("keywords", "")
        table_docs["match"] = request.form.get("match", "None")
    else:
        table_docs["keywords"] = request.args.get("keywords", "")
        table_docs["match"] = request.args.get("match", "None")

    run_ids = query_docs(Keywords=table_docs["keywords"], Match=table_docs["match"])

    if "config" in run_ids:
        run_ids.remove("config")

    nrun_entries = len(run_ids)
    run_num_list = []
    if nrun_entries < (sort_cnt + 1) * max_num:
        for i in range(nrun_entries % max_num):
            run_num_list.append(sort_cnt * (max_num) + i)
    else:
        for i in range(max_num):
            run_num_list.append(sort_cnt * (max_num) + i)


    run_counts = 0
    if not nrun_entries == 0:
        for i in run_num_list:
            try:
                run_data = getScanSummary(run_ids[i])
                run_counts += 1
                table_docs["run"].append({"run_data": run_data, "nrun": run_counts})
            except:
                pass

    table_docs["run"] = sorted(
        table_docs["run"], key=lambda x: x["run_data"]["datetime"], reverse=True
    )
    cnt = []
    for i in range(((nrun_entries - 1) // max_num) + 1):
        if sort_cnt - (max_num / 2) < i:
            cnt.append(i)
        if len(cnt) == max_num:
            break

    scans_num = {
        "total": run_counts,
        "cnt": cnt,
        "now_cnt": sort_cnt,
        "max_cnt": ((nrun_entries - 1) // max_num),
        "max_num": max_num,
    }
    table_docs.update(scans_num)

    # table_docs['run'] = sorted(table_docs['run'], key=lambda x:x['run_data']['datetime'], reverse=True)

    table_docs["page"] = "scan"
    table_docs["title"] = "Scan List"

    return render_template(
        "toppage.html", table_docs=table_docs, timezones=pytz.all_timezones
    )


####################
# Show test run list
@toppage_api.route("/moduleDB", methods=["GET", "POST"])
def show_module():
    initPage()

    if not session.get("logged_in", False):
        return render_template( "401.html" )

    table_docs = {"components": {}}
    table_docs["page"] = "module"
    table_docs["title"] = "Register a Module to ITkPD"

    stage = request.form.get("stage", "input_module")
    code1 = request.form.get("code1", "")
    code2 = request.form.get("code2", "")
    institution = request.form.get("institution", "")
    moduleinfo = request.form.getlist("moduleinfo")
    tripletinfo = request.form.getlist("tripletinfo")
    userinfo = request.form.get("userinfo", type=itkdb.core.User)
    check_serialNum = ""
    module_serialnum = ""
    code = {"code1": os.environ.get("ITKDB_ACCESS_CODE1"), "code2": os.environ.get("ITKDB_ACCESS_CODE2")}
    user = os.environ.get("USER_ITK")
    text = ""
    parent = {}
    config = {}
    look_up_table = {}
    currentLocation = {}



    localdbtools_db = client.localdbtools
    try:
        module_config = localdbtools_db["QC.module.types"].find_one()
        look_up_table = ModuleRegistration(code1, code2).make_table(module_config)
    except:
        stage = "download_moduletypes"


    if os.path.exists(IF_DIR + '/doing_download.txt'):
        stage = "input_module"

    if stage == "confirm":
        module_serialnum = "20" + "U" + look_up_table["MODULE_TYPES"][moduleinfo[0]]["XX"] + look_up_table["MODULE_TYPES"][moduleinfo[0]]["YY"] + str(moduleinfo[1][7]) + str(moduleinfo[1][8]) + str(moduleinfo[2][9:14])

    if stage == "complete":
        flug = process_request(code1, code2)
        if flug == 0:
            stage = "confirm"
            text = "Not authorized. Please input correct codes."
        if flug == 1:
            stage = "complete"
            #institution = self.pd_client.get("getUser", json={"userIdentity":self.u.identity})["institutions"][0]["code"]
            institution = ModuleRegistration(code1,code2).user_institution()
            module_serialnum = "20" + "U" + look_up_table["MODULE_TYPES"][moduleinfo[0]]["XX"] + look_up_table["MODULE_TYPES"][moduleinfo[0]]["YY"] + str(moduleinfo[1][7]) + str(moduleinfo[1][8]) + str(moduleinfo[2][9:14])

            try:
                bare_info = ModuleRegistration(code1,code2).get_component(moduleinfo[1])
                bare_assemble = bare_info["assembled"]
                bare_location = bare_info["currentLocation"]["code"]
                if bare_assemble:
                    text = "Bare:" + moduleinfo[1] + " is already assembled..."
                    stage = "confirm"
                    return render_template("toppage.html", table_docs=table_docs, timezones=pytz.all_timezones, stage=stage, text=text, moduleInfo=moduleinfo, look_up_table=look_up_table, module_serialnum=module_serialnum, institution=institution, code=code, userInfo=userinfo, parent=parent, tripletInfo=tripletinfo, check_serialNum=check_serialNum, currentLocation=currentLocation)
            except:
                text = "Bare:" + moduleinfo[1] + " does not exist in ITkPD..."
                stage = "confirm"
                return render_template("toppage.html", table_docs=table_docs, timezones=pytz.all_timezones, stage=stage, text=text, moduleInfo=moduleinfo, look_up_table=look_up_table, module_serialnum=module_serialnum, institution=institution, code=code, userInfo=userinfo, parent=parent, tripletInfo=tripletinfo, check_serialNum=check_serialNum, currentLocation=currentLocation)
            try:
                pcb_info = ModuleRegistration(code1,code2).get_component(moduleinfo[2])
                pcb_assemble = pcb_info["assembled"]
                pcb_location = pcb_info["currentLocation"]["code"]
                if pcb_assemble:
                    text = "PCB:" + moduleinfo[2] + " is already assembled..."
                    stage = "confirm"
                    return render_template("toppage.html", table_docs=table_docs, timezones=pytz.all_timezones, stage=stage, text=text, moduleInfo=moduleinfo, look_up_table=look_up_table, module_serialnum=module_serialnum, institution=institution, code=code, userInfo=userinfo, parent=parent, tripletInfo=tripletinfo, check_serialNum=check_serialNum, currentLocation=currentLocation)
            except:
                text = "PCB:" + moduleinfo[2] + " does not exist in ITkPD..."
                stage = "confirm"
                return render_template("toppage.html", table_docs=table_docs, timezones=pytz.all_timezones, stage=stage, text=text, moduleInfo=moduleinfo, look_up_table=look_up_table, module_serialnum=module_serialnum, institution=institution, code=code, userInfo=userinfo, parent=parent, tripletInfo=tripletinfo, check_serialNum=check_serialNum, currentLocation=currentLocation)

            if tripletinfo[0] and tripletinfo[1]:
                try:
                    bare_info2 = ModuleRegistration(code1,code2).get_component(tripletinfo[0])
                    bare_assemble2 = bare_info2["assembled"]
                    bare_location2 = bare_info2["currentLocation"]["code"]
                    if bare_assemble2:
                        text = "Bare:" + tripletinfo[1] + " is already assembled..."
                        stage = "confirm"
                        return render_template("toppage.html", table_docs=table_docs, timezones=pytz.all_timezones, stage=stage, text=text, moduleInfo=moduleinfo, look_up_table=look_up_table, module_serialnum=module_serialnum, institution=institution, code=code, userInfo=userinfo, parent=parent, tripletInfo=tripletinfo, check_serialNum=check_serialNum, currentLocation=currentLocation)
                except:
                    text = "Bare module:" + tripletinfo[0] + " does not exist in ITkPD..."
                    stage = "confirm"
                    return render_template("toppage.html", table_docs=table_docs, timezones=pytz.all_timezones, stage=stage, text=text, moduleInfo=moduleinfo, look_up_table=look_up_table, module_serialnum=module_serialnum, institution=institution, code=code, userInfo=userinfo, parent=parent, tripletInfo=tripletinfo, check_serialNum=check_serialNum, currentLocation=currentLocation)
                try:
                    bare_info3 = ModuleRegistration(code1,code2).get_component(tripletinfo[1])
                    bare_assemble3 = bare_info3["assembled"]
                    bare_location3 = bare_info3["currentLocation"]["code"]
                    if bare_assemble3:
                        text = "Bare:" + tripletinfo[1] + " is already assembled..."
                        stage = "confirm"
                        return render_template("toppage.html", table_docs=table_docs, timezones=pytz.all_timezones, stage=stage, text=text, moduleInfo=moduleinfo, look_up_table=look_up_table, module_serialnum=module_serialnum, institution=institution, code=code, userInfo=userinfo, parent=parent, tripletInfo=tripletinfo, check_serialNum=check_serialNum, currentLocation=currentLocation)
                except:
                    text = "Bare module:" + tripletinfo[1] + " does not exist in ITkPD..."
                    stage = "confirm"
                    return render_template("toppage.html", table_docs=table_docs, timezones=pytz.all_timezones, stage=stage, text=text, moduleInfo=moduleinfo, look_up_table=look_up_table, module_serialnum=module_serialnum, institution=institution, code=code, userInfo=userinfo, parent=parent, tripletInfo=tripletinfo, check_serialNum=check_serialNum, currentLocation=currentLocation)

                if not any([bare_assemble, bare_assemble2, bare_assemble3, pcb_assemble]):
                    if institution == bare_location == bare_location2 == bare_location3 == pcb_location:
                        config =  {
                          "type":moduleinfo[0],
                          "FECHIP":moduleinfo[1][7],
                          "child":{
                            "BARE_MODULE":[moduleinfo[1], tripletinfo[0], tripletinfo[1]],
                            "PCB":moduleinfo[2],
                            "CARRIER":moduleinfo[3]
                          },
                          "serialNumber":module_serialnum
                        }
                    else:
                        stage = "confirm"
                else:
                    stage = "confirm"


#                parent = {
#                 "bare1":ModuleRegistration(code1,code2).check_parent(moduleinfo[1]),
#                 "bare2":ModuleRegistration(code1,code2).check_parent(tripletinfo[0]),
#                 "bare3":ModuleRegistration(code1,code2).check_parent(tripletinfo[1]),
#                 "pcb":ModuleRegistration(code1,code2).check_parent(moduleinfo[2]),
#                 "carrier":ModuleRegistration(code1,code2).check_exist(moduleinfo[3])
#                }
#                currentLocation = {
#                  "bare1": ModuleRegistration(code1,code2).module_institution(institution, moduleinfo[1]),
#                  "bare2": ModuleRegistration(code1,code2).module_institution(institution, tripletinfo[0]),
#                  "bare3": ModuleRegistration(code1,code2).module_institution(institution, tripletinfo[1]),
#                  "pcb": ModuleRegistration(code1,code2).module_institution(module_serialnum, moduleinfo[2]),
#                  "carrier": ModuleRegistration(code1,code2).module_institution(module_serialnum, moduleinfo[3])
#                }
#                if not ModuleRegistration(code1,code2).check_FEchips(moduleinfo[1]):
#                    parent["bare1"] = "3"
#                if not ModuleRegistration(code1,code2).check_FEchips(tripletinfo[0]):
#                    parent["bare2"] = "3"
#                if not ModuleRegistration(code1,code2).check_FEchips(tripletinfo[1]):
#                    parent["bare3"] = "3"
#
#                if parent == {"bare1":"1", "bare2":"1", "bare3":"1", "pcb":"1", "carrier":"1"}:# and currentLocation == {"bare1":"1", "pcb":"1", "carrier":"1"}:
#                    config =  {
#                      "type":moduleinfo[0],
#                      "FECHIP":moduleinfo[1][7],
#                      "child":{
#                        "BARE_MODULE":[moduleinfo[1], tripletinfo[0], tripletinfo[1]],
#                        "PCB":moduleinfo[2],
#                        "CARRIER":moduleinfo[3]
#                      },
#                      "serialNumber":module_serialnum
#                    }
#                else:
#                    stage = "confirm"
            else:
                if not any([bare_assemble, pcb_assemble]):
                    if institution == bare_location == pcb_location:
                        config =  {
                             "type":moduleinfo[0],
                             "FECHIP":moduleinfo[1][7],
                             "THICKNESS":moduleinfo[1][8],
                             "child":{
                               "BARE_MODULE":[moduleinfo[1], tripletinfo[0], tripletinfo[1]],
                               "PCB":moduleinfo[2],
                               "CARRIER":moduleinfo[3]
                             },
                             "serialNumber":module_serialnum
                           }
                    else:
                        stage = "confirm"
                else:
                    stage = "confirm"

#                parent = {
#                 "bare1":ModuleRegistration(code1,code2).check_parent(moduleinfo[1]),
#                 "pcb":ModuleRegistration(code1,code2).check_parent(moduleinfo[2]),
#                 "carrier":ModuleRegistration(code1,code2).check_exist(moduleinfo[3])
#                }
#                currentLocation = {
#                  "bare1": ModuleRegistration(code1,code2).module_institution(module_serialnum, moduleinfo[1]),
#                  "pcb": ModuleRegistration(code1,code2).module_institution(module_serialnum, moduleinfo[2]),
#                  "carrier": ModuleRegistration(code1,code2).module_institution(module_serialnum, moduleinfo[3])
#                }
#                if not ModuleRegistration(code1,code2).check_FEchips(moduleinfo[1]):
#                    parent["bare1"] = "3"
#
#                if parent == {"bare1":"1", "pcb":"1", "carrier":"1"}:# and currentLocation == {"bare1":"1", "pcb":"1", "carrier":"1"}:
#                    config =  {
#                         "type":moduleinfo[0],
#                         "FECHIP":moduleinfo[1][7],
#                         "child":{
#                           "BARE_MODULE":[moduleinfo[1], tripletinfo[0], tripletinfo[1]],
#                           "PCB":moduleinfo[2],
#                           "CARRIER":moduleinfo[3]
#                         },
#                         "serialNumber":module_serialnum
#                       }
#                else:
#                    stage = "confirm"


            check_serialNum = ModuleRegistration(code1,code2).check_exist(module_serialnum)
            if check_serialNum == "2":
                ModuleRegistration(code1,code2).register_Module(code1, code2, config, look_up_table)
                try:
                    ModuleDownloader(code1,code2).download_QC_info(module_serialnum)
                    dic1 = {"code1": code1, "code2": code2, "component_id":None}
                    text = ""
                except:
                    stage = "confirm"
                    text = "An error occered. Please contact LocalDB admin."
            else:
                stage = "confirm"

    return render_template(
        "toppage.html", table_docs=table_docs, timezones=pytz.all_timezones, stage=stage, text=text, moduleInfo=moduleinfo, look_up_table=look_up_table, module_serialnum=module_serialnum, institution=institution, code=code, userInfo=userinfo, parent=parent, tripletInfo=tripletinfo, check_serialNum=check_serialNum, currentLocation=currentLocation
    )

########################
### Download all results
@toppage_api.route("/download_allresult", methods=["GET", "POST"])
def download_allresult():

    if not session.get("logged_in", False):
        return render_template( "401.html" )

    stage = request.form.get("stage", "input")
    code1 = request.form.get("code1", "")
    code2 = request.form.get("code2", "")
    text = ""

    if os.path.exists(IF_DIR + '/doing_download.txt'):
        stage = "complete"

    if stage == "complete":
        flug = process_request(code1, code2)
        if flug == 0:
            stage = "input"
            text = "Not authorized. Please input correct codes."
        else:
            if not os.path.exists(IF_DIR + '/doing_download.txt'):
                try:
                    dic1 = {"code1": code1, "code2": code2}
                    thread_1 = threading.Thread(target=QCResultDownloader(code1,code2).download_all_results)
                    thread_1.start()
                    #download_QC_info(code1, code2, None)
                except:
                    stage = "input"
                    text = "An error occered. Please contact LocalDB admin."


    return render_template( "download_allresult.html", stage=stage, text=text )

#################
## Query Function
def query_docs(Keywords, Match):

    flug = 1
    run_ids = []
    if Keywords == "None" or Keywords == "":
        entries = userdb.viewer.query.find({})
    else:
        Keyword = Keywords.split(" ")
        Keyword_num = len(Keyword)
        AND_flug = 0
        OR_flug = 0
        for i in range(Keyword_num):
            if Keyword[i].upper() == "AND":
                AND_flug = 1
            if Keyword[i].upper() == "OR":
                OR_flug = 1

        query_list = []
        if AND_flug == 1 and OR_flug == 1:
            entries = userdb.viewer.query.find({})
        elif AND_flug == 0 and OR_flug == 1:
            flug = 0
            for i in range(Keyword_num):
                if not Keyword[i] == "":
                    if Match == "partial":
                        query_list.append({"data": {"$regex": Keyword[i]}})
                    if Match == "perfect":
                        query_list.append({"data": Keyword[i]})
            entries = userdb.viewer.query.find({"$or": query_list})
        else:
            Keyword_flug = []
            num = []
            for i in range(Keyword_num):
                if Keyword[i].upper() == "AND":
                    pass
                else:
                    if Match == "partial":
                        query_list.append({"data": {"$regex": Keyword[i]}})
                    if Match == "perfect":
                        query_list.append({"data": Keyword[i]})
            entries = userdb.viewer.query.find({"$and": query_list})

    entries = sorted(entries, key=lambda x: x["timeStamp"], reverse=True)

    for entry in entries:
        run_ids.append(entry["runId"])

    return run_ids


def query_keywords(Keywords, query_targets, Match):
    flug = 1
    if Keywords == "None" or Keywords == "":
        pass
    else:
        Keyword = Keywords.split(" ")
        Keyword_num = len(Keyword)
        AND_flug = 0
        OR_flug = 0
        for i in range(Keyword_num):
            if Keyword[i].upper() == "AND":
                AND_flug = 1
            if Keyword[i].upper() == "OR":
                OR_flug = 1

        if AND_flug == 1 and OR_flug == 1:
            pass
        elif AND_flug == 0 and OR_flug == 1:
            flug = 0
            for i in range(Keyword_num):
                for query_target in query_targets:
                    if Match == "partial":
                        if Keyword[i].upper() in str(query_target).upper():
                            flug = 1
                    if Match == "perfect":
                        if Keyword[i].upper() == str(query_target).upper():
                            flug = 1
        else:
            Keyword_flug = []
            num = []
            delete_num = []
            for i in range(Keyword_num):
                num.append(i)
                if Keyword[i].upper() == "AND":
                    delete_num.append(i)
                Keyword_flug.append(0)
                for query_target in query_targets:
                    if Match == "partial":
                        if Keyword[i].upper() in str(query_target).upper():
                            Keyword_flug[i] = 1
                    if Match == "perfect":
                        if Keyword[i].upper() == str(query_target).upper():
                            Keyword_flug[i] = 1
            query_num = list(set(num) - set(delete_num))
            for i in query_num:
                if Keyword_flug[i] == 0:
                    flug = 0
    return flug


def check_database_update():

    config_doc = userdb.viewer.query.find_one({"runId": "config"})
    #    query_list = [{"sys.cts": {"$gt":config_doc["sys"]["mts"]}},{"sys.mts": {"$gt":config_doc["sys"]["mts"]}}]
    #    append_docs = localdb.testRun.find({'$or':query_list}).sort([('startTime', -1)])
    querys = {"sys.cts": {"$gte": config_doc["sys"]["mts"]}}

    append_docs = list(localdb.testRun.find(querys).sort([("startTime", -1)]))

    for entry in append_docs:
        userdb.viewer.query.remove({"runId": str(entry["_id"])})
        docs = createScanCache(entry)
        userdb.viewer.query.insert_one(docs)
    userdb.viewer.query.update(
        {"runId": "config"}, {"$set": {"sys.mts": datetime.utcnow()}}
    )
    userdb.viewer.query.update(
        {"runId": "config"}, {"$set": {"sys.rev": config_doc["sys"]["rev"] + 1}}
    )

    return 0
