from functions.imports import *

# import module for metrology
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.ticker import MultipleLocator
from mpl_toolkits.mplot3d import axes3d
from scipy import linalg
import sys
import re
from io import open

component_api = Blueprint("component_api", __name__)

#######################
### route functions ###
#######################

########################
# display component page
@component_api.route("/component", methods=["GET", "POST"])
def show_component():
    initPage()

    session["this"] = request.args.get("id", None)
    session["collection"] = request.args.get("collection", "chip")
    session["code"] = request.args.get("code", "")
    session["test"] = request.args.get("test", "")
    session["runId"] = request.args.get("runId", None)
    session["unit"] = "front-end_chip"  ### TODO just temporary coding
    session["logged_in"] = session.get("logged_in",False)

    # component info
    info = setComponentInfo(session.get("this", None), session["collection"])

    # comment
    comments = setComments(session.get("this", None), session.get("runId",None), info)

    # check test items for the current stage
    latest_tests = setLatestQCSummary(session.get("this", None))
    latest_props = setLatestPropSummary(session.get("this", None))

    # make status of current stage
    try:
        selection_result = [ item["runId"] for item in latest_tests["results"]]
        selection_prop = [ item["runId"] for item in latest_props["results"]]
    except:
        selection_result = []
        selection_prop = []

    # set summary
    summary = setQCSummary(session.get("this", None))
    try:
        upload_status = localdb.QC.module.status.find_one( {"component":session.get("this", None),"proddbVersion":proddbv} )["upload_status"]
    except:
        upload_status = None

    # set results for QC tests
    with open("{}/json-lists/supported_test.json".format(VIEWER_DIR)) as f:
        qctest_item_list = json.load(f)
    qcresult_index = setQCResultIndex(session["this"], session["collection"])
    qcresults = setQCResults(session["this"], session["collection"], session["runId"])
    qctest = {
      "test_item":qctest_item_list,
      "resultIndex": qcresult_index,
      "results": qcresults
    }

    # set results for Module properties
    with open("{}/json-lists/module_prop.json".format(VIEWER_DIR)) as f:
        module_prop_list = json.load(f)
    moduleprop_index = setModulePropIndex(session["this"], session["collection"])
    modulepropresults = setModuleProp(session["this"], session["collection"], session["runId"])
    moduleprop = {
      "prop_item": module_prop_list,
      "propIndex": moduleprop_index,
      "properties": modulepropresults
    }

    # set results for electrical scans
    scan_result_index = setResultIndex(session["this"], session["collection"])
    scan_results = setResults(session["this"], session["collection"], session["runId"])
    plots = setPlots(session["this"], session["collection"], session["runId"], False)
    dcs = setDCS(session["runId"])
    electrical = {
      "resultIndex": scan_result_index,
      "results": scan_results,
      "dcs": dcs,
      "plots": plots
    }

    component = {
      "_id": session.get("this", None),
      "collection": session["collection"],
      "test": session["test"],
      "info": info,
      "comments": comments,
      "property": moduleprop,
      "electrical": electrical,
      "qctest":qctest,
      "summary": summary,
      "upload_status": upload_status,
      "latest_tests": latest_tests,
      "latest_props": latest_props,
      "selection_result": selection_result,
      "selection_prop": selection_prop
    }

    if info:
        component["upload_result"] = {}
        component["upload_result"]["uploading"] = os.path.exists(IF_DIR+"/doing_upload_" + info["name"] + ".txt")
        if os.path.exists(IF_DIR+"/doing_upload_" + info["name"] + "_fail" + ".txt"):
            with open(IF_DIR+"/doing_upload_" + info["name"] + "_fail" + ".txt") as f:
                component["upload_result"]["fail"] = [item.rstrip('\n') for item in f.readlines()]
    else:
        component["upload_result"] = None

    return render_template( "component.html", component=component, timezones=pytz.all_timezones )


@component_api.route("/nomodule_result", methods=["GET", "POST"])
def nomodule_result():
    initPage()

    session["collection"] = request.args.get("collection", "chip")
    session["code"] = request.args.get("code", "")
    session["test"] = request.args.get("test", "")
    session["runId"] = request.args.get("runId", None)
    session["unit"] = "front-end_chip"  ### TODO just temporary coding

    scan_results = setResults("", session["collection"], session["runId"])
    plots = setPlots("", session["collection"], session["runId"], False)
    dcs = setDCS(session["runId"])
    electrical = {
      "results": scan_results,
      "dcs": dcs,
      "plots": plots
    }

    component = {
      "electrical": electrical
    }


    return render_template("nomodule_result.html", component=component)


###################
# show summary plot
@component_api.route("/show_plot", methods=["GET"])
def show_plot():
    name = request.args.get("name")
    tr_oid = request.args.get("runId")

    filepath = "{0}/{1}/{2}_root.png".format(CACHE_DIR, tr_oid, name)

    image = Image.open(filepath)
    filename = "{0}_{1}_{2}_root.png".format(session["uuid"], tr_oid, name)
    image.save("{0}/{1}".format(THUMBNAIL_DIR, filename))
    url = url_for("thumbnail.static", filename=filename)

    return redirect(url)




###################
# show summary plot
@component_api.route("/show_summary", methods=["GET"])
def show_summary():
    initPage()

    # get from args
    code = request.args.get("code")
    scan = request.args.get("scan")
    stage = request.args.get("stage")

    query = {"_id": ObjectId(code)}
    data = localdb.fs.files.find_one(query)
    if not "png" in data["filename"]:
        filePath = "{0}/{1}/{2}_{3}_{4}.png".format(
            THUMBNAIL_DIR, session["uuid"], stage, scan, data["filename"]
        )
    else:
        filePath = "{0}/{1}/{2}_{3}_{4}".format(
            THUMBNAIL_DIR, session["uuid"], stage, scan, data["filename"]
        )

    thum_dir = "{0}/{1}".format(THUMBNAIL_DIR, session["uuid"])
    cleanDir(thum_dir)

    binary = fs.get(ObjectId(code)).read()
    image_bin = io.BytesIO(binary)
    image = Image.open(image_bin)
    image.save(filePath)
    if not "png" in data["filename"]:
        url = url_for(
            "thumbnail.static",
            filename="{0}/{1}_{2}_{3}.png".format(
                session["uuid"], stage, scan, data["filename"]
            ),
        )
    else:
        url = url_for(
            "thumbnail.static",
            filename="{0}/{1}_{2}_{3}".format(
                session["uuid"], stage, scan, data["filename"]
            ),
        )

    return redirect(url)


#######################
# Display JSON/DAT Data
@component_api.route("/display_data", methods=["GET"])
def display_data():
    initPage()

    code = request.args.get("code")
    data_format = request.args.get("format")
    if data_format == "json":
        data = jsonify(json.loads(fs.get(ObjectId(code)).read().decode("ascii")))
    elif data_format == "dat":
        data = fs.get(ObjectId(code)).read().decode("ascii")
    response = make_response(data)

    return response


###################
# show summary plot
@component_api.route("/display_module", methods=["GET"])
def display_module():
    module = ['apple', 'pine']
    return render_template(
        module = module
    )



########################
# Download JSON/DAT Data
@component_api.route("/download_data", methods=["GET", "POST"])
def download_data():
    initPage()
    cmp_name = request.form.get("cmp", None)
    run_number = request.form.get("run", "data")
    if cmp_name:
        zip_name = "{0}_{1}".format(cmp_name, run_number)
    else:
        zip_name = run_number

    zip_dir = "{0}/{1}".format(TMP_DIR, session["uuid"])
    cleanDir(zip_dir)
    zip_path = "{0}/{1}/{2}.zip".format(TMP_DIR, session["uuid"], zip_name)
    zip_file = zipfile.ZipFile(zip_path, "a")

    entries = request.form.getlist("data")
    for entry in entries:
        this = ast.literal_eval(entry)
        filename = "{0}.{1}".format(this["type"], this["format"])
        if not this["chip_name"] == "":
            filename = "{0}_{1}".format(this["chip_name"], filename)
        filepath = "{0}/{1}".format(zip_dir, filename)
        with open(filepath, "wb") as f:
            f.write(fs.get(ObjectId(this["code"])).read())
        zip_file.write(filepath, filename)
    zip_file.close()

    return send_file(zip_path, as_attachment=True, mimetype=ZIP_MIMETYPE)


##################
# Add/Edit Comment
@component_api.route("/edit_comment", methods=["GET", "POST"])
def edit_comment():
    oid = request.args.get("id", -1)
    tr_oid = request.args.get("runId", -1)

    thistime = datetime.utcnow()
    info = {
        "sys": {"rev": 0, "cts": thistime, "mts": thistime},
        "component": oid,
        "runId": tr_oid,
        "comment": request.form.get("text").replace("\r\n", "<br>"),
        "componentType": session["unit"],
        "collection": session["collection"],
        "user_id": session["user_id"],
        "name": request.form.get("text2"),
        "institution": request.form.get("text3"),
        "datetime": thistime,
    }

    localdb.comments.insert(info)

    return redirect(request.headers.get("Referer"))

@component_api.route("/analyze_scan", methods=["GET", "POST"])
def analyze_scan():

    initPage()
    table_docs = { "run":[] }
    table_docs["_id"] = request.args.get("id", "")
    table_docs["mname"] = str(localdb.component.find_one( {"_id":ObjectId(table_docs["_id"]) } )["name"])
    table_docs["stage"] = request.args.get("stage", "")
    table_docs["test"] = request.args.get("test", "")
    table_docs["text"] = ""
    table_docs["pstage"] = request.form.get("pstage", "input")
    if table_docs["test"] == "SOURCE_SCAN":
        table_docs["page"] = "source_scan"
    else:
        table_docs["page"] = "analyze_scan"
    table_docs["plots"] = []
    table_docs["temp"] = []
    table_docs["find"] = []

    ea = ElectricalAnalyzer(table_docs["mname"],table_docs["stage"])
    table_docs["scans"] = {}
    table_docs["scans"]["analysis_result"] = []
    table_docs["scans"].update(ea.getScanList(table_docs["test"]))
    table_docs["title"] = "Select scan results for QC result in this stage."

    if not session["logged_in"]:
        table_docs["pstage"] = "input"
        table_docs["text"] = "Please log in before the selection."
    elif ( ea.checkScanItems( None, table_docs["scans"]["scantypes"] ) ):
        table_docs["pstage"] = "input"
        table_docs["text"] = "All scan items for this stage have not been uploaded. " + "Ramaining items to be uploaded are " + ", ".join(ea.checkScanItems( None, table_docs["scans"]["scantypes"] ))
    else:
        if table_docs["pstage"] == "input":
            if ( ea.checkScanItems( table_docs["scans"]["setting_temp"], table_docs["scans"]["scantypes"] ) ):
                table_docs["text"] = "The DCS records of 'SETTING TEMPERATURE' have not uploaded. Listing all of this stage without filtering setting temperature."
                this_run_ids = ea.getScanIds(None)
            else:
                this_run_ids = ea.getScanIds(table_docs["scans"]["setting_temp"])
            table_docs["run"] = ea.createFullSummary( this_run_ids )

        elif table_docs["pstage"] == "confirm":
            this_run_ids = []
            for scantype in table_docs["scans"]["scantypes"]:
                tr_oid = request.form.get(scantype + "_id")
                this_run_ids.append(tr_oid)

            table_docs["run"] = ea.createPartSummary( this_run_ids )
            if not len(this_run_ids) == len(set(this_run_ids)):
                table_docs["pstage"] = "input"
                table_docs["text"] = "Please select test results so that each Id does not overlap."

            for this_run_id in this_run_ids:
                table_docs["plots"].append(setPlots(table_docs["_id"], "collection", this_run_id, True))

            commit_doc = ea.createElecTestDocs( this_run_ids, table_docs["scans"] )
            if table_docs["scans"]["analysis"]["run"]:
                 table_docs["scans"]["analysis_result"] = ea.getAnalysisResult( commit_doc )

        elif table_docs["pstage"] == "complete":
            this_run_ids = request.form.getlist("selectinfo")
            commit_doc = ea.createElecTestDocs( this_run_ids, table_docs["scans"] )
            localdb.QC.result.insert(commit_doc)

        elif table_docs["pstage"] == "scancomplete":
            this_run_ids = request.form.getlist("selectinfo")
            commit_doc = ea.createElecTestDocs( this_run_ids, table_docs["scans"] )
            localdb.QC.result.insert(commit_doc)

        elif table_docs["pstage"] == "source_confirm":
            this_run_ids = []
            for num in [1, 2, 3, 4]:
                tr_oid = request.form.get(str(num) + "_id")
                this_run_ids.append(tr_oid)

#            table_docs["run"] = ea.createSourceSummary( this_run_ids )
            if not len(this_run_ids) == len(set(this_run_ids)):
                table_docs["pstage"] = "input"
                table_docs["text"] = "Please select test results so that each Id does not overlap."

#            for this_run_id in this_run_ids:
#                table_docs["plots"].append(setPlots(table_docs["_id"], "collection", this_run_id, True))

            commit_doc = ea.createSourceTestDocs( this_run_ids, table_docs["scans"] )
            table_docs["run"] = commit_doc
#            if table_docs["scans"]["analysis"]["run"]:
#                 table_docs["scans"]["analysis_result"] = ea.getAnalysisResult( commit_doc )

        elif table_docs["pstage"] == "source_complete":
            this_run_ids = []
            for num in [1, 2, 3, 4]:
                tr_oid = request.form.get(str(num) + "_id")
                this_run_ids.append(tr_oid)

            commit_doc = ea.createSourceTestDocs( this_run_ids, table_docs["scans"] )
#            localdb.QC.result.insert(commit_doc)


    table_docs["total"] = len(table_docs["run"])
    return render_template( "selection.html", table_docs=table_docs, timezones=pytz.all_timezones )

@component_api.route("/select_test", methods=["GET", "POST"])
def select_test():

    initPage()

    table_docs = { "run":[] }
    table_docs["_id"] = request.args.get("id", "")
    table_docs["text"] = ""
    table_docs["stage"] = ""
    table_docs["pstage"] = request.form.get("pstage", "input")
    table_docs["page"] = "select_test"
    table_docs["title"] = "Sign off QC test in this stage"
    table_docs["scantypes"] = []
    table_docs["test_list"] = []
    with open("{}/json-lists/supported_test.json".format(VIEWER_DIR)) as f:
        supported_test = json.load(f)
    table_docs["supported"] = supported_test["test"]
    with open("{}/json-lists/module_prop.json".format(VIEWER_DIR)) as f:
        supported_prop = json.load(f)
    table_docs["prop"] = supported_prop["test"]

    this_cp = localdb.component.find_one( {"_id":ObjectId(table_docs["_id"]) } )
    table_docs["mname"] = str(this_cp["name"])

    qc_module_status = localdb.QC.module.status.find_one( { "component":str(this_cp["_id"]),"proddbVersion":proddbv } )
    table_docs["stage"] = qc_module_status["currentStage"]
    table_docs["scantypes"] = [ test for test in qc_module_status["QC_results"][table_docs["stage"]] ]
    table_docs["test_list"] = [ test for test in qc_module_status["QC_results"][table_docs["stage"]] ]

    if not session["logged_in"]:
        table_docs["pstage"] = "input"
        table_docs["text"] = "Please log in before the selection."
    elif ( not checkQCTestItems( table_docs["_id"], table_docs["stage"], table_docs["test_list"] ) ):
        table_docs["pstage"] = "input"
        table_docs["text"] = "All QC results for this stage do not exist. Please take all QC results."
    elif table_docs["stage"] not in supported_test["sign_off_stage"]:
        table_docs["pstage"] = "input"
        table_docs["text"] = "Signing off for this stage is not supported in the current setting."

    else:
        if table_docs["pstage"] == "input":
            result_ids = getResultIds( table_docs["mname"], table_docs["stage"] )
            table_docs["run"] = createQCtestSummary( result_ids )
            prop_ids = getPropIds( table_docs["mname"], table_docs["stage"] )
            table_docs["prop_run"] = createPropSummary( prop_ids )

        elif table_docs["pstage"] == "confirm":
            this_run_ids = []
            for scantype in table_docs["scantypes"]:
                tr_oid = request.form.get(scantype + "_id")
                this_run_ids.append(tr_oid)

            table_docs["run"] = createQCtestSummary( this_run_ids )

            this_prop_ids = []
            for proptype in table_docs["prop"]:
                pr_oid = request.form.get(proptype + "_id")
                this_prop_ids.append(pr_oid)

            table_docs["prop_run"] = createPropSummary( this_prop_ids )

        elif table_docs["pstage"] == "complete":
            this_run_ids = request.form.getlist("selectinfo")
            id_doc = {}
            n = 0
            for i,v in enumerate(table_docs["scantypes"]):
                if v in table_docs["supported"]:
                    id_doc[v] = this_run_ids[n]
                    n += 1
                else:
                    id_doc[v] = "-1"
            stage_result = "QC_results." + table_docs["stage"]
            upload_status = "upload_status." + table_docs["stage"]
            localdb.QC.module.status.update({"component":str(this_cp["_id"]),"proddbVersion":proddbv},{"$set":{"currentStage":getNextStage(this_cp["_id"], table_docs["stage"])}})
            localdb.QC.module.status.update({"component":str(this_cp["_id"]),"proddbVersion":proddbv},{"$set":{ stage_result: id_doc }})
            localdb.QC.module.status.update({"component":str(this_cp["_id"]),"proddbVersion":proddbv},{"$set":{ upload_status: "-1" }})

            if table_docs["stage"] == "MODULEWIREBONDING":
                this_prop_ids = request.form.getlist("selectprop")
                table_docs["test"] = this_prop_ids
                id_prop = {}
                n_p = 0
                for proptype in table_docs["prop"]:
                    id_prop[proptype] = this_prop_ids[n_p]
                    n_p += 1

                find = localdb.QC.prop.status.find_one({"component": str(this_cp["_id"])})
                stage_prop = "QC_properties"
                if find:
                    localdb.QC.prop.status.update({"component":str(this_cp["_id"]),"proddbVersion":proddbv},{"$set":{"currentStage":getNextStage(this_cp["_id"], table_docs["stage"])}})
                    localdb.QC.prop.status.update({"component":str(this_cp["_id"]),"proddbVersion":proddbv},{"$set":{ stage_prop: id_prop }})
                else:
                    prop_post = {
                        "proddbVersion": proddbv,
                        "component": str(this_cp["_id"]),
                        "currentStage": getNextStage(this_cp["_id"], table_docs["stage"]),
                        "status": "created",
                        "rework_stage": [],
                        stage_prop: id_prop
                    }
                    localdb.QC.prop.status.insert_one(prop_post)

    table_docs["total"] = len(table_docs["run"])
    return render_template( "selection.html", table_docs=table_docs, timezones=pytz.all_timezones )


@component_api.route("/set_stage", methods=["GET", "POST"])
def set_stage():

    if not session.get("logged_in", False):
        return render_template( "401.html" )

    module = request.args.get("module", "")
    nstage = request.form.get("nstage", "")
    pstage = request.form.get("pstage", "input")
    cstage = ""
    text = ""

    query = { "name": module }
    module_doc = localdb.component.find_one( query )

    query = { "component":str(module_doc["_id"]),"proddbVersion":proddbv }
    qc_module_doc = localdb.QC.module.status.find_one( query )
    stage_list = [s for s in qc_module_doc["QC_results"]]
    cstage = qc_module_doc["currentStage"]
    if pstage == "complete":
        if not nstage:
            text = "Please select stage from pull-down menue..."
            pstage = "input"
            doc = { "_id":str(module_doc["_id"]), "module": module, "pstage": pstage, "cstage": cstage, "nstege":nstage, "text": text, "stages":stage_list }
            return render_template("set_stage.html", doc = doc)
        localdb.QC.module.status.update_one( query, { "$set":{"currentStage":nstage} })
        pstage = "input"
        text = "The current stage was switched to " + nstage + '.'

    doc = { "_id":str(module_doc["_id"]),"module": module, "pstage": pstage, "cstage": cstage, "nstege":nstage, "text": text, "stages":stage_list }
    return render_template("set_stage.html", doc = doc)

@component_api.route("/customize_module", methods=["GET", "POST"])
def customize_module():

    if not session.get("logged_in", False):
        return render_template( "401.html" )

    module = request.args.get("module", "")
    nstage = request.form.get("nstage", "")
    pstage = request.form.get("pstage", "option")
    cstage = ""
    text = ""

    query = { "name": module }
    module_doc = localdb.component.find_one( query )

    query = { "component":str(module_doc["_id"]),"proddbVersion":proddbv }
    qc_module_doc = localdb.QC.module.status.find_one( query )
    stage_list = [s for s in qc_module_doc["QC_results"]]
    cstage = qc_module_doc["currentStage"]
    if pstage == "local_complete":
        if not nstage:
            text = "Please select stage from pull-down menue..."
            pstage = "local_input"
            doc = { "_id":str(module_doc["_id"]), "module": module, "pstage": pstage, "cstage": cstage, "nstege":nstage, "text": text, "stages":stage_list }
            return render_template("customize_moduleinfo.html", doc = doc)
        localdb.QC.module.status.update_one( query, { "$set":{"currentStage":nstage} })
        pstage = "local_input"
        text = "The current stage was switched to " + nstage + '.'
    if pstage == "itkpd_complete":
        code1 = request.form.get("code1", "")
        code2 = request.form.get("code2", "")
        flug = process_request(code1, code2)
        if flug == 0:
            pstage = "itkpd_input"
            text = "Not authorized. Please input correct codes."
        elif flug == 1:
            pstage = "itkpd_complete"
            try:
                ModuleStageDownloader(code1,code2).download_stage_info(module)
            except:
                text = "This component does not exist in ITkPD..."
                pstage = "download_input"


    doc = { "_id":str(module_doc["_id"]), "module": module, "pstage": pstage, "cstage": cstage, "nstege":nstage, "text": text, "stages":stage_list }
    return render_template("customize_moduleinfo.html", doc = doc)

@component_api.route("/result_transceiver", methods=["GET", "POST"])
def result_transceiver():
    doc = {}
    module = request.args.get("module", "")
    doc["_id"] = request.args.get("id", "")
    stage = request.form.get("stage", "select")
    text = ""

    query = { "name": module }
    module_doc = localdb.component.find_one( query )

    doc = { "_id":str(module_doc["_id"]),"mname": module, "stage": stage, "text": text }

    ####################
    ## Upload results ##
    if doc["stage"] == "upload_input":
        summary = {}
        uploaded_stage = []
        status_doc = localdb.QC.module.status.find_one( {"component": doc["_id"]} )

        for upload_stage in status_doc["upload_status"]:
            if status_doc["upload_status"][upload_stage] == "-1":
                summary[upload_stage] = status_doc["QC_results"][upload_stage]
            else:
                uploaded_stage.append(upload_stage)

        doc["upload_summary"] = summary
        doc["uploaded_stage"] = uploaded_stage

    if doc["stage"] == "upload_complete":
        code1 = request.form.get("code1", "")
        code2 = request.form.get("code2", "")
        flug = process_request(code1, code2)
        if flug == 0:
            doc["stage"] = "upload_input"
            doc["text"] = "Not authorized. Please input correct codes."
        elif flug == 1:
            doc["stage"] = "upload_complete"
            dic1 = {"msn": doc["mname"]}
            thread_1 = threading.Thread(target=QCResultUploader(code1,code2).upload_results, kwargs=dic1)
            thread_1.start()
#            QCResultUploader(code1,code2).upload_results(doc["mname"])

    ######################
    ## Download results ##
    if doc["stage"] == "download_complete":
        code1 = request.form.get("code1", "")
        code2 = request.form.get("code2", "")
        flug = process_request(code1, code2)
        if flug == 0:
            doc["stage"] = "download_input"
            doc["text"] = "Not authorized. Please input correct codes."
        elif flug == 1:
            doc["stage"] = "download_complete"
            try:
                doc["download_list"] = QCResultDownloader(code1,code2).download_results(doc["mname"])
            except:
                doc["text"] = "This component does not exist in ITkPD..."
                doc["stage"] = "download_input"

    return render_template( "result_tranceiver.html", doc = doc )

@component_api.route("/download_rootfile", methods=["GET", "POST"])
def download_rootfile():
    root_path = request.form.get("path","")
    return send_file(root_path, as_attachment=True, mimetype=ROOT_MIMETYPE)

#######################
### other functions ###
#######################

###########################
# Set Component Information
# i_oid: ObjectId of this component/chip document
# i_col: collection name (component or chip)
def setComponentInfo(i_oid, i_col):
    if not i_oid:
        return {}
    ### parent
    parents = setCpr(i_oid, "child", "parent")
    ### child
    children = setCpr(i_oid, "parent", "child")
    ### component
    query = {"_id": ObjectId(i_oid)}
    this = localdb[i_col].find_one(query)
    qc_doc = localdb.QC.module.status.find_one( { "component": i_oid,"proddbVersion":proddbv } )
    if qc_doc == None: qc_doc = {}
    session["unit"] = this["componentType"].lower()  ### TODO wakarinikui
    docs = {
        "name": this["name"],
        "stage": qc_doc.get("currentStage","..."),
        "parents": parents,
        "children": children,
        "type": this["componentType"].lower(),
        "chipType": this["chipType"],
        "qc": this.get("proDB",False),
        "proID": this.get("proID",False)
    }
    return docs

###########################
# Set Child Parent Relation
# i_oid: ObjectId of this component/chip document
# i_cp_in: set this component to child or parent
# i_cp_out: get child or parent of this component
def setCpr(i_oid, i_cp_in, i_cp_out):
    docs = []
    query = {i_cp_in: i_oid}
    entries = localdb.childParentRelation.find(query)
    oids = []
    for entry in entries:
        oids.append(entry[i_cp_out])
    for oid in oids:
        query = {"_id": ObjectId(oid)}
        this = localdb.component.find_one(query)
        docs.append({"_id": str(oid), "collection": "component", "name": this["name"]})
    return docs

def setQCSummary(i_oid):
    summary = {}
    doc = localdb.QC.module.status.find_one( {"component":i_oid,"proddbVersion":proddbv} )

    if doc == None:
        return summary

    cstage = doc["currentStage"]
    for result in doc["QC_results"]:
        if result == cstage: break
        summary[result] = doc["QC_results"][result]
    return summary

def setLatestQCSummary(i_oid):

    tests = {}
    doc = localdb.QC.module.status.find_one( {"component":i_oid,"proddbVersion":proddbv} )
    if doc == None:
        return tests

    mname = localdb.component.find_one( {"_id":ObjectId(i_oid)} )["name"]
    cstage = doc["currentStage"]
    tests["stage"] = cstage
    tests["results"] = []
    etests = ElectricalAnalyzer( mname, cstage ).test_scan_map
    try:
        for test in doc["QC_results"][cstage]:
            test_docs = localdb.QC.result.find({"$and":[{"component":i_oid},{"currentStage":cstage},{"testType":test}]}).sort([("$natural", -1)]).limit(1)
            test_doc = {}
            for doc in test_docs:
                test_doc = doc
            tests["results"].append({
              "name"           :test,
              "runId"          :str(test_doc["_id"])  if test_doc != {} else "",
              "scan_selection" :True                  if test in etests else False
            })
    except:
        pass
    return tests

def setLatestPropSummary(i_oid):

    tests = {}
    doc = ["RD53A_PULL-UP_RESISTOR", "IREFTRIM_FE", "ORIENTATION"]
    if doc == None:
        return tests

    mname = localdb.component.find_one( {"_id":ObjectId(i_oid)} )["name"]
    tests["results"] = []
    for test in doc:
        test_docs = localdb.QC.module.prop.find({"$and":[{"component":i_oid},{"testType":test}]}).sort([("$natural", -1)]).limit(1)
        test_doc = {}
        for item in test_docs:
            test_doc = item
        tests["results"].append({
          "name"           :test,
          "runId"          :str(test_doc["_id"])  if test_doc != {} else "",
        })
    return tests

#################
# Set Result List
# i_oid: ObjectId of this component/chip document
# i_col: collection name (component or chip)
def setResultIndex(i_oid, i_col):
    docs = []
    query = {i_col: i_oid, "dbVersion":dbv}
    entries = localdb.componentTestRun.find(query)
    oids = []
    for entry in entries:
        oids.append(str(entry["_id"]))
    for oid in oids:
        query = {"_id": ObjectId(oid)}
        this_ctr = localdb.componentTestRun.find_one(query)
        query = {"_id": ObjectId(this_ctr["testRun"])}
        this_tr = localdb.testRun.find_one(query)

        ### result data
        result = (not this_tr["plots"] == [] and this_ctr["chip"] == "module") or (
            not this_ctr["attachments"] == [] and not this_ctr["chip"] == "module"
        )
        ### user
        query = {"_id": ObjectId(this_tr["user_id"])}
        this_user = localdb.user.find_one(query)
        ### site
        query = {"_id": ObjectId(this_tr["address"])}
        this_site = localdb.institution.find_one(query)
        ### score
        count = setCount()

        docs.append(
            {
                "_id": str(this_tr["_id"]),
                "runNumber": this_tr["runNumber"],
                "datetime": setTime(
                    this_tr["startTime"], session.get("timezone", str(get_localzone()))
                ),
                "testType": this_tr["testType"],
                "result": result,
                "stage": this_tr.get("stage", "..."),
                "user": this_user["userName"],
                "site": this_site["institution"],
                "rate": count.get("module", {}).get("rate", "-"),
                "score": count.get("module", {}).get("score", None),
                "values": count.get("module", {}).get("parameters", {}),
                "summary": this_tr.get("summary"),
            }
        )

    docs = sorted(docs, key=lambda x: (x["datetime"]), reverse=True)

    return docs


##############################
# Set QC Result List
# i_oid: ObjectId of this component/chip document
# i_col: collection name (component or chip)
def setQCResultIndex(i_oid, i_col):
    docs = []
    query = { "component": i_oid, "dbVersion":dbv }
    entries = localdb.QC.result.find(query)
    oids = []

    for entry in entries:
        oids.append(str(entry["_id"]))

    for oid in oids:
        query = {"_id": ObjectId(oid)}
        this_tr = localdb.QC.result.find_one(query)
        this_status = localdb.QC.module.status.find_one({"component":this_tr["component"],"proddbVersion":proddbv})
        try:
            QC_result = (oid == this_status["QC_results"][this_tr["stage"]][this_tr["testType"]])
        except:
            QC_result = False

        ### result data of non-electrical test
        docs.append(
            {
                "component_id": str(this_tr["component"]),
                "_id": str(this_tr["_id"]),
                "testType": this_tr["testType"],
                "user": this_tr["user"],
                "institute": this_tr["address"],
                "datetime": setTime(
                            this_tr["sys"]["mts"], session.get("timezone", str(get_localzone()))),
                "currentStage": this_tr.get("currentStage", "..."),
                "QC_result": QC_result
            }
        )

    docs = sorted(docs, key = lambda x: (x["datetime"]), reverse=True)

    return docs


###############################
# Set Result Information & Data
# i_oid: ObjectId of this component/chip document
# i_col: collection name (component or chip)
# i_tr_oid: ObjectId of this testRun document
def setResults(i_oid, i_col, i_tr_oid):
    if not i_tr_oid:
        return {}

    query = {"_id": ObjectId(i_tr_oid)}
    this_tr = localdb.testRun.find_one(query)
    if this_tr == None:
        return {}
    ### output data
    outputs = {}
    outputs = setOutput(this_tr, outputs)
    query = {"testRun": i_tr_oid}
    ctr_entries = localdb.componentTestRun.find(query)
    cmps = []
    for this_ctr in ctr_entries:
        outputs = setOutput(this_ctr, outputs)
        cmps.append(
            {
                "name": this_ctr["name"],
                "_id": this_ctr[i_col]
                if not this_ctr[i_col] == "module"
                else this_ctr["component"],
                "collection": i_col if not this_ctr[i_col] == "module" else "component",
            }
        )
    ### basic info
    info = setTestRunInfo(this_tr, cmps)

    results = {"runId": i_tr_oid, "info": info, "output": outputs}

    return results


##############################################
# Set QC Result Information & Data
# i_old: ObjectId of this component/chip document
# i_col: collection name (component or chip)
# i_tr_old: ObjectId of this testRun document
def setQCResults(i_oid, i_col, i_tr_oid):
    if not i_tr_oid:
        return {}

    query = {"_id": ObjectId(i_tr_oid)}
    this_tr = localdb.QC.result.find_one(query)
    if this_tr == None:
        return {}
    query = {"_id": ObjectId(this_tr["component"])}
    this_cp = localdb.component.find_one(query)

    results = {}
    results = {
      "testType"     : this_tr["testType"],
      "currentStage" : this_tr["currentStage"],
      "component"    : this_tr["component"],
      "datetime"     : setTime( this_tr["sys"]["mts"], session.get("timezone", str(get_localzone()))),
      "user"         : this_tr["user"],
      "institute"    : this_tr["address"],
      "runId"        : i_tr_oid
    }

    if this_tr["results"] == {}:
        return results

    ## For electrical tests
    ea = ElectricalAnalyzer( this_cp["name"], this_tr["currentStage"] )

    if "OPTICAL" in this_tr["testType"]:
        ### output data
        pictures = this_tr["results"]["img_tile"]
        pics = {}
        for i in pictures:
            pic = pictures[i]
            data = fs.get( ObjectId( str(pic)) ).read()
            code_base64 = base64.b64encode(data).decode()
            pic = bin2image("png", code_base64)
            pics[i] = pic

        results.update({
          "results": this_tr["results"],
          "anomalyNum": {
                         "anomaly": len(this_tr["results"]["anomaly"]),
                         "comments": len(this_tr["results"]["comment"]),
                         "image": len(this_tr["results"]["img_tile"])
                        },
          "img": pics
        })

    elif this_tr["testType"] == "WIREBONDING":
        try:
            fsb = gridfs.GridFSBucket(localdb)
            bond_file = open('static/Bonding_Program.dat','wb+')
            fsb.download_to_stream(ObjectId(this_tr["results"]["property"]["Bond_program"]), bond_file)
            bond_file.seek(0)
            contents = bond_file.read()
            this_tr["results"]["property"]["Bond_program"] = 'static/Bonding_Program.dat'
        except:
            this_tr["results"]["property"]["Bond_program"] = ""

        results.update({
            "results": this_tr["results"],
        })

    elif this_tr["testType"] in ea.test_scan_map:
        plots = { d["name"]: setPlots(this_tr["component"], "collection", d["runId"], True) for d in this_tr["results"]["scans"] }
        if ea.test_scan_map[this_tr["testType"]]["analysis"]["run"]:
            analysis = ea.getAnalysisResult( this_tr )
        else:
            analysis = {}
        results.update({"results":this_tr["results"]["scans"], "plots": plots, "analysis_result": analysis })

    elif this_tr["testType"] == "SENSOR_IV" or this_tr["testType"] == "SENSOR_IV_min15_DEGREE" or this_tr["testType"] == "SENSOR_IV_20_DEGREE" or this_tr["testType"] == "SENSOR_IV_30_DEGREE":
         results.update({
           "results": this_tr["results"],
           "graph": SensorIV_graph(this_tr),
           "unit_list": SensorIV_units(this_tr)
         })

    elif this_tr["testType"] == "SLDO_VI":
         results.update({
           "results": this_tr["results"],
           "graph": SLDOVI_graph(this_tr),
           "unit_list": SLDOVI_units(this_tr)
         })

    else :
         results.update({
           "results": this_tr["results"]
         })

    return results



##############################################
# Set Module Property List
# i_oid: ObjectId of this component/chip document
# i_col: collection name (component or chip)
def setModulePropIndex(i_oid, i_col):
    docs = []
    query = { "component": i_oid, "dbVersion":dbv }
    entries = localdb.QC.module.prop.find(query)
    oids = []

    for entry in entries:
        oids.append(str(entry["_id"]))

    for oid in oids:
        query = {"_id": ObjectId(oid)}
        this_tr = localdb.QC.module.prop.find_one(query)
        this_status = localdb.QC.module.status.find_one({"component":this_tr["component"],"proddbVersion":proddbv})
        try:
            QC_result = (oid == this_status["QC_results"][this_tr["stage"]][this_tr["testType"]])
        except:
            QC_result = False

        ### result data of non-electrical test
#            docs[this_tr["testType"]] = {}
#            docs[this_tr["testType"]]["component_id"] = str(this_tr["component"])
#            docs[this_tr["testType"]]["_id"] = str(this_tr["_id"])
#            docs[this_tr["testType"]]["testType"] = this_tr["testType"]
#            docs[this_tr["testType"]]["user"] = this_tr["user"]
#            docs[this_tr["testType"]]["institute"] = this_tr["address"]
#            docs[this_tr["testType"]]["datetime"] = setTime(this_tr["sys"]["mts"], session.get("timezone", str(get_localzone())))
#            docs[this_tr["testType"]]["currentStage"] = this_tr.get("currentStage", "...")
#            docs[this_tr["testType"]]["QC_result"] = QC_result

        docs.append(
            {
                "component_id": str(this_tr["component"]),
                "_id": str(this_tr["_id"]),
                "testType": this_tr["testType"],
                "user": this_tr["user"],
                "institute": this_tr["address"],
                "datetime": setTime(
                            this_tr["sys"]["mts"], session.get("timezone", str(get_localzone()))),
                "currentStage": this_tr.get("currentStage", "..."),
                "QC_result": QC_result
            }
        )
        docs = sorted(docs, key = lambda x: (x["datetime"]), reverse=True)

    return docs


##############################################
# Set Module Property
# i_old: ObjectId of this component/chip document
# i_col: collection name (component or chip)
# i_tr_old: ObjectId of this testRun document
def setModuleProp(i_oid, i_col, i_tr_oid):
    if not i_tr_oid:
        return {}

    query = {"_id": ObjectId(i_tr_oid)}
    this_tr = localdb.QC.module.prop.find_one(query)
    if this_tr == None:
        return {}
    query = {"_id": ObjectId(this_tr["component"])}
    this_cp = localdb.component.find_one(query)

    localdbtools_db = client.localdbtools
    module_config = localdbtools_db["QC.module.types"].find_one()
    triplet_list = {}
    triplet_list["STAVE"] = []
    triplet_list["RING"] = []
    for i in range(len(module_config["types"])):
        if "TRIPLET" in module_config["types"][i]["code"]:
            if "STAVE" in module_config["types"][i]["code"]:
                triplet_list["STAVE"].append(module_config["types"][i]["subprojects"][0]["code"] + module_config["types"][i]["snComponentIdentifier"])
            if "RING" in module_config["types"][i]["code"]:
                triplet_list["RING"].append(module_config["types"][i]["subprojects"][0]["code"] + module_config["types"][i]["snComponentIdentifier"])

    results = {}
    results =  {
      "testType"     : this_tr["testType"],
      "currentStage" : this_tr["currentStage"],
      "component"    : this_tr["component"],
      "datetime"     : setTime( this_tr["sys"]["mts"], session.get("timezone", str(get_localzone()))),
      "user"         : this_tr["user"],
      "institute"    : this_tr["address"],
      "runId"        : i_tr_oid,
      "triplet"      : triplet_list
    }

    results.update({
      "results": this_tr["results"]
    })

    return results

########################
# Set Result Information
# i_tr: this testRun document
# i_cmps: component list of this testRun
def setTestRunInfo(i_tr, i_cmps):
    info = {
        "runNumber": None,
        "testType": None,
        "stage": None,
        "component": i_cmps,
        "startTime": None,
        "finishTime": None,
        "user": None,
        "site": None,
        "targetCharge": None,
        "targetTot": None,
    }
    for key in i_tr:
        if (
            "Cfg" in key
            or key == "_id"
            or key == "sys"
            or key == "chipType"
            or key == "dbVersion"
            or key == "timestamp"
        ):
            continue
        elif key == "startTime" or key == "finishTime":
            info.update(
                {key: setTime(i_tr[key], session.get("timezone", str(get_localzone())))}
            )
        elif key == "user_id" and not i_tr[key] == "...":
            query = {"_id": ObjectId(i_tr[key])}
            this_user = localdb.user.find_one(query)
            info.update({"user": this_user["userName"]})
        elif key == "address" and not i_tr[key] == "...":
            query = {"_id": ObjectId(i_tr[key])}
            this_site = localdb.institution.find_one(query)
            info.update({"site": this_site["institution"]})
        elif type(i_tr[key]) == type([]):
            value = ""
            for i in i_tr[key]:
                value += "{}<br>".format(i)
            info.update({key: value})
        elif type(i_tr[key]) == type({}):
            value = ""
            for i in i_tr[key]:
                value += "{0}: {1}<br>".format(i, i_tr[key][i])
            info.update({key: value})
        else:
            info.update({key: i_tr[key]})

    return info


#################
# Set Result Data
# i_docs: this testRun/componentTestRun document
# i_files: result data file list
def setOutput(i_docs, i_files):
    for key in i_docs:
        if "Cfg" in key and not i_docs[key] == "...":
            if not key in i_files:
                i_files.update({key: []})
            query = {"_id": ObjectId(i_docs[key])}
            this = localdb.config.find_one(query)
            i_files[key].append(
                {
                    "type": key,
                    "code": this["data_id"],
                    "chip_name": i_docs.get("name", ""),
                    "format": "json",
                }
            )
    for this in i_docs.get("attachments", []):
        if not this["title"] in i_files:
            i_files.update({this["title"]: []})
        i_files[this["title"]].append(
            {
                "type": this["title"],
                "code": this["code"],
                "chip_name": i_docs.get("name", ""),
                "format": this["contentType"],
            }
        )
    return i_files


##############
# Set DCS Plot
def setDCS(i_tr_oid):
    docs = {}
    if not i_tr_oid:
        return docs

    query = {"_id": ObjectId(i_tr_oid)}
    this_tr = localdb.testRun.find_one(query)
    if this_tr == None:
        return docs

    docs.update({"dcs_data_exist": False, "environment": "..."})
    if not i_tr_oid:
        return docs
    if this_tr.get("environment", False):
        docs.update({"dcs_data_exist": True})
    return docs


###########
# Set Score
def setCount():  # TODO ./dev/component.py
    docs = {}
    return docs


##############################
# Set Result Plots by ROOT CXX
def setPlots(i_oid, i_col, i_tr_oid, selection):  # TODO ./dev/component.py
    plots = {}
    if not i_tr_oid:
        return plots

    if localdb.testRun.find_one({"_id": ObjectId(i_tr_oid)}) == None:
        return plots

    plot_root.retrieveFiles(localdb, i_tr_oid, selection)
    if plot_root.plotRoot("{}/plotting-tool".format(VIEWER_DIR), i_tr_oid) == 1:
        plots.update({"rootsw": False})
        return plots
    else:
        plots.update({"rootsw": True})

    results = []
    query = {"_id": ObjectId(i_tr_oid)}
    this_tr = localdb.testRun.find_one(query)
    for maptype in this_tr.get("plots", []):
        if not selection or maptype.split("-")[0] in DATA_SELECTION_LIST[this_tr["testType"]]:
            result = {
                "mapType": maptype,
                "sortkey": "{}0".format(maptype),
                "runId": i_tr_oid,
                "chips": {},
            }
            for filename in sorted(os.listdir("{0}/{1}".format(CACHE_DIR, i_tr_oid)), key=str.lower):
                if (
                    not "_{}_".format(maptype.replace("-", "_")) in filename
                    or not "png" in filename
                ):
                    continue
                chip_name = filename[: filename.find(maptype.replace("-", "_")) - 1]
                if not chip_name in result["chips"]:
                    result["chips"].update({chip_name: {"num": 0, "plots": []}})
                binary_image = open(
                    "{0}/{1}/{2}".format(CACHE_DIR, i_tr_oid, filename), "rb"
                )
                code_base64 = base64.b64encode(binary_image.read()).decode()
                binary_image.close()
                plot = {"url": bin2image("png", code_base64), "name": filename}
                result["chips"][chip_name]["plots"].append(plot)
            for chip_name in result["chips"]:
                result["chips"][chip_name]["num"] = len(result["chips"][chip_name]["plots"])
                result["chips"][chip_name]["width"] = 90
                result["chips"][chip_name]["length"] = (
                    len(result["chips"][chip_name]["plots"])
                    * result["chips"][chip_name]["width"]
                )
            results.append(result)
            compTestRun = localdb.componentTestRun.find({"testRun" : i_tr_oid})
            for item in compTestRun:
                if "scanSN" in item:
                    if item["chip"] != "module" and item["scanSN"] in result["chips"]:
                        result["chips"][item["name"]] = result["chips"].pop(item["scanSN"], None)

#    results = sorted(
#        results,
#        key=lambda x: int((re.search(r"[0-9]+", x["sortkey"])).group(0)),
#        reverse=True,
#    )

    plots.update({"results": results})

    ### for JSROOT
    root_path = "{0}/{1}/rootfile.root".format(CACHE_DIR, i_tr_oid)
    jsroot_path = "{0}/{1}".format(JSROOT_DIR, i_tr_oid)
    if os.path.isfile(root_path) and not os.path.isdir(jsroot_path):
        os.mkdir(jsroot_path)
        shutil.copy(root_path, "{}/rootfile.root".format(jsroot_path))
    if os.path.isfile("{}/rootfile.root".format(jsroot_path)):
        plots.update({ "jsroot": { "path": "./jsroot/{}/".format(i_tr_oid), "files": "rootfile.root" }})

    return plots


##############
# Set Comments
# i_oid: ObjectId of this component/chip document
# i_tr_oid: ObjectId of this testRun document
# i_info: component information
def setComments(i_oid, i_tr_oid, i_info):
    docs = []
    query = []
    if i_tr_oid:
        query.append({ 'runId': i_tr_oid })
    if i_oid:
        query.append({ "component": i_oid })
        for child in i_info.get("children", []):
            query.append({"component": child["_id"]})
        for parent in i_info.get("parents", []):
            query.append({"component": parent["_id"]})

    if not query:
        return docs

    entries = localdb.comments.find({"$or": query})
    for entry in entries:
        docs.append(entry)

    return docs

### Others ###

def getResultIds(mname, stage):

    run_ids = []
    this_cp = localdb.component.find_one( { "name":mname } )
    if this_cp != None:
        results = localdb.QC.result.find({"$and": [{ "component": str(this_cp["_id"]) }, {"currentStage":stage} ]},{ "_id":1 } )
        run_ids = [ str(result["_id"]) for result in results ]
    return run_ids

def getPropIds(mname, stage):

    run_ids = []
    this_cp = localdb.component.find_one( {"name":mname} )
    if this_cp != None:
        results = localdb.QC.module.prop.find({"$and": [{ "component": str(this_cp["_id"]) }, {"currentStage": "MODULEWIREBONDING"} ]},{ "_id":1 } )
        run_ids = [ str(result["_id"]) for result in results ]
    return run_ids

def checkQCTestItems(c_oid, stage, test_list):
    with open("{}/json-lists/supported_test.json".format(VIEWER_DIR)) as f:
        supported_test = json.load(f)
    for testType in test_list:
        test_doc = localdb.QC.result.find_one({"$and":[{"component": c_oid},{"currentStage":stage},{"testType":testType}]})
        if testType in supported_test["test"]:
            if not test_doc: return False

    return True

def createQCtestSummary( run_ids ):
    list_ = []

    nrun_entries = len(run_ids)
    if not nrun_entries == 0:
        # for i in run_num_list:
        for i in range(nrun_entries):
            if run_ids[i] == None:
                list_.append({ "run_data": {} })
                continue

            query = {"_id": ObjectId(run_ids[i])}
            this_run = localdb.QC.result.find_one(query)

            ### user
            user_name = this_run["user"]

            ### site
            site_name = this_run["address"]

            ### component
            this_cp = localdb.component.find_one( {"_id": ObjectId(this_run["component"]) } )
            component = this_cp["name"]

            ### tags
            query = {}
            testRun_tag_candidates = userdb.viewer.tag.categories.find(query)
            tag_candidate = []
            for testRun_tag in testRun_tag_candidates:
                tag_candidate.append(testRun_tag)
            ### put tags
            query = {"runId": run_ids[i]}
            testRun_tags = userdb.viewer.tag.docs.find(query)
            tag = []
            for testRun_tag in testRun_tags:
                tag.append(testRun_tag)

            run_data = {
                "_id": run_ids[i],
                "datetime": setTime(
                    this_run["sys"]["cts"], session.get("timezone", str(get_localzone()))
                ),
                "testType": this_run["testType"],
                "runNumber": 0,
                "stage": this_run["currentStage"],
                "component": component,
                "user": user_name,
                "site": site_name,
                "testRun_tag_candidate": tag_candidate,
                "testRun_tag": tag,
            }

            list_.append( { "run_data": run_data } )

    #list_ = sorted( list_, key=lambda x: x["run_data"]["datetime"], reverse=True )
    return list_

def createPropSummary( run_ids ):
    list_ = []
    nrun_entries = len(run_ids)
    if not nrun_entries == 0:
        # for i in run_num_list:
        for i in range(nrun_entries):
            if run_ids[i] == None:
                list_.append({ "run_data": {} })
                continue

            query = {"_id": ObjectId(run_ids[i])}
            this_run = localdb.QC.module.prop.find_one(query)

            ### user
            user_name = this_run["user"]

            ### site
            site_name = this_run["address"]

            ### component
            this_cp = localdb.component.find_one( {"_id": ObjectId(this_run["component"]) } )
            component = this_cp["name"]

            ### tags
            query = {}
            testRun_tag_candidates = userdb.viewer.tag.categories.find(query)
            tag_candidate = []
            for testRun_tag in testRun_tag_candidates:
                tag_candidate.append(testRun_tag)
            ### put tags
            query = {"runId": run_ids[i]}
            testRun_tags = userdb.viewer.tag.docs.find(query)
            tag = []
            for testRun_tag in testRun_tags:
                tag.append(testRun_tag)

            run_data = {
                "_id": run_ids[i],
                "datetime": setTime(
                    this_run["sys"]["cts"], session.get("timezone", str(get_localzone()))
                ),
                "testType": this_run["testType"],
                "runNumber": 0,
                "stage": this_run["currentStage"],
                "component": component,
                "user": user_name,
                "site": site_name,
                "testRun_tag_candidate": tag_candidate,
                "testRun_tag": tag,
            }

            list_.append( { "run_data": run_data } )

    #list_ = sorted( list_, key=lambda x: x["run_data"]["datetime"], reverse=True )
    return list_

def getNextStage(module_id, stage):
    qc_status = localdb.QC.module.status.find_one({"component" : str(module_id)})
    stage_list = [item for item in qc_status["QC_results"]]
    nextStage = stage_list[stage_list.index(stage)+1]
    return nextStage


####################################
# results for Sensor_IV & SLDO_VI
def SensorIV_graph(sensorIVresult):
    graph = layout_ModuleQC(sensorIVresult, "Sensor_IV")

    pngImage = io.BytesIO()
    FigureCanvas(graph).print_png(pngImage)

    # Encode PNG image to base64 string
    pngImageB64String = "data:image/png;base64,"
    pngImageB64String += base64.b64encode(pngImage.getvalue()).decode('utf8')

    return pngImageB64String

def SensorIV_units(sensorIVresult):
    units = {}
    try:
        units["V_step"] = sensorIVresult["results"]["unit"]["Voltage"]
        units["Step_duration"] = sensorIVresult["results"]["unit"]["Time"]
        units["N_measurements_per_step"] = ""
        units["I_Compliance"] = sensorIVresult["results"]["unit"]["Current"]
        units["Temperature"] = sensorIVresult["results"]["unit"].get("Temperature", "no unit")
        units["Humidity"] = sensorIVresult["results"]["unit"].get("Humidity", "no unit")
        units["comment"] = ""
        return units
    except Exception as E:
        logger.warning(E)
        return units

def SLDOVI_graph(sensorIVresult):
    graph = layout_ModuleQC(sensorIVresult, "SLDO_VI")

    pngImage = io.BytesIO()
    FigureCanvas(graph).print_png(pngImage)

    # Encode PNG image to base64 string
    pngImageB64String = "data:image/png;base64,"
    pngImageB64String += base64.b64encode(pngImage.getvalue()).decode('utf8')

    return pngImageB64String

def SLDOVI_units(sensorIVresult):
    units = {}
    try:
        units["I_step"] = sensorIVresult["results"]["unit"]["Current"]
        units["N_iteration"] = ""
        units["N_measurements_per_step"] = ""
        units["Voltage_limit"] = sensorIVresult["results"]["unit"]["Voltage"]
        units["Temperature"] = sensorIVresult["results"]["unit"].get("Temperature", "no unit")
        units["Humidity"] = sensorIVresult["results"]["unit"].get("Humidity", "no unit")
        units["comment"] = ""
        return units
    except Exception as E:
        logger.warning(E)
        return units



##########################
## For selecting functions for electrical tests
class ElectricalAnalyzer:

    def __init__( self, module_name, stage_name ):
         self.mname = module_name
         self.sname = stage_name
         with open("{}/json-lists/scan_list.json".format(VIEWER_DIR)) as f:
             self.test_scan_map = json.load(f)

    def getScanFromCtr(self, temp):
        ctr_ids = localdb.componentTestRun.find({"name": self.mname}, {"testRun": 1, "_id": 0})
        tr_querys = []
        for ctr_id in ctr_ids:
            if temp == None or self.getModuleNtcTemp(ctr_id["testRun"]) == str(temp):
                tr_querys.append({"_id": ObjectId(ctr_id["testRun"])})
        return tr_querys

    def getScanIds(self, temp):
        run_ids = []
        tr_querys = self.getScanFromCtr(temp)
        if not tr_querys == [] or self.sname == None:
            query = {"$and": [{"$or": tr_querys}, {"stage": self.sname}]}
            tr_docs = localdb.testRun.find(query)
            tr_docs = sorted(tr_docs, key=lambda x: x["timestamp"], reverse=True)
            for tr_doc in tr_docs:
                run_ids.append(str(tr_doc["_id"]))

        return run_ids

    def checkScanItems(self, temp, test_list):
        tr_querys = self.getScanFromCtr(temp)
        if not tr_querys: return test_list
        needed_scan = []
        for testtype in test_list:
            query = {"$and": [{"$or": tr_querys}, {"stage": self.sname}, {"testType": testtype}]}
            tr_docs = localdb.testRun.find_one( query )
            if tr_docs == None: needed_scan.append(testtype)

        return needed_scan

    def createFullSummary(self, run_ids ):
        list_ = []
        nrun_entries = len(run_ids)
        run_counts = 0
        if not nrun_entries == 0:
            # for i in run_num_list:
            for i in range(nrun_entries):
                run_data = getScanSummary(run_ids[i])
                run_data["temp"] = self.getModuleNtcTemp(run_ids[i])
                run_counts += 1
                list_.append( { "run_data": run_data, "nrun": run_counts } )

        #list_ = sorted( list_, key=lambda x: x["run_data"]["datetime"], reverse=True )
        return list_

    def createPartSummary( self, run_ids ):
        list_ = []
        for tr_oid in run_ids:
            query = {"_id": ObjectId(tr_oid)}
            this_run = localdb.testRun.find_one(query)
            testType = this_run["testType"]
            rn = this_run["runNumber"]

            list_.append({
                      "_id": tr_oid,
                      "testType": testType,
                      "time": setTime(
                          this_run["startTime"],
                          session.get("timezone", str(get_localzone())),
                      ),
                      "rn"  : rn,
                      'temp': self.getModuleNtcTemp( tr_oid )
                    })
        return list_

    def createSourceSummary( self, run_ids ):
        list_ = []
        for tr_oid in run_ids:
            query = {"_id": ObjectId(tr_oid)}
            this_run = localdb.testRun.find_one(query)
            testType = this_run["testType"]
            rn = this_run["runNumber"]

            list_.append({
                      "_id": tr_oid,
                      "testType": testType,
                      "time": setTime(
                          this_run["startTime"],
                          session.get("timezone", str(get_localzone())),
                      ),
                      "rn"  : rn,
                      'temp': self.getModuleNtcTemp( tr_oid )
                    })
        return list_

    def createElecTestDocs( self, run_ids, scan_doc ):
        this_cp = localdb.component.find_one( { "name":self.mname } )
        doc = {
                 "component"   : str(this_cp["_id"]),
                 "user"        : session.get("fullname","No user"),
                 "address"     : session.get("institution", "Not registered"),
                 "currentStage": self.sname,
                 "sys"         : { "mts":datetime.now(),"cts":datetime.now(),"rev":0 },
                 "dbVersion"   : dbv,
                 "testType"    : scan_doc["test"],
                 "startTime"   : datetime.now(),
                 "results"     : { "setting_temp": self.getModuleNtcTemp(run_ids[0]) , "scans":[], "analysis":[] }
              }
        for i, run_id in enumerate(run_ids):
            doc["results"]["scans"].append({"name":scan_doc["scantypes"][i], "runId":run_id})
        if scan_doc["analysis"]["run"]:
            doc["results"]["analysis"].append( {"name": scan_doc["analysis"]["name"], "result": self.getBadPixelNum( doc )} )
        return doc

    def createSourceTestDocs( self, run_ids, scan_doc ):
        this_cp = localdb.component.find_one( { "name":self.mname } )
        doc = {
                 "component"   : str(this_cp["_id"]),
                 "user"        : session.get("fullname","No user"),
                 "address"     : session.get("institution", "Not registered"),
                 "currentStage": self.sname,
                 "sys"         : { "mts":datetime.now(),"cts":datetime.now(),"rev":0 },
                 "dbVersion"   : dbv,
                 "testType"    : scan_doc["test"],
                 "startTime"   : datetime.now(),
                 "results"     : { "setting_temp": self.getModuleNtcTemp(run_ids[0]) , "scans":[], "analysis":[] }
              }
        for i, run_id in enumerate(run_ids):
            doc["results"]["scans"].append({"name":"chip" + str(i), "runId":run_id, "testType": scan_doc["test"], "results": { "setting_temp": self.getModuleNtcTemp(run_ids[0]) , "scans":[], "analysis":[] }})
#        if scan_doc["analysis"]["run"]:
#            doc["results"]["analysis"].append( {"name": scan_doc["analysis"]["name"], "result": self.getBadPixelNum( doc )} )
        return doc

    def getModuleNtcTemp( self, tr_oid ):
        query={"testRun":tr_oid}
        this_env={}
        this_ctrs = localdb.componentTestRun.find( query )
        for this_ctr in this_ctrs:
            if this_ctr.get("environment","...") != "...":
                this_env = localdb.environment.find_one( {"_id":ObjectId(this_ctr["environment"]) })
                break
        this_temp = this_env.get("temperature_modulentc","None")
        if this_temp != "None":
            this_temp = this_temp[0].get("setting","None")

        return str(this_temp)

    def getScanList(self, test):
        doc = {}
        if test in self.test_scan_map:
            doc["test"]      = test
            doc["setting_temp"] = self.test_scan_map[test]["setting_temp"]
            doc["scantypes"] = self.test_scan_map[test]["scantypes"]
            doc["analysis"]  = self.test_scan_map[test]["analysis"]

        if doc == {}:
            doc["test"]      = "ELECTRICAL_TEST"
            doc["setting_temp"] = None
            doc["scantypes"] = self.test_scan_map["ELECTRICAL_TEST"]["scantypes"]
            doc["analysis"]  = self.test_scan_map["ELECTRICAL_TEST"]["analysis"]

        return doc

    def getAnalysisResult( self, doc ):
        run_dir = "{0}/{1}".format(AT_DIR, self.mname + "_" + self.sname)
        this_cp = localdb.component.find_one({"name":self.mname})
        this_cprs = localdb.childParentRelation.find({"parent":str(this_cp["_id"])})
        chips = []
        for this_cpr in this_cprs:
            this_chip = localdb.component.find_one({"_id":ObjectId(this_cpr["child"])})
            chips.append(this_chip["name"])

        self.getDataFile( run_dir, doc )
        i = self.doAnalysis( chips, run_dir )

        infos = []

        try:
            if i == 1:
                infos = [{ "Tree_path":"", "result_path":"", "plot_info":"", "name":"", "tool":False }]

            else:
                for chip in chips:
                    this_dir = run_dir + "/chip_" + chip
                    with open(this_dir + "/plotLog.json") as f:
                        plot_infos = json.load(f)

                    info = { "chipname":chip, "Tree_path":run_dir + "/Tree.root", "result_path":this_dir + "/after_analysis.root", "plot_info":{}, "name":"Bad Pixel Analysis", "tool":True }
                    for i, plot_info in enumerate(plot_infos["criterias"]):
                        binary_image = open( plot_info[1][0] , "rb" )
                        code_base64 = base64.b64encode(binary_image.read()).decode()
                        binary_image.close()
                        info["plot_info"][plot_info[0]] = {}
                        info["plot_info"][plot_info[0]]["png_path"] = bin2image("png", code_base64)
                        info["plot_info"][plot_info[0]]["cut"] = plot_info[1][1]

                        bad_pixel_total = plot_infos["criterias"][i][1][2]
                        info["plot_info"][plot_info[0]]["total"] = bad_pixel_total

                        bad_pixel_num = 0
                        try:
                            bad_pixel_num = int(plot_infos["criterias"][i][1][2])-int(plot_infos["criterias"][i-1][1][2])
                        except:
                            bad_pixel_num = plot_infos["criterias"][i][1][2]
                        info["plot_info"][plot_info[0]]["num"] = bad_pixel_num

                    ### for JSROOT
                    root_path = "{0}/after_analysis.root".format( this_dir )
                    jsroot_path = "{0}/{1}".format( JSROOT_DIR, chip + "_" + self.sname )
                    if os.path.isfile(root_path) and not os.path.isdir(jsroot_path):
                        os.mkdir(jsroot_path)
                        shutil.copy(root_path, "{}/after_analysis.root".format(jsroot_path))
                    if os.path.isfile("{}/after_analysis.root".format(jsroot_path)):
                        info.update({ "jsroot": { "path": "./jsroot/{}/".format(chip + "_" + self.sname), "files": "after_analysis.root" }})

                    infos.append(info)
        except: pass

        return infos

    def getBadPixelNum( self, doc ):
        docs = self.getAnalysisResult( doc )
        list_ = []
        for chip_info in docs:
            dict_ = {}
            dict_["chip"]=chip_info["chipname"]
            dict_["result"]=[]
            for criteria,v in chip_info["plot_info"].items():
                if not v["num"] == '':
                    this_analysis = { "criteria": criteria, "num":int(v["num"]) }
                    dict_["result"].append(this_analysis)
            list_.append(dict_)

        return list_

    def getDataFile( self, path, commit_doc):
        config_file = {
                        "datadir": path,
                        "module":{
                          "serialNumber":self.mname,
                        },
                        "chip":[]
                      }

        if os.path.exists(path):
            with open( path + "/scans.json") as f:
                 scans = json.load(f)
            if scans != commit_doc["results"]["scans"]:
                shutil.rmtree(path)
            else:
                return False

        tmp_dir = "{0}/{1}".format( path, "module_" + self.mname)

        for i,d in enumerate(commit_doc["results"]["scans"]):
            query = {"_id": ObjectId(d["runId"])}
            tr_doc = localdb.testRun.find_one( query )
            if i == 0:
                config_file["module"]["chipType"] = tr_doc["chipType"]
            this_dir = "{0}/{1}_1".format( self.sname, tr_doc["testType"] )
            os.makedirs( tmp_dir + "/" + this_dir )
            query = {"testRun": str(tr_doc["_id"])}
            this_ctrs = localdb.componentTestRun.find( query )

            j = 0
            for this_ctr in this_ctrs:
                query = {"name": this_ctr["name"]}
                this_cp = localdb.component.find_one(query)
                if this_cp["componentType"] == "front-end_chip":
                    if i == 0:
                        config_file["chip"].append({ "serialNumber":this_cp["name"], "filepath":[] })
                    for attachment in this_ctr["attachments"]:
                        if attachment["filename"].split(".")[0].split("-")[0] in DATA_SELECTION_LIST[tr_doc["testType"]]:
                            data = fs.get(ObjectId(attachment["code"])).read()
                            filename = "{0}_{1}".format( this_cp["name"], attachment["filename"] )
                            filepath = "{0}/{1}/{2}".format( tmp_dir, this_dir, filename )
                            config_file["chip"][j]["filepath"].append(this_dir + "/" + filename)
                            with open(filepath, "wb") as f:
                                f.write(data)
                    j += 1

        with open( path + "/scans.json", "w") as f:
            json.dump( commit_doc["results"]["scans"], f, ensure_ascii=False, indent=4, sort_keys=False, separators=(',', ': ') )
        with open( path + "/info.json", "w") as f:
            json.dump( config_file, f, ensure_ascii=False, indent=4, sort_keys=False, separators=(',', ': ') )

        return True

    def doAnalysis( self, chips, run_dir ):

        if not os.path.isdir(run_dir):
            return

        tree_config = run_dir+"/info.json"
        tree_file = run_dir+"/Tree.root"
        analysis_config = "analysis-tool/ana.json"
        o_file = run_dir

        if os.path.isfile("{}/Tree.root".format(run_dir)):
            return 0
        else:
            try:
                command = ["{}/analysis-tool/bin/makeTree".format(VIEWER_DIR), "-c", tree_config, "-o", tree_file]
                subprocess.call(command)
                for this_chip in chips:
                    os.makedirs( o_file + "/" + "chip_" + this_chip )
                    command = ["{}/analysis-tool/bin/analysis".format(VIEWER_DIR), "-i", tree_file, "-o", o_file + "/" + "chip_" + this_chip, "-c", analysis_config, "-f", this_chip, "-s", self.sname]
                    subprocess.call(command)
                return 0
            except:
                return 1
