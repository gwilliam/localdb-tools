from functions.imports import *

static_api = Blueprint(
    "upload",
    __name__,
    static_url_path="/static",
    static_folder="{}/static".format(TMP_DIR),
)

static_api = Blueprint(
    "result",
    __name__,
    static_url_path="/result",
    static_folder="{}/result".format(TMP_DIR),
)

static_api = Blueprint(
    "thumbnail",
    __name__,
    static_url_path="/thumbnail",
    static_folder="{}/thumbnail".format(TMP_DIR),
)

static_api = Blueprint(
    "jsroot",
    __name__,
    static_url_path="/jsroot",
    static_folder="{}/jsroot".format(TMP_DIR)
)

static_api = Blueprint(
    "cache",
    __name__,
    static_url_path="/cache",
    static_folder="{}/cache".format(TMP_DIR)
)
