# Viewer Application

The Viewer Application displays the contents in Local DB on web browser. 

## Usage

### 0. Pre Requirement

- MongoDB is running
- python3
- ROOT CXX Library

### 1. Start up

```bash
$ cd viewer
$ ./setup_viewer.sh
[LDB] Set editor command ... > vim
<some texts>

[LDB] Set ROOT installed directroy.
> /opt/root/6.16.00-build/

<some texts>
```

It will initialize ./conf.yml and install plotting tool from git repository (https://gitlab.cern.ch/YARR/utilities/plotting-tools).

### 2. Run Application

```bash
$ python3 app.py --config conf.yml
# Start Viewer Application and access http://127.0.0.1:5000/localdb/ in the local browser to check the Viewer Application
```

## Config file

```yaml
mongoDB:
    host: 127.0.0.1 # IP address running mongoDB
    port: 27017     # port number running mongoDB
    db: localdb     # local database name
    username: # username of user account in MongoDB
    password: # password of user account in MongoDB
    KeyFile: #localdbkeypass # path/to/user/key/file
    ssl:
        enabled: False
        CAFile: # path/to/CA/file
        PEMKeyFile: # path/to/certificate/file 
    tls:
        enabled: False
        CAFile: # path/to/CA/file 
        CertificateFile: # path/to/certificate/file

userDB:
    db: localdbtools

flask:
    host: 127.0.0.1 # IP address running app.py
    port: 5000      # port number running app.py
```

# Advance

----------------------------------

# Run flask on debug mode
```
export FLASK_DEBUG=1
nohup python3 app.py &
```
