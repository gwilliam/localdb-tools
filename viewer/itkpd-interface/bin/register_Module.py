#!/usr/bin/env python3
# -*- coding: utf-8 -*
##################################################################
## Author1: Satoshi Kinoshita (kinoshita at hep.phys.titech.ac.jp)
## Copyright: Copyright 2019, ldbtools
## Date: Nov. 2019
## Project: Local Database Tools
## Description: ITkPD Interface
##################################################################

import os, sys

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))
from functions.imports import *

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from register_Module import *

#####################################################
## register modules

def register_module():

    code1 = os.environ.get("ITKDB_ACCESS_CODE1", " ")
    code2 = os.environ.get("ITKDB_ACCESS_CODE2", " ")
    print("Connecting to Production DB...")
    token = process_request(code1, code2)
    if token == 0:
        sys.exit(1)
    else:
        with open("module_registerCfg.json") as f:
            config = json.load(f)
        module_config = localdb.QC.module.types.find_one()
        look_up_table = make_table(module_config)
        register_Module(code1, code2, config, look_up_table)

if __name__ == "__main__":
    register_module()

