import os, sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

class PDInterface:
    # constructor
    def __init__(self, code1, code2):
        self.u = itkdb.core.User(accessCode1=code1, accessCode2=code2)
        self.pd_client = itkdb.Client(user=self.u)
        with open("{}/itkpd-interface/config/stage_test.json".format(VIEWER_DIR)) as f:
            self.stage_test_map = json.load(f)

    def getCompFromLocalDB(self, sn):
        query = {"serialNumber": sn,"proDB": True}
        return localdb.component.find_one(query)

    def getFEchipsDocs(self, msn):
        m_doc = self.getCompFromLocalDB(msn)
        cprs = localdb.childParentRelation.find({"parent":str(m_doc["_id"])})
        chip_ids = [ chip["child"] for chip in cprs ]
        return [localdb.component.find_one({"_id":ObjectId(chip_id)}) for chip_id in chip_ids]

    def getQmsFromLocalDB(self, oid):
        query = {"component": oid}
        return localdb.QC.module.status.find_one(query)

    def getPmsFromLocalDB(self, oid):
        query = {"component": oid}
        return localdb.QC.prop.status.find_one(query)

    def getQrFromLocalDB(self, oid):
        query = {"_id": ObjectId(str(oid))}
        return localdb.QC.result.find_one(query)

    def getPrFromLocalDB(self, oid):
        query = {"_id": ObjectId(str(oid))}
        return localdb.QC.module.prop.find_one(query)

    def getTrFromLocalDB(self, oid):
        query = {"_id": ObjectId(str(oid))}
        return localdb.testRun.find_one(query)

    def getCtrFromLocalDB(self, tr_id):
        query = {"testRun": str(tr_id)}
        return localdb.componentTestRun.find(query)

    def getCprFromLocalDB(self, poid, coid):
        query = { "parent": poid, "child": coid }
        return localdb.childParentRelation.find_one(query)

    def getCompFromProdDB(self, sn):
        return self.pd_client.get( "getComponent", json={"component": sn} )

    def insertDocToLdb(self, col, doc):
        localdb[col].insert(doc)
        return

    def getBinaryDataFromProdDB(self, code):
        return self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code":code}).content

    def writeBinaryData(self, path, data):
        with open(path,"wb") as f:
            f.write(data)
        return
