import os, sys
import PDInterface

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

class ModuleStageDownloader(PDInterface.PDInterface):
    # private

    def __create_QC_status_doc(self, cpt_doc):
        stage_flow = []
        stage_vs_test = {}
        stage_map = {}
        test_item_map = {}

        for stage in cpt_doc["stages"]:
            stage_map[stage["code"]] = stage["name"]
            test_items = []
            if "testTypes" in stage and not stage["testTypes"] is None:
                for testType in stage["testTypes"]:
                    test_item_map[testType["testType"]["code"]] = testType["testType"]["name"]
                    test_items.append(testType["testType"]["code"])
            stage_flow.append(stage["code"])
            stage_vs_test[stage["code"]] = test_items

        cpt_doc = {
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "proddbVersion": proddbv,
            "stage_flow":stage_flow,
            "stage_test":stage_vs_test,
            "stages":stage_map,
            "test_items":test_item_map
        }

        return cpt_doc

    def __create_module_status_doc_from_itkpd(self, module_doc, module_cpt_doc):
        itk_stage = self.__create_QC_status_doc(module_cpt_doc)
        doc = {
           # "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion"          : dbv,
            "proddbVersion"      : proddbv,
            "component"          : str(module_doc["_id"]),
            "currentStage"       : itk_stage["stage_flow"][0],
            "latestSyncedStage"  : itk_stage["stage_flow"][0],
            "status"             : "created",
            "rework_stage"       : [],
            "QC_results"         : {},
            "upload_status"      : {}
        }
        # TO DO:
        # 1. Create a case for QUAD and TRIPLET for Metrology
        # 2. Templeature setting
        for stage, tests in itk_stage["stage_test"].items():
            doc["QC_results"][stage] = {}
            for test in tests:
                if test == "SENSOR_IV" or test == "PIXEL_FAILURE_TEST":
                    doc["QC_results"][stage][test + "_30_DEGREE"] = "-1"
                    doc["QC_results"][stage][test + "_20_DEGREE"] = "-1"
                    doc["QC_results"][stage][test + "_min15_DEGREE"] = "-1"
                elif test == "CHIP_CONFIGURATION":
                    continue
                else:
                    doc["QC_results"][stage][test] = "-1"
        return doc


    def __downloadStagesAndTests(self, module_cpt_doc):

        # create an example of a document of QC status
        logger.info("Start downloading info of stages and QC tests")
        if userdb.QC.status.find_one({"proddbVersion":proddbv}) == None:
            userdb.QC.status.insert_one( self.__create_QC_status_doc(module_cpt_doc) )
        logger.info("Finished!!\n")

        return 0


    # public
    def download_stage_info(self, component_id):

        if not os.path.exists(IF_DIR):
            os.makedirs(IF_DIR)
        do_filepath = IF_DIR + '/doing_download.txt'
        with open(do_filepath, 'w') as f:
            f.write('doing now')

        # create stage information from ITkPD
        module_cpt_doc = self.pd_client.get("getComponentType",json={"id":"5bb1e1bef981520009c54bc5"})
        self.__downloadStagesAndTests(module_cpt_doc)

        # create list of module to fix stage flow
        d_module = []
        if component_id != None:
            d = super().getCompFromLocalDB(component_id)
            if d["componentType"] == "module":
                d_module.append(d["serialNumber"])
        else:
            doc = localdb.component.find({"componentType" : "module"})
            for d in doc:
                d_module.append(d["serialNumber"])

        for m_serial in d_module:
            logger.info(m_serial)
            # try to fix component stage
            try:
                module_doc = super().getCompFromLocalDB(m_serial)
                QC_docs = self.__create_module_status_doc_from_itkpd(module_doc, module_cpt_doc)
                localdb.QC.module.status.remove({"component": QC_docs["component"]})
                localdb.QC.module.status.insert(QC_docs)
            except:
                logger.info("")


        logger.info("Finished!!\n")

        if os.path.exists(IF_DIR + '/doing_download.txt'):
             os.remove(IF_DIR + '/doing_download.txt')
