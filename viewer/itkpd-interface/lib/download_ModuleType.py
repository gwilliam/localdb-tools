import os, sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

##############
## functions


def query_moduletype(doc):

    localdbtools_db = client.localdbtools
    query = {"code": doc["code"]}
    if localdbtools_db["QC.module.types"].find_one(query) == None:
        moduletype_info = {
            "id": doc["id"],
            "code": doc["code"],
            "name": doc["name"],
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
        }
        localdbtools_db["QC.module.types"].insert_one(moduletype_info)
    address = localdbtools_db["QC.module.types"].find_one(query)

    return address

def create_moduletype(moduletype_doc, baretype_doc, pcbtype_doc):

    baretypes = [ baretype_doc["types"][i]["code"] for i in range(len(baretype_doc["types"])) ]
    pcbtypes = [ pcbtype_doc["types"][i]["code"] for i in range(len(pcbtype_doc["types"])) ]

    doc = {
        "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
        "dbVersion": dbv,
        "_id": ObjectId(moduletype_doc["id"]),
        "code": moduletype_doc["code"],
        "name": moduletype_doc["name"],
        "project": moduletype_doc["project"],
        "subprojects": moduletype_doc["subprojects"],
        "FEchip": moduletype_doc["properties"][9]["codeTable"]
    }
    doc["types"] = [ {
                    "code": moduletype["code"],
                    "name": moduletype["name"],
                    "subprojects": moduletype["subprojects"],
                    "snComponentIdentifier": moduletype["snComponentIdentifier"]
                     }
                     for moduletype in moduletype_doc["types"] if moduletype["code"] != "SINGLE_CHIP_MODULE" ]

    doc["children"] = {}
    for childtype in moduletype_doc["children"].keys():
        doc["children"][childtype.replace('.', '')] = {}
        for i in range(len(moduletype_doc["children"][childtype])):
            if moduletype_doc["children"][childtype][i]["code"] == "BARE_MODULE":
                bare_type = baretypes.index(moduletype_doc["children"][childtype][i]["type"]["code"])
                doc["children"][childtype.replace('.', '')]["BARE_MODULE"] = baretype_doc["types"][bare_type]["subprojects"][0]["code"] + baretype_doc["types"][bare_type]["snComponentIdentifier"]
            elif moduletype_doc["children"][childtype][i]["code"] == "PCB":
                pcb_type = pcbtypes.index(moduletype_doc["children"][childtype][i]["type"]["code"])
                doc["children"][childtype.replace('.', '')]["PCB"] = pcbtype_doc["types"][pcb_type]["subprojects"][0]["code"] + pcbtype_doc["types"][pcb_type]["snComponentIdentifier"]

    return doc

def download_ModuleType(code1, code2):

    u = itkdb.core.User(accessCode1=code1, accessCode2=code2)
    pd_client = itkdb.Client(user=u)

    localdbtools_db = client.localdbtools

    # download module type infomation from ITkPD
    logger.info("Start downloading module type info...")
    moduletype_doc = pd_client.get("getComponentTypeByCode", json={"project":"P","code":"MODULE"})
    baretype_doc = pd_client.get("getComponentTypeByCode", json={"project":"P","code":"BARE_MODULE"})
    pcbtype_doc = pd_client.get("getComponentTypeByCode", json={"project":"P","code":"PCB"})
    doc = create_moduletype(moduletype_doc, baretype_doc, pcbtype_doc)
    if localdbtools_db["QC.module.types"].find_one({"_id":ObjectId(moduletype_doc["id"])}) == None:
        localdbtools_db["QC.module.types"].insert_one(doc)
    else:
        localdbtools_db.drop_collection("QC.module.types")
        localdbtools_db["QC.module.types"].insert_one(doc)
    address = query_moduletype(doc)
    logger.info("Finished!!\n")
