import os, sys
import PDInterface

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

class QCResultUploader(PDInterface.PDInterface):
    def __upload_functions(self, module_doc, result_doc, test_item):

        if len(result_doc) == 1:
            logger.info("This result format is not proper.")
            return

        for test in self.stage_test_map[result_doc["currentStage"]]:
            if test["ldb_testType"] == result_doc["testType"]:
                result_doc["testType"] = test["pdb_testType"]
                break

        functions = {
            "OPTICAL"                         :  self.__upload_VI,
            "PIXEL_FAILURE_TEST"              :  self.__upload_electrical,
            "READOUT_IN_BASIC_ELECTRICAL"     :  self.__upload_electrical,
            "METROLOGY"                       :  self.__upload_metrology,
            "SENSOR_IV"                       :  self.__upload_SensorIV,
            "SLDO_VI"                         :  self.__upload_SLDOVI,
            "MASS"                            :  self.__upload_mass,
            "GLUE_MODULE_FLEX_ATTACH"         :  self.__upload_glueInformation,
            "WIREBONDING"                     :  self.__upload_wirebonding,
            "WIREBOND"                        :  self.__upload_wirebond
        }

        if result_doc != {} and result_doc["testType"] in functions: functions[result_doc["testType"]](module_doc, result_doc, test_item)
        else: logger.info("There is no uploading function.")
        return

    def __upload_prop_functions(self, module_doc, prop_doc):
        if prop_doc == {}:
            logger.info("This result format is not proper.")
            return

        prop_functions = {
            "RD53A_PULL-UP_RESISTOR"    : self.__upload_pullupregistor,
            "IREFTRIM_FE"               : self.__upload_ireftrim,
            "ORIENTATION"               : self.__upload_orientation
        }

        if prop_doc != {} and prop_doc["testType"] in prop_functions: prop_functions[prop_doc["testType"]](module_doc, prop_doc)
        else: logger.info("There is no uploading function.")
        return


########################
## for non-electrical ##
    def __upload_VI(self, module_doc, result_doc, test_item):
        qr_oid = result_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        str_date = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        old_result_docs = []
        for tr in result_doc["old_tests"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == result_doc["testType"] and tr["stage"]["code"] == result_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    old_result_doc = self.__getOldResult(tr_doc)
                    old_result_docs.append(old_result_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createOpticalTestRun(result_doc, module_doc, str_date)
            result_doc.pop("old_tests")
            new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
            attach_golden_imgs = self.__AttachGoldens(result_doc, new_test_result, test_item)
        else:
            if str(result_doc["_id"]) in old_result_docs:
#                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createOpticalTestRun(result_doc, module_doc, str_date)
                result_doc.pop("old_tests")
                new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
                attach_golden_imgs = self.__AttachGoldens(result_doc, new_test_result, test_item)
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], old_result_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_metrology(self, module_doc, result_doc, test_item):
        qr_oid = result_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        str_date = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        old_result_docs = []
        for tr in result_doc["old_tests"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == result_doc["testType"] and tr["stage"]["code"] == result_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    old_result_doc = self.__getOldResult(tr_doc)
                    old_result_docs.append(old_result_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createMetrologyTestRun(result_doc, module_doc, str_date)
            result_doc.pop("old_tests")
            self.__createRawResultAttachment(result_doc, new_test_result, test_item)
        else:
            if str(result_doc["_id"]) in old_result_docs:
#                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createMetrologyTestRun(result_doc, module_doc, str_date)
                result_doc.pop("old_tests")
                self.__createRawResultAttachment(result_doc, new_test_result, test_item)
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], old_result_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_SensorIV(self, module_doc, result_doc, test_item):
        qr_oid = result_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        str_date = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        old_result_docs = []
        for tr in result_doc["old_tests"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == result_doc["testType"] and tr["stage"]["code"] == result_doc["currentStage"]:
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                tr_doc["testType"]["code"] = test_item
                try:
                    old_result_doc = self.__getOldResult(tr_doc)
                    if old_result_doc["0"]["testType"] == test_item:
                        result_counter = result_counter + 1
                        old_result_docs.append(old_result_doc["0"]["_id"])
                except:
                    logger.warning("this is not localdb results")

        if result_counter == 0:
            new_test_result = self.__createSensorIVTestRun(result_doc, module_doc, str_date, test_item)
            result_doc.pop("old_tests")
            self.__createRawResultAttachment(result_doc, new_test_result, test_item)
        else:
            if str(result_doc["_id"]) in old_result_docs:
                try:
                    shutil.rmtree(cache_dir)
                except:
                    pass
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createSensorIVTestRun(result_doc, module_doc, str_date, test_item)
                result_doc.pop("old_tests")
                self.__createRawResultAttachment(result_doc, new_test_result, test_item)
                #self.__deleteResult(tr_docs[i], new_test_result["testRun"]["id"], old_result_docs[i])

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_SLDOVI(self, module_doc, result_doc, test_item):
        qr_oid = result_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        str_date = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        old_result_docs = []
        for tr in result_doc["old_tests"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == result_doc["testType"] and tr["stage"]["code"] == result_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    old_result_doc = self.__getOldResult(tr_doc)
                    old_result_docs.append(old_result_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createSLDOVITestRun(result_doc, module_doc, str_date)
            result_doc.pop("old_tests")
            new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
        else:
            if str(result_doc["_id"]) in old_result_docs:
#                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createSLDOVITestRun(result_doc, module_doc, str_date)
                result_doc.pop("old_tests")
                new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], old_result_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_mass(self, module_doc, result_doc, test_item):
        qr_oid = result_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        str_date = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        old_result_docs = []
        for tr in result_doc["old_tests"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == result_doc["testType"] and tr["stage"]["code"] == result_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    old_result_doc = self.__getOldResult(tr_doc)
                    old_result_docs.append(old_result_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createMassTestRun(result_doc, module_doc, str_date)
            result_doc.pop("old_tests")
            new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
        else:
            if str(result_doc["_id"]) in old_result_docs:
#                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createMassTestRun(result_doc, module_doc, str_date)
                result_doc.pop("old_tests")
                new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], old_result_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_glueInformation(self, module_doc, result_doc, test_item):
        qr_oid = result_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        str_date = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        old_result_docs = []
        for tr in result_doc["old_tests"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == result_doc["testType"] and tr["stage"]["code"] == result_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    old_result_doc = self.__getOldResult(tr_doc)
                    old_result_docs.append(old_result_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createGlueTestRun(result_doc, module_doc, str_date)
            result_doc.pop("old_tests")
            new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
        else:
            if str(result_doc["_id"]) in old_result_docs:
#                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createGlueTestRun(result_doc, module_doc, str_date)
                result_doc.pop("old_tests")
                new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], old_result_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_wirebonding(self, module_doc, result_doc, test_item):
        qr_oid = result_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        str_date = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        old_result_docs = []
        for tr in result_doc["old_tests"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == result_doc["testType"] and tr["stage"]["code"] == result_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    old_result_doc = self.__getOldResult(tr_doc)
                    old_result_docs.append(old_result_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createWirebondingTestRun(result_doc, module_doc, str_date)
            result_doc.pop("old_tests")
            attach_bonding_program = self.__attachBondingProgram(result_doc, new_test_result)
            new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
        else:
            if str(result_doc["_id"]) in old_result_docs:
#                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createWirebondingTestRun(result_doc, module_doc, str_date)
                result_doc.pop("old_tests")
                attach_bonding_program = self.__attachBondingProgram(result_doc, new_test_result)
                new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], old_result_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __attachBondingProgram(self, result_doc, new_test_result):
        try:
            fsb = gridfs.GridFSBucket(localdb)
            bond_file = open('static/Bonding_Program.dat','wb+')
            fsb.download_to_stream(ObjectId(result_doc["results"]["property"]["Bond_program"]), bond_file)
            bond_file.seek(0)
            contents = bond_file.read()
        except:
            return

        attachment_file = open('static/Bonding_Program.dat', 'rb')
        page_attachment = {"data": ('Bonding_Program.dat', attachment_file, "dat")}

        self.pd_client.post( "createTestRunAttachment", data=dict( testRun=new_test_result["testRun"]["id"], type="file" ), files=page_attachment )
        return  os.remove('static/Bonding_Program.dat')

    def __upload_wirebond(self, module_doc, result_doc, test_item):
        qr_oid = result_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        str_date = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        old_result_docs = []
        for tr in result_doc["old_tests"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == result_doc["testType"] and tr["stage"]["code"] == result_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    old_result_doc = self.__getOldResult(tr_doc)
                    old_result_docs.append(old_result_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createWirebondTestRun(result_doc, module_doc, str_date)
            result_doc.pop("old_tests")
            new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
        else:
            if str(result_doc["_id"]) in old_result_docs:
#                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createWirebondTestRun(result_doc, module_doc, str_date)
                result_doc.pop("old_tests")
                new_test_attachment = self.__createRawResultAttachment(result_doc, new_test_result, test_item)
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], old_result_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

####################
## for electrical ##
    ## main function
    def __upload_electrical(self, module_doc, result_doc, test_item):
        #check if a result is already uploaded or not
        qr_oid = result_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        str_date = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        old_result_docs = []
        tr_docs = []
        for tr in result_doc["old_tests"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == result_doc["testType"] and tr["stage"]["code"] == result_doc["currentStage"]:
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                tr_doc["testType"]["code"] = test_item
                try:
                    old_result_doc = self.__getOldResult(tr_doc)
                    if old_result_doc["0"]["testType"] == test_item:
                        result_counter = result_counter + 1
                        old_result_docs.append(old_result_doc['0']['_id'])
                        tr_docs.append(tr_doc)
                except:
                    logger.warning("Results not uploaded from LocalDB exist")
        if result_counter == 0:
            self.__create_electrial_pdb(module_doc, result_doc, test_item)
        else:
            if str(result_doc["_id"]) in old_result_docs:
                try:
                    shutil.rmtree(cache_dir)
                except:
                    pass
                logger.info("This result is already uploaded.")
                return
            else:
                self.__create_electrial_pdb(module_doc, result_doc, test_item)
                #for result in tr_docs[i]['results']:
                #    if result['code'] == "RESULT_IDS":
                #        for scan_id in result['value']:
                #            self.pd_client.post("deleteTestRun", json={"testRun": scan_id})
                #self.__deleteResult(tr_docs[i], new_test_result["testRun"]["id"], old_result_docs[i])

        logger.info("Finished!!\n")
        return
        #time.sleep(1)


    def __create_electrial_pdb(self, module_doc, result_doc, test_item):
        cp_test_map = {}
        str_date = None
        scans = []

        #create cache files
        for i, d_ in enumerate(result_doc["results"]["scans"]):
            scanType, tr_oid = d_["name"], d_["runId"]
            scans.append(d_["name"])
            path = "{}/{}".format(IF_DIR,d_["name"])
            if os.path.exists(path): shutil.rmtree(path)
            command = ["{}/itkpd-interface/localdb-interface/bin/localdbtool-retrieve".format(VIEWER_DIR), "pull", "--test", d_["runId"], "--directory", path]
            subprocess.call(command)
            if i == 0:
                qr_doc = super().getQrFromLocalDB(result_doc["_id"])
                str_date = self.__getQCStrTime(qr_doc["sys"]["cts"])
#                tr_doc = super().getTrFromLocalDB(tr_oid)
#                str_date = self.__getStrTime(tr_doc["timestamp"])

            # fix scan result's files if component name is changed
            scan_files = os.listdir(path)

            # create relationship between past & new components
            old_chips = []
            new_chips = []
            old_module_name = ""
            new_module_name = ""
            compTestRun = localdb.componentTestRun.find({"testRun" : d_["runId"]})
            for item in compTestRun:
                if "scanSN" in item:
                    if item["chip"] == "module":
                        old_module_name = item["scanSN"]
                        new_module_name = item["name"]
                    else:
                        old_chips.append(item["scanSN"])
                        new_chips.append(item["name"])

            if old_module_name:
                # fix contents of scan files
                for file in scan_files:
                    f1 = open(path + "/" + file, 'r', encoding='utf-8').read()
                    f2 = open(path + "/" + file, 'w', encoding='utf-8')
                    for i in range(len(old_chips)):
                        f1 = f1.replace(old_chips[i], new_chips[i])
                    if file == "connectivity.json" or file == "scanLog.json":
                        f1 = f1.replace(old_module_name, new_module_name)
                        f1 = f1.replace(path + "//", "/")
                    f2.write(f1)

                # fix file name
                for file in scan_files:
                    for i in range(len(old_chips)):
                        if old_chips[i] in file:
                            new_file = file.replace(old_chips[i], new_chips[i])
                            os.rename(path + "/" + file, path + "/" + new_file)

        #for FE chips
        fe_docs = super().getFEchipsDocs(module_doc["serialNumber"])
        for fe_doc in fe_docs:
            new_test_result = self.__createElecTestRunForFEChip(result_doc, fe_doc["serialNumber"], str_date)
            cp_test_map[fe_doc["serialNumber"]] = new_test_result["testRun"]["id"]
            for scan in scans:
                zip_name = "{}_{}".format(fe_doc["serialNumber"],scan)
                zip_path = "{0}/{1}.zip".format(IF_DIR, zip_name)
                if os.path.exists(zip_path): os.remove(zip_path)
                zip_file = zipfile.ZipFile(zip_path, "a", zipfile.ZIP_DEFLATED)
                files = glob.glob("{}/{}/*".format(IF_DIR,scan))
                for filepath in files:
                    filename = filepath.split("/")[-1]
                    if fe_doc["serialNumber"] in filename: zip_file.write(filepath, filename)
                zip_file.close()
                self.__attachFileToTestRun(zip_path, scan + ".zip", cp_test_map[fe_doc["serialNumber"]])

        #for module
        new_module_result = self.__createElecTestRunForModule(result_doc, module_doc, str_date, cp_test_map, test_item)
        result_doc.pop("old_tests")
        new_test_attachment = self.__createRawResultAttachment(result_doc, new_module_result, test_item)
        for i, scan in enumerate(scans):
            zip_name = "{}_{}".format(module_doc["serialNumber"],scan)
            zip_path = "{0}/{1}.zip".format(IF_DIR, zip_name)
            if os.path.exists(zip_path): os.remove(zip_path)
            zip_file = zipfile.ZipFile(zip_path, "a", zipfile.ZIP_DEFLATED)
            files = glob.glob("{}/{}/*".format(IF_DIR,scan))
            for filepath in files:
                filename = filepath.split("/")[-1]
                if i == 0 and (filename.split(".")[0] in [ "dbCfg", "userCfg", "siteCfg" ]): self.__attachFileToTestRun(filepath, filename, new_module_result["testRun"]["id"])
                if filename.split(".")[0] in [ scan, "scanLog"]:
                    if filename.split(".")[0] == "scanLog": self.__modifyScanLog(filepath, result_doc["currentStage"])
                    zip_file.write(filepath, filename)

            zip_file.close()
            self.__attachFileToTestRun(zip_path, scan + ".zip" , new_module_result["testRun"]["id"])

        # attach the best configs to the module's top page.
        for scan in scans:
            if scan == "std_thresholdscan":
                zip_name = "{}_{}_degree_{}".format("BestCfg",str(self.__getElecTestSettingTemp(result_doc, test_item)).replace("-","minus"), result_doc["currentStage"])
                zip_path = "{0}/{1}.zip".format(IF_DIR, zip_name)
                if os.path.exists(zip_path): os.remove(zip_path)
                zip_file = zipfile.ZipFile(zip_path, "a", zipfile.ZIP_DEFLATED)
                for fe_doc in fe_docs:
                    filepath = "{}/{}/{}".format(IF_DIR,scan,fe_doc["serialNumber"] + ".json.after")
                    if os.path.exists(filepath): zip_file.write(filepath, filepath.split("/")[-1])
                zip_file.close()
                self.__attachFileToComp(zip_path, zip_name + ".zip", module_doc)

        return new_module_result



#########################
## for module property ##
    ## main function
    def __upload_pullupregistor(self, module_doc, prop_doc):
        pr_oid = prop_doc["_id"]
        pr_doc = super().getQrFromLocalDB(pr_oid)
        new_prop_result = self.__createPullupTestRun(prop_doc, module_doc)
        new_prop_attachment = self.__createPropertyAttachment(prop_doc, module_doc)

        logger.info("Finished!!\n")

    def __upload_ireftrim(self, module_doc, prop_doc):
        pr_oid = prop_doc["_id"]
        pr_doc = super().getQrFromLocalDB(pr_oid)
        new_prop_result = self.__createIreftrimTestRun(prop_doc, module_doc)
        new_prop_attachment = self.__createPropertyAttachment(prop_doc, module_doc)

        logger.info("Finished!!\n")

    def __upload_orientation(self, module_doc, prop_doc):
        pr_oid = prop_doc["_id"]
        pr_doc = super().getQrFromLocalDB(pr_oid)
        new_prop_result = self.__createOrientationTestRun(prop_doc, module_doc)
        new_prop_attachment = self.__createPropertyAttachment(prop_doc, module_doc)

        logger.info("Finished!!\n")


    ## sub functions
    def __getStrTime(self, time):
        this_time = time.split("_")
        scan_date = this_time[0].split("-")
        scan_time = this_time[1].split(":")
        return  str(scan_date[2] + "." + scan_date[1] + "." + scan_date[0] + " " + scan_time[0] + ":" + scan_time[1])

    #####################
    ## for non-electrical

    def __getQCStrTime(self, time):
        this_time = time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

        return  this_time

    def __createTestTemplate(self, doc, testType, str_date):
        project = {"project": doc["project"]["code"]}
        componentType = {"componentType": doc["componentType"]["code"]}
        pd_testType = {"code": testType}

        test_template = self.pd_client.get("generateTestTypeDtoSample", json={**project, **componentType, **pd_testType})
        json_template = {
            **test_template,
            "component": doc["serialNumber"],
            "institution": doc["currentLocation"]["code"],
            "date": str_date
        }

        return json_template

    def __createRawResultAttachment(self, result_doc, new_test_result, test_item):
        attachment_doc = result_doc
        attachment_doc["_id"] = str(attachment_doc["_id"])
        pdb_testType = result_doc["testType"]
        attachment_doc["sys"]["cts"] = result_doc["sys"]["cts"].isoformat()
        attachment_doc["sys"]["mts"] = result_doc["sys"]["mts"].isoformat()
        if "PIXEL_FAILURE_TEST" in result_doc["testType"]:
            try:
                attachment_doc["startTime"] = result_doc["startTime"].isoformat()
            except:
                pass
        f = open(result_doc["testType"] + '_results.json', 'w')
        attachment_doc["testType"] = str(test_item)
        json.dump(attachment_doc, f, indent=4)
        f.close()

        attachment_file = open(pdb_testType + '_results.json', 'rb')
        page_attachment = {"data": (pdb_testType + '_results.json', attachment_file, "json")}

        self.pd_client.post( "createTestRunAttachment", data=dict( testRun=new_test_result["testRun"]["id"], type="file" ), files=page_attachment )
        return  os.remove(pdb_testType + '_results.json')


    #######################
    ## for Optical Inspection
    def __createOpticalTestRun(self, result_doc, module_doc, str_date):
        json = self.__createTestTemplate(module_doc, result_doc["testType"], str_date)
        json["results"]["FULL_IMAGE"] = ""
        new_test_result = self.pd_client.post("uploadTestRunResults",json=json)

        pic_name = result_doc["currentStage"] + "_optical_img" + ".png"
        inspect_pic = result_doc["results"]["img_entire"]["target"]
        data = fs.get(ObjectId(str(inspect_pic))).read()
        with open(pic_name, 'wb') as f:
            f.write(data)
        f = open(pic_name, "rb")
        os.remove(pic_name)
        self.pd_client.post("createBinaryTestRunParameter", data=dict(testRun=new_test_result["testRun"]["id"], parameter="FULL_IMAGE"), files=dict(data=f) )
        f.close()

        return new_test_result

    def __AttachGoldens(self, result_doc, new_test_result, test_item):
        target_name = str(result_doc["results"]["img_entire"]["target"]) + ".png"
        target_pic = result_doc["results"]["img_entire"]["target"]
        data_target = fs.get(ObjectId(str(target_pic))).read()
        with open(target_name, 'wb') as f:
            f.write(data_target)

        golden_name = str(result_doc["results"]["img_entire"]["reference"]) + ".png"
        golden_pic = result_doc["results"]["img_entire"]["reference"]
        data_golden = fs.get(ObjectId(str(golden_pic))).read()
        with open(golden_name, 'wb') as f:
            f.write(data_golden)

        tiles = []
        for i, item in enumerate(result_doc["results"]["img_tile"]):
            tiles.append({})
            tiles[i]["name"] = str(result_doc["results"]["img_tile"][item]) + ".png"
            tiles[i]["pic"] = result_doc["results"]["img_tile"][item]
            tiles[i]["data"] = fs.get(ObjectId(str(tiles[i]["pic"]))).read()
            with open(tiles[i]["name"], 'wb') as f:
                f.write(tiles[i]["data"])

        zip_name = "GoldenImages_Inspect"
        zip_path = zip_name + ".zip"
        if os.path.exists(zip_path): os.remove(zip_path)
        zip_file = zipfile.ZipFile(zip_path, "w", zipfile.ZIP_DEFLATED)

        zip_file.write(target_name)
        os.remove(target_name)
        zip_file.close()
        self.__attachFileToTestRun(zip_path, zip_path, new_test_result["testRun"]["id"])
        os.remove(zip_path)

        zip_name = "GoldenImages_Golden"
        zip_path = zip_name + ".zip"
        if os.path.exists(zip_path): os.remove(zip_path)
        zip_file = zipfile.ZipFile(zip_path, "w", zipfile.ZIP_DEFLATED)

        zip_file.write(golden_name)
        os.remove(golden_name)
        zip_file.close()
        self.__attachFileToTestRun(zip_path, zip_path, new_test_result["testRun"]["id"])
        os.remove(zip_path)


        zip_name = "GoldenImages_Tile"
        zip_path = zip_name + ".zip"
        if os.path.exists(zip_path): os.remove(zip_path)
        zip_file = zipfile.ZipFile(zip_path, "w", zipfile.ZIP_DEFLATED)

        for item in tiles:
            zip_file.write(item["name"])
            os.remove(item["name"])
        zip_file.close()
        self.__attachFileToTestRun(zip_path, zip_path, new_test_result["testRun"]["id"])
        os.remove(zip_path)

        return


    #######################
    ## for Mass measurement
    def __createMassTestRun(self, result_doc, module_doc, str_date):
        json = self.__createTestTemplate(module_doc, result_doc["testType"], str_date)
        json["properties"]["ACCURACY"] = float('{:.3g}'.format(float(result_doc["results"]["property"]["Scale_accuracy"])))
        json["results"]["MASS"] = float('{:.3g}'.format(float(result_doc["results"]["mass_value"])))
        json["comments"] = [result_doc["results"]["comment"]]

        return self.pd_client.post("uploadTestRunResults",json=json)

    ################
    ## for Metrology
    def __createMetrologyTestRun(self, result_doc, module_doc, str_date):
        json = self.__createTestTemplate(module_doc, result_doc["testType"], str_date)
        json["results"]["DISTANCE_TOP"]    = float('{:.3g}'.format(float(result_doc["results"]["distance top"])))
        json["results"]["DISTANCE_LEFT"]   = float('{:.3g}'.format(float(result_doc["results"]["distance left"])))
        json["results"]["DISTANCE_RIGHT"]  = float('{:.3g}'.format(float(result_doc["results"]["distance right"])))
        json["results"]["DISTANCE_BOTTOM"] = float('{:.3g}'.format(float(result_doc["results"]["distance bottom"])))

        json["results"]["ANGLE_OF_BARE_VS_FLEX"] = [
          float('{:.3g}'.format(float(result_doc["results"]["angle top-left"]))),
          float('{:.3g}'.format(float(result_doc["results"]["angle top-right"]))),
          float('{:.3g}'.format(float(result_doc["results"]["angle bottom-left"]))),
          float('{:.3g}'.format(float(result_doc["results"]["angle bottom-right"])))
        ]

        json["results"]["MODULE_THICKNESS_PICKUP_AREA"] = [
          float('{:.3g}'.format(float(result_doc["results"]["module thickness pickup area chip1"]))),
          float('{:.3g}'.format(float(result_doc["results"]["module thickness pickup area chip2"]))),
          float('{:.3g}'.format(float(result_doc["results"]["module thickness pickup area chip3"]))),
          float('{:.3g}'.format(float(result_doc["results"]["module thickness pickup area chip4"])))
        ]
        try:
            json["results"]["MODULE_THICKNESS_EDGE"] = [
              float('{:.3g}'.format(float(result_doc["results"]["module thickness edge chip1"]))),
              float('{:.3g}'.format(float(result_doc["results"]["module thickness edge chip2"]))),
              float('{:.3g}'.format(float(result_doc["results"]["module thickness edge chip3"]))),
              float('{:.3g}'.format(float(result_doc["results"]["module thickness edge chip4"])))
            ]
        except:
            json["results"]["MODULE_THICKNESS_EDGE"] = [1.0, 1.0, 1.0, 1.0]

        json["results"]["MODULE_THICKNESS_HV_CAPACITOR"] = float('{:.3g}'.format(float(result_doc["results"]["module thickness HV capacitor"])))
        json["results"]["MODULE_THICKNESS_DATA_CONNECTOR"] = float('{:.3g}'.format(float(result_doc["results"]["module thickness Data connector"])))

        try:
            json["results"]["PLANARITY_VACUUM_ON"]  = float('{:.3g}'.format(float(result_doc["results"]["planarity vacuum on"])))
            json["results"]["PLANARITY_VACUUM_OFF"] = float('{:.3g}'.format(float(result_doc["results"]["planarity vacuum off"])))
            json["results"]["PLANARITY_VACUUM_ON_STD_DEV"]  = float('{:.3g}'.format(float(result_doc["results"]["planarity vacuum on std dev"])))
            json["results"]["PLANARITY_VACUUM_OFF_STD_DEV"] = float('{:.3g}'.format(float(result_doc["results"]["planarity vacuum off std dev"])))
        except:
            json["results"]["PLANARITY_VACUUM_ON"]  = 1.0
            json["results"]["PLANARITY_VACUUM_OFF"] = 1.0
            json["results"]["PLANARITY_VACUUM_ON_STD_DEV"]  = 1.0
            json["results"]["PLANARITY_VACUUM_OFF_STD_DEV"] = 1.0

        json["comments"] = [result_doc["results"]["comment"], "No results about Thickness Edge and Planarity Vacuum on/off"]

        return self.pd_client.post("uploadTestRunResults",json=json)

    ################
    ## for Sensor IV
    def __createSensorIVTestRun(self, result_doc, module_doc, str_date, test_item):
        json = self.__createTestTemplate(module_doc, result_doc["testType"], str_date)
        json["results"]["TIME"] = []
        json["results"]["CURRENT_MEAN"] = []
        json["results"]["CURRENT_SIGMA"] = []
        json["results"]["VOLTAGE"] = []
        json["results"]["HUMIDITY"] = []
        json["results"]["TEMPERATURE"] = []
        for items in result_doc["results"]["Sensor_IV"]:
            json["results"]["TIME"].append(float('{:.3g}'.format(items["Time"])))
            json["results"]["CURRENT_MEAN"].append(float('{:.3g}'.format(items["Current_mean"])))
            json["results"]["CURRENT_SIGMA"].append(float('{:.3g}'.format(items["Current_sigma"])))
            json["results"]["VOLTAGE"].append(float('{:.3g}'.format(items["Voltage"])))
            if "Humidity" in items:
                json["results"]["HUMIDITY"].append(float('{:.3g}'.format(items["Humidity"])))
            if "Temperature" in items:
                json["results"]["TEMPERATURE"].append(float('{:.3g}'.format(items["Temperature"])))
        json["comments"] = [result_doc["results"]["comment"]]
        if test_item == "SENSOR_IV_30_DEGREE":
            json["properties"]["ENVIRONMENT_TEMPERATURE"] = 30
        elif test_item == "SENSOR_IV_20_DEGREE":
            json["properties"]["ENVIRONMENT_TEMPERATURE"] = 20
        elif test_item == "SENSOR_IV_min15_DEGREE":
            json["properties"]["ENVIRONMENT_TEMPERATURE"] = -15
        else:
            json["properties"]["ENVIRONMENT_TEMPERATURE"] = 99999

        return self.pd_client.post("uploadTestRunResults",json=json)

    ##############
    ## for SLDO VI
    def __createSLDOVITestRun(self, result_doc, module_doc, str_date):
        json = self.__createTestTemplate(module_doc, result_doc["testType"], str_date)
        json["results"]["TIME"] = []
        json["results"]["CURRENT"] = []
        json["results"]["VOLTAGE_MEAN"] = []
        json["results"]["VOLTAGE_SIGMA"] = []
        json["results"]["HUMIDITY"] = []
        json["results"]["TEMPERATURE"] = []
        for items in result_doc["results"]["SLDO_VI"]:
            json["results"]["TIME"].append(float('{:.3g}'.format(items["Time"])))
            json["results"]["CURRENT"].append(float('{:.3g}'.format(items["Current"])))
            json["results"]["VOLTAGE_MEAN"].append(float('{:.3g}'.format(items["Voltage_mean"])))
            json["results"]["VOLTAGE_SIGMA"].append(float('{:.3g}'.format(items["Voltage_sigma"])))
            if "Humidity" in items:
                json["results"]["HUMIDITY"].append(float('{:.3g}'.format(items["Humidity"])))
            if "Temperature" in items:
                json["results"]["TEMPERATURE"].append(float('{:.3g}'.format(items["Temperature"])))
        json["comments"] = [result_doc["results"]["comment"]]

        return self.pd_client.post("uploadTestRunResults",json=json)

    ################################
    ## for Glue module + flex attach
    def __createGlueTestRun(self, result_doc, module_doc, str_date):
        json = self.__createTestTemplate(module_doc, result_doc["testType"], str_date)
        json["properties"]["GLUE_TYPE"] = result_doc["results"]["property"]["Glue_name"]
        Main_A = result_doc["results"]["property"]["Volume_ratio_of_glue_mixture"].split(":")[1].split(",")[0]
        Sub_B = result_doc["results"]["property"]["Volume_ratio_of_glue_mixture"].split(":")[2]
        json["properties"]["RATIO"] = float(Main_A)/float(Sub_B)
        json["properties"]["NAME"] = result_doc["user"]
        json["properties"]["BATCH_NUMBER"] = result_doc["results"]["property"]["Glue_batch_number"]
        json["results"]["TEMP"] = result_doc["results"]["Room_temperature"]
        json["results"]["HUMIDITY"] = result_doc["results"]["Humidity"]
        json["comments"] = [result_doc["results"]["comment"]]

        return self.pd_client.post("uploadTestRunResults",json=json)

    ##################
    ## for Wirebonding
    def __createWirebondingTestRun(self, result_doc, module_doc, str_date):
        json = self.__createTestTemplate(module_doc, result_doc["testType"], str_date)
        json["properties"]["MACHINE"] = result_doc["results"]["property"]["Machine"]
        json["properties"]["OPERATOR"] = result_doc["results"]["property"]["Operator_name"]
        json["properties"]["BOND_WIRE_BATCH"] = result_doc["results"]["property"]["Bond_wire_batch"]
        if result_doc["results"]["property"]["Bond_program"] != '':
            json["properties"]["BOND_PROGRAM"] = "Attached Bond Program"
        else:
            json["properties"]["BOND_PROGRAM"] = "No Bond Program"
        json["properties"]["BONDING_JIG"] = result_doc["results"]["property"]["Bonding_jig"]
        json["results"]["TEMPERATURE"] = result_doc["results"]["Temperature"]
        json["results"]["HUMIDITY"] = result_doc["results"]["Humidity"]
        json["comments"] = [result_doc["results"]["comment"]]

        return self.pd_client.post("uploadTestRunResults",json=json)

    ###############
    ## for Wirebond
    def __createWirebondTestRun(self, result_doc, module_doc, str_date):
        json = self.__createTestTemplate(module_doc, result_doc["testType"], str_date)
        json["properties"]["MACHINE"] = "No result"
        json["properties"]["SPEED"] = 0
        json["properties"]["SHEAR"] = 0
        json["properties"]["LOAD"] = 0
        json["properties"]["OPERATOR"] = "No result"
        json["results"]["MIN_LOAD"] = result_doc["results"]["minimum_load"]
        json["results"]["MAX_LOAD"] = result_doc["results"]["maximum_load"]
        json["results"]["MEAN_LOAD"] = result_doc["results"]["mean_load"]
        json["results"]["STD_DEV_LOAD"] = result_doc["results"]["load_standard_deviation"]
        json["results"]["PERCENT_HEEL_BREAKS"] = result_doc["results"]["percentage_of_heel_breaks"]
        json["comments"] = ["No properties for this test run" , result_doc["results"]["comment"]]

        return self.pd_client.post("uploadTestRunResults",json=json)

    #################
    ## for electrical

    def __getElecTestAnalysisResult(self, result_doc, chip_name):
        dict_ = {}
        if "analysis" in result_doc["results"]:
            for analysis in result_doc["results"]["analysis"]:
                if analysis["name"] == "bad_pixel_analysis":
                    for chip_result in analysis["result"]:
                        if chip_result["chip"] == chip_name:
                            for criteria in chip_result["result"]:
                                dict_[criteria["criteria"].upper()] = criteria["num"]
        return dict_

    def __getElecTestSettingTemp(self, result_doc, test_item):
        temp = result_doc["results"]["setting_temp"]
        try:
            temp = int(temp)
        except:
            if test_item == "PIXEL_FAILURE_TEST_30_DEGREE":
                temp = 30
            elif test_item == "PIXEL_FAILURE_TEST_20_DEGREE":
                temp = 20
            elif test_item == "PIXEL_FAILURE_TEST_min15_DEGREE":
                temp = -15
            else:
                temp = 99999
        return temp

    def __createElecTestTemplate(self, doc, testType, str_date):
        project = {"project": doc["project"]["code"]}
        componentType = {"componentType": doc["componentType"]["code"]}
        pd_testType = {"code": testType}

        test_template  = self.pd_client.get( "generateTestTypeDtoSample", json={**project, **componentType, **pd_testType} )

        json = {
          **test_template,
          "component": doc["serialNumber"],
          "institution": doc["currentLocation"]["code"],
          "date": str_date,
          "properties":{},
          "results":{}
        }
        return json

    def __createElecTestRunForModule(self, result_doc, module_doc, str_date, cp_test_map, test_item):
        json = self.__createElecTestTemplate(module_doc, result_doc["testType"], str_date)
        json["properties"] = {"ENVIRONMENT_TEMPERATURE":self.__getElecTestSettingTemp(result_doc, test_item)}
        json["results"]["RESULT_IDS"] = [v for i,v in cp_test_map.items()]
        json["results"]["DIGITAL_DEAD"] = []
        json["results"]["DIGITAL_BAD"] = []
        json["results"]["ANALOG_DEAD"] = []
        json["results"]["ANALOG_BAD"] = []
        json["results"]["TUNING_FAILED"] = []
        json["results"]["TUNING_BAD_THRESHOLD"] = []
        json["results"]["TUNING_BAD_TOT"] = []
        json["results"]["HIGH_ENC"] = []
        json["results"]["NOISY"] = []
        for chip in result_doc["results"]["analysis"][0]["result"]:
            for values in chip["result"]:
                if values["criteria"] == "digital_dead": json["results"]["DIGITAL_DEAD"].append(values["num"])
                if values["criteria"] == "digital_bad": json["results"]["DIGITAL_BAD"].append(values["num"])
                if values["criteria"] == "analog_dead": json["results"]["ANALOG_DEAD"].append(values["num"])
                if values["criteria"] == "analog_bad": json["results"]["ANALOG_BAD"].append(values["num"])
                if values["criteria"] == "tuning_failed": json["results"]["TUNING_FAILED"].append(values["num"])
                if values["criteria"] == "tuning_bad_threshold": json["results"]["TUNING_BAD_THRESHOLD"].append(values["num"])
                if values["criteria"] == "tuning_bad_tot": json["results"]["TUNING_BAD_TOT"].append(values["num"])
                if values["criteria"] == "high_enc": json["results"]["HIGH_ENC"].append(values["num"])
                if values["criteria"] == "noisy": json["results"]["NOISY"].append(values["num"])

        return self.pd_client.post("uploadTestRunResults",json=json)

    def __createElecTestRunForFEChip(self, result_doc, chip_name, str_date):
        child_doc = super().getCompFromProdDB(chip_name)
        json = self.__createElecTestTemplate(child_doc, result_doc["testType"], str_date)
        json["properties"] = {"QC_STAGE":result_doc["currentStage"]}
        json["results"] = self.__getElecTestAnalysisResult(result_doc, chip_name)

        return self.pd_client.post( "uploadTestRunResults", json=json)

    def __attachFileToTestRun(self, filepath, filename, runId):
        with open(filepath, "rb") as f:
            upload_file = f
            upload_filetype = filename.split(".")[-1]
            page_attachment = { "data": (filename, upload_file, upload_filetype)}
            self.pd_client.post( "createTestRunAttachment", data=dict( testRun=runId, type="file" ), files=page_attachment )
        return

    def __attachFileToComp(self, filepath, filename, module_doc):
        for attachment in module_doc["attachments"]:
            if attachment["filename"] == filename:
                self.pd_client.post( "deleteComponentAttachment", json = {"component":module_doc["serialNumber"], "code": attachment["code"]} )
                logger.info("Delete an old config for the module page: " + filename)
        with open(filepath, "rb") as f:
            upload_file = f
            upload_filetype = filename.split(".")[-1]
            page_attachment = { "data": (filename, upload_file, upload_filetype)}
            self.pd_client.post( "createComponentAttachment", data=dict( component=module_doc["serialNumber"], type="file" ), files=page_attachment )
            logger.info("Attach a new config for the module page: " + filename)
        return

    def __modifyScanLog(self, filepath, curStage):
        with open(filepath) as f: df = json.load(f)
        df.pop("_id")
        df["connectivity"][0]["stage"] = curStage
        with open(filepath, 'w') as f: json.dump(df, f, indent=4)
        return


    #######################
    ## for Pullup registor
    def __createPullupTestRun(self, prop_doc, module_doc):
        for i in range(len(prop_doc["results"]["value"])):
            n = i + 1
            json_name = "json" + str(n)
            json_name = {
                "component": module_doc["serialNumber"],
                "code": "RD53A_PULL-UP_RESISTOR" + str(n),
                "value": prop_doc["results"]["value"]["chip"+str(n)]
            }
            self.pd_client.post("setComponentProperty", json=json_name)

        return "0"

    ########################
    ## for IrefTrim registor
    def __createIreftrimTestRun(self, prop_doc, module_doc):
        for i in range(len(prop_doc["results"]["value"])):
            n = i + 1
            json_name = "json" + str(n)
            json_name = {
                "component": module_doc["serialNumber"],
                "code": "IREFTRIM_FE" + str(n),
                "value": prop_doc["results"]["value"]["chip"+str(n)]
            }
            self.pd_client.post("setComponentProperty", json=json_name)

        return "0"

    ###########################
    ## for Orientation registor
    def __createOrientationTestRun(self, prop_doc, module_doc):
        json_name = {
            "component": module_doc["serialNumber"],
            "code": "ORIENTATION",
            "value": prop_doc["results"]["orientation"]
        }

        return self.pd_client.post("setComponentProperty", json=json_name)

    ##################################
    ## Create & Delete Attachmet for Properties
    def __createPropertyAttachment(self, prop_doc, module_doc):
        attachment_doc = prop_doc
        attachment_doc["_id"] = str(attachment_doc["_id"])
        attachment_doc["sys"]["cts"] = prop_doc["sys"]["cts"].isoformat()
        attachment_doc["sys"]["mts"] = prop_doc["sys"]["mts"].isoformat()
        f = open(prop_doc["testType"] + '_detail.json', 'w')
        json.dump(attachment_doc, f, indent=4)
        f.close()

        attachment_file = open(prop_doc["testType"] + '_detail.json', 'rb')
        page_attachment = {"data": (prop_doc["testType"] + '_detail.json', attachment_file, "json")}

        self.pd_client.post( "createComponentAttachment", data=dict( component=module_doc["serialNumber"], type="file" ), files=page_attachment )
        return  os.remove(prop_doc["testType"] + '_detail.json')

    def __deletePropertyAttachment(self, module_doc):
        for item in module_doc["attachments"]:
            if "detail.json" in item["filename"]:
                self.pd_client.post("deleteComponentAttachment", json={"component": module_doc["code"], "code": item["code"]} )

        return

    ########################
    ## set stage of ITkPD ##
    def __setStage(self, module_doc, module_QC_info):
        nextStage = module_QC_info["currentStage"]
        json_stage = {"component": module_doc["serialNumber"], "stage": nextStage}

        return self.pd_client.post("setComponentStage", json=json_stage )


    ########################
    ## delete old results ##
    def __getOldResult(self, tr_doc):
        cache_dir = "{}/attachments".format(".")
        try:
            shutil.rmtree(cache_dir)
        except:
            pass
        os.mkdir(cache_dir)
        old_result_doc = {}
        for attachment in tr_doc["attachments"]:
            jfile = self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code": attachment["code"]}).content
            if "_results.json" in attachment["filename"]:
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"wb") as f:
                    f.write(jfile)
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"r") as f:
                    old_result_doc["0"] = json.load(f)

        return old_result_doc

#    def __deleteResult(self, tr_doc, new_result_id, old_result_doc):
#        cache_dir = "{}/attachments".format(".")
#        attachment_file = open('attachments/' + "delete.json", 'rb')
#        page_attachment = {"data": (old_result_doc["0"]["testType"] + "_deletedResults.json", attachment_file, "json")}
#        attach_ins = self.pd_client.post("createTestRunAttachment", data=dict( testRun=new_result_id, type="file" ), files=page_attachment )
#
#        self.pd_client.post("deleteTestRun", json={"testRun": tr_doc["id"]})
#        shutil.rmtree(cache_dir)
#        return page_attachment


###################
## main function ##
    def upload_results(self, msn):
        if not os.path.exists(IF_DIR):
            os.makedirs(IF_DIR)
        do_filepath = IF_DIR + "/doing_upload_" + msn + ".txt"
        with open(do_filepath, "w") as f:
            f.write("doing now")

        if os.path.exists(IF_DIR + '/doing_upload_' + msn + '_fail' + '.txt'):
             os.remove(IF_DIR + '/doing_upload_' + msn + '_fail' + '.txt')

        module_info = super().getCompFromLocalDB(msn)
        QC_info = localdb.QC.module.status.find_one({"component": str(module_info["_id"])})
        QC_info["stage_flow"] = [item for item in QC_info["QC_results"]]
        module_QC_info = super().getQmsFromLocalDB(str(module_info["_id"]))
        module_prop_info = super().getPmsFromLocalDB(str(module_info["_id"]))
        logger.info("Module name: " + msn)
        module_doc = super().getCompFromProdDB(msn)
        old_test_list = self.pd_client.get("listTestRunsByComponent", json={"component":module_doc["code"]})
        old_tests = [item for item in old_test_list]

        try:
            upload_status = module_QC_info["upload_status"]
        except:
            upload_status = {}
            for stage in QC_info["stage_flow"]:
                upload_status[stage] = "-1"
            localdb.QC.module.status.update_one( {"_id": ObjectId(str(module_QC_info["_id"]))}, {"$set": {"upload_status": upload_status }} )

        ## Below IF statement will be used for the case you want to procced stage of LDB and PDB at the same time.
#        if not module_doc["currentStage"]["code"] == module_QC_info["latestSyncedStage"]:
#            logger.info( "Stage is not corresponded to ITk production DB. Please check it" )
#        else:
        ## get the parent information and setting upload parameter
        ustages = QC_info["stage_flow"]
        upload_stage = []
        for stage in ustages:
            if stage == module_QC_info["currentStage"]:
                 break
            upload_stage.append(stage)

        for stage in upload_stage:
            try:
                if upload_status[stage] == "-1":
                    json_stage = {"component": module_doc["serialNumber"], "stage": stage}
                    self.pd_client.post("setComponentStage", json=json_stage )
                    logger.info("Stage: " + str(stage))
                    for test_item, result_id in module_QC_info["QC_results"][stage].items():
                        logger.info("Test Item: " + str(test_item))
                        result_doc = super().getQrFromLocalDB(result_id) if result_id != "-1" else {}
                        result_doc["old_tests"] = old_tests
                        new_result_id = self.__upload_functions(module_doc, result_doc, test_item)

                    if stage == "MODULEWIREBONDING":
                        self.__deletePropertyAttachment(module_doc)
                        for prop_item, prop_id in module_prop_info["QC_properties"].items():
                            logger.info("Property Item: " + str(prop_item))
                            prop_doc = super().getPrFromLocalDB(prop_id)
                            self.__upload_prop_functions(module_doc, prop_doc)

                    if stage == "MODULERECEPTION":
                        next_stage = "complete"
                        localdb.QC.module.status.update_one( {"_id": ObjectId(str(module_QC_info["_id"]))}, {"$set": {"latestSyncedStage": next_stage}} )
                    else:
                        next_stage = QC_info["stage_flow"][QC_info["stage_flow"].index(stage)+1]
                        #pd_client.post("setComponentStage", json = {"component":msn, "stage":next_stage})
                        localdb.QC.module.status.update_one( {"_id": ObjectId(str(module_QC_info["_id"]))}, {"$set": {"latestSyncedStage": next_stage}} )
                    upload_status[stage] = "0"
                    localdb.QC.module.status.update_one( {"_id": ObjectId(str(module_QC_info["_id"]))}, {"$set": {"upload_status": upload_status }} )

                logger.info("")
            except Exception as e:
                print(e)
                if result_id == "-1":
                    logger.info(stage + " is not signed off...")
                else:
                    logger.info("Failed to upload result of " + str(test_item) + "...")
                    if not os.path.exists(IF_DIR):
                        os.makedirs(IF_DIR)
                    with open(IF_DIR + '/doing_upload_' + msn + "_fail" + '.txt', 'w') as f:
                        f.write(str(test_item) + '\n' + result_id)

        logger.info("Finished for all results!!\n")

        if not os.path.exists(IF_DIR + '/doing_upload_' + msn + "_fail" + '.txt'):
            stage_doc = self.__setStage(module_doc, module_QC_info)
            logger.info( "Chenged Stage in ITk production DB to " + stage_doc["currentStage"] + "."  )
        else:
            logger.info("Not changed Stage in ITk production DB.")

        os.remove(do_filepath)
        try:
            shutil.rmtree("{}/attachments".format("."))
        except:
            pass

        if os.path.exists(IF_DIR + '/doing_upload_' + msn + '.txt'):
             os.remove(IF_DIR + '/doing_upload_' + msn + '.txt')
