import os, sys
import PDInterface

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

class QCResultDownloader(PDInterface.PDInterface):
    def __download_functions(self, module_doc, tr_doc):
        result_doc = self.__createLdbResultDoc(module_doc, tr_doc) # change the format from pdb to ldb
        functions = {
            "OPTICAL"                     :  self.__download_Optical,
            "MASS"                        :  self.__download_NonEle,
            "METROLOGY"                   :  self.__download_NonEle,
            "SENSOR_IV"                   :  self.__download_NonEle,
            "SLDO_VI"                     :  self.__download_NonEle,
            "GLUE_MODULE_FLEX_ATTACH"     :  self.__download_NonEle,
            "WIREBOND"                    :  self.__download_NonEle,
            "WIREBONDING"                 :  self.__download_Wirebonding,
            "PIXEL_FAILURE_TEST"          :  self.__download_electrical,
            "READOUT_IN_BASIC_ELECTRICAL" :  self.__download_electrical
        }

        if tr_doc["testType"]["code"] in functions:
            new_result = functions[tr_doc["testType"]["code"]](module_doc, result_doc, tr_doc)
        else: logger.info("There is no downloading function.")


        ## set stage for LocalDB
        localdb.QC.module.status.update({"component":str(result_doc["component"]),"proddbVersion":proddbv},{"$set":{ "currentStage": module_doc["currentStage"]["code"] }}  )

        ## set download status for each stage
        QC_info = userdb.QC.status.find_one({})
        module_QC_info = super().getQmsFromLocalDB(str(module_doc["id"]))
        try:
            upload_status = module_QC_info["upload_status"]
        except:
            upload_status = {}
            for stage in QC_info["stage_flow"]:
                upload_status[stage] = "-1"
            localdb.QC.module.status.update_one( {"_id": ObjectId(str(module_QC_info["_id"]))}, {"$set": {"upload_status": upload_status }} )

        upload_status[result_doc["currentStage"]] = "1"
        localdb.QC.module.status.update_one( {"_id": ObjectId(str(module_QC_info["_id"]))}, {"$set": {"upload_status": upload_status }} )

        try:
            return new_result
        except:
            return "skip"


    #######################
    ## create result doc ##
    def __createLdbResultDoc(self, module_doc, tr_doc):
        doc = {
          "_id":ObjectId(tr_doc["id"]),
          "component": module_doc["id"],
          "user": self.__getUserName(tr_doc["user"]),
          "address": tr_doc["institution"]["name"],
          "currentStage": tr_doc["components"][0]["testedAtStage"]["code"],
          "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
          "dbVersion":dbv,
          "testType":tr_doc["testType"]["code"],
#          "startTime":self.__getDateTime(tr_doc["date"]),
          "results":{}
        }
        return doc

    def __getDateTime(self, time):
        return datetime.strptime(time, '%d.%m.%Y %H:%M')

    def __getUserName(self, user_info):
        if user_info["middleName"]: return user_info["firstName"] + user_info["middleName"] + user_info["lastName"]
        else: return user_info["firstName"] + user_info["lastName"]

    #########################
    ## for module property ##
    def __download_props(self, module_doc):
        prop_docs = self.__createPropDocfromAttachment(module_doc)
        for prop_doc in prop_docs:
            try:
                super().insertDocToLdb("QC.module.prop", prop_doc)
            except:
                logger.info("This module property already exists in LocalDB.")
            try:
                stage_result = "QC_properties."  + prop_doc["testType"]
                localdb.QC.prop.status.update({"component":str(prop_doc["component"]),"proddbVersion":proddbv},{"$set":{ stage_result: str(prop_doc["_id"]) }}  )
            except:
                logger.info("Sign off list does not updated.")
        logger.info("Finished!!\n")
        return

    #########################
    ## for Module property ##
    def __createPropDocfromAttachment(self, module_doc):
        cache_dir = "{}/prop_attachments".format(".")
        os.mkdir(cache_dir)
        prop_docs = []
        for attachment in module_doc["attachments"]:
            if "detail.json" in attachment["filename"]:
                jfile = self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code": attachment["code"]}).content
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"wb") as f:
                    f.write(jfile)
                prop_doc = {}
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"r") as f:
                    prop_doc = json.load(f)
                prop_doc["_id"] = ObjectId(prop_doc["_id"])
                try:
                    prop_doc["sys"]["cts"] = datetime.fromisoformat(prop_doc["sys"]["cts"])
                    prop_doc["sys"]["mts"] = datetime.fromisoformat(prop_doc["sys"]["mts"])
                except:
                    prop_doc["sys"]["cts"] = datetime.strptime(prop_doc["sys"]["cts"], "%Y-%m-%dT%H:%M:%S.%f")
                    prop_doc["sys"]["mts"] = datetime.strptime(prop_doc["sys"]["mts"], "%Y-%m-%dT%H:%M:%S.%f")
                prop_docs.append(prop_doc)

        shutil.rmtree(cache_dir)
        return prop_docs


    ########################
    ## for non-electrical ##
    def __download_NonEle(self, module_doc, result_doc, tr_doc):
        result_doc = self.__createTestDocfromAttachment(module_doc, tr_doc)
        try:
            super().insertDocToLdb("QC.result", result_doc)
            new_result = result_doc
        except:
            logger.info("This test result already exists in LocalDB.")
        try:
            stage_result = "QC_results." + result_doc["currentStage"] + "." + result_doc["testType"]
            localdb.QC.module.status.update({"component":str(result_doc["component"]),"proddbVersion":proddbv},{"$set":{ stage_result: str(result_doc["_id"]) }}  )
        except:
            logger.info("Sign off list does not updated.")
        logger.info("Finished!!\n")

        try:
            return new_result
        except:
            return "skip"

    ############################
    ## for Optical Inspection ##
    def __download_Optical(self, module_doc, result_doc, tr_doc):
        result_doc = self.__createOpticalDocfromAttachment(module_doc, tr_doc)
        try:
            super().insertDocToLdb("QC.result", result_doc)
            self.__insertGoldenImages(module_doc, tr_doc)
            new_result = result_doc
        except:
            logger.info("This test result already exists in LocalDB.")
        try:
            stage_result = "QC_results." + result_doc["currentStage"] + "." + result_doc["testType"]
            localdb.QC.module.status.update({"component":str(result_doc["component"]),"proddbVersion":proddbv},{"$set":{ stage_result: str(result_doc["_id"]) }}  )
        except:
            logger.info("Sign off list does not updated.")
        logger.info("Finished!!\n")

        try:
            return new_result
        except:
            return "skip"

    def __insertGoldenImages(self, module_doc, tr_doc):
        os.mkdir("optical_data")
        for attachment in tr_doc["attachments"]:
            if "GoldenImages" in attachment["filename"]:
                jfile = self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code": attachment["code"]}).content
                with open(os.path.join("optical_data","{}".format(attachment["filename"])), "wb") as f:
                    f.write(jfile)
                with zipfile.ZipFile('optical_data/' + attachment["filename"]) as existing_zip:
                    existing_zip.extractall('optical_data/golden')

        goldens = os.listdir('optical_data/golden')
        for file_name in goldens:
            pic_id = file_name.split(".")[0]
            with open(os.path.join("optical_data/golden","{}".format(file_name)), "rb") as f:
                try:
                    fs.put(f, _id = ObjectId(pic_id))
                except:
                    logger.info("This picture for optical is already exists...")

        shutil.rmtree("optical_data")

    ##########################
    ## for Wirebonding info ##
    def __download_Wirebonding(self, module_doc, result_doc, tr_doc):
        result_doc = self.__createTestDocfromAttachment(module_doc, tr_doc)
        try:
            super().insertDocToLdb("QC.result", result_doc)
            self.__insertProgramToLdb(result_doc, tr_doc)
            new_result = result_doc
        except:
            logger.info("This test result already exists in LocalDB.")
        try:
            stage_result = "QC_results." + result_doc["currentStage"] + "." + result_doc["testType"]
            localdb.QC.module.status.update({"component":str(result_doc["component"]),"proddbVersion":proddbv},{"$set":{ stage_result: str(result_doc["_id"]) }}  )
        except:
            logger.info("Sign off list does not updated.")
        logger.info("Finished!!\n")

        try:
            return new_result
        except:
            return "skip"

    def __insertProgramToLdb(self, result_doc, tr_doc):
        os.mkdir("bonding_data")
        for attachment in tr_doc["attachments"]:
            if attachment["filename"] == "Bonding_Program.dat":
                jfile = self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code": attachment["code"]}).content
                with open(os.path.join("bonding_data","{}".format(attachment["filename"])), "wb") as f:
                    f.write(jfile)

                with open(os.path.join("bonding_data","{}".format(attachment["filename"])), "rb") as f:
                    fs.put(f, _id = ObjectId(result_doc["results"]["property"]["Bond_program"]))

        shutil.rmtree("bonding_data")
        return

    #############################
    ## for Non-electrical test ##
    def __createTestDocfromAttachment(self, module_doc, tr_doc):
        cache_dir = "{}/attachments".format(".")
        os.mkdir(cache_dir)
        for attachment in tr_doc["attachments"]:
            if attachment["filename"] == tr_doc["testType"]["code"] + "_results.json":
                jfile = self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code": attachment["code"]}).content
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"wb") as f:
                    f.write(jfile)
                result_doc = {}
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"r") as f:
                    result_doc = json.load(f)
                result_doc["_id"] = ObjectId(result_doc["_id"])
                try:
                    result_doc["sys"]["cts"] = datetime.fromisoformat(result_doc["sys"]["cts"])
                    result_doc["sys"]["mts"] = datetime.fromisoformat(result_doc["sys"]["mts"])
                except:
                    result_doc["sys"]["cts"] = datetime.strptime(result_doc["sys"]["cts"], "%Y-%m-%dT%H:%M:%S.%f")
                    result_doc["sys"]["mts"] = datetime.strptime(result_doc["sys"]["mts"], "%Y-%m-%dT%H:%M:%S.%f")

        shutil.rmtree(cache_dir)
        return result_doc

    #############################
    ## for Optical Inspection ##
    def __createOpticalDocfromAttachment(self, module_doc, tr_doc):
        cache_dir = "{}/attachments".format(".")
        os.mkdir(cache_dir)
        for attachment in tr_doc["attachments"]:
            if attachment["filename"] == tr_doc["testType"]["code"] + "_results.json":
                jfile = self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code": attachment["code"]}).content
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"wb") as f:
                    f.write(jfile)
                result_doc = {}
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"r") as f:
                    result_doc = json.load(f)
                result_doc["_id"] = ObjectId(result_doc["_id"])
                try:
                    result_doc["sys"]["cts"] = datetime.fromisoformat(result_doc["sys"]["cts"])
                    result_doc["sys"]["mts"] = datetime.fromisoformat(result_doc["sys"]["mts"])
                except:
                    result_doc["sys"]["cts"] = datetime.strptime(result_doc["sys"]["cts"], "%Y-%m-%dT%H:%M:%S.%f")
                    result_doc["sys"]["mts"] = datetime.strptime(result_doc["sys"]["mts"], "%Y-%m-%dT%H:%M:%S.%f")


        shutil.rmtree(cache_dir)
        return result_doc

    ####################
    ## for electrical ##
    ## main function
    def __download_electrical(self, module_doc, result_doc, tr_doc):
        result_doc = self.__createTestDocfromAttachment(module_doc, tr_doc)
        self.cache_dir = "{}/download_scans".format(IF_DIR)
        os.makedirs(self.cache_dir)

#        self.__renameElecTestType(result_doc, self.__getSettingTemp(tr_doc["properties"]))
        try:
            super().insertDocToLdb("QC.result", result_doc)
            new_result = result_doc
            self.__retrieveScanFiles(tr_doc)
            self.__modifyDbCfg()
            self.__uploadScansToLdb()
        except:
            logger.info("This test result already exists in LocalDB.")
        try:
            stage_result = "QC_results." + result_doc["currentStage"] + "." + result_doc["testType"]
            localdb.QC.module.status.update({"component":str(result_doc["component"]),"proddbVersion":proddbv},{"$set":{ stage_result: str(result_doc["_id"]) }}  )
        except:
            logger.info("Sign off list does not updated.")

        shutil.rmtree(self.cache_dir)

        try:
            return new_result
        except:
            return "skip"

    ## sub functions
    def __renameElecTestType(self, doc, temp):
        if temp:
            for d_ in self.stage_test_map[doc["currentStage"]]:
                if doc["testType"] == d_["pdb_testType"] and str(temp) in d_["ldb_testType"]:
                    doc["testType"] = d_["ldb_testType"]
                    break
        return

    def __retrieveScanFiles(self, tr_doc):
        # for module
        os.makedirs(self.cache_dir + "/zip_cache")
        for attachment in tr_doc["attachments"]:
            filename, filetype = attachment["filename"].split(".")
            bfile = super().getBinaryDataFromProdDB(attachment["code"])
            if filetype == "zip":
                os.mkdir("{}/{}".format(self.cache_dir, filename))
                zip_path = os.path.join(self.cache_dir,"zip_cache/module_{}".format(attachment["filename"]))
                super().writeBinaryData(zip_path,bfile)
                with zipfile.ZipFile(zip_path) as existing_zip:
                    existing_zip.extractall("{}/{}".format(self.cache_dir,filename))
            else:
                super().writeBinaryData(os.path.join(self.cache_dir,"{}".format(attachment["filename"])),bfile)

        # for fe chips
        for item in tr_doc["results"]:
            if item["code"] == "RESULT_IDS":
                for i, tr_id in enumerate(item["value"]):
                    chip_tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr_id})
                    for attachment in chip_tr_doc["attachments"]:
                        filename, filetype = attachment["filename"].split(".")
                        bfile = super().getBinaryDataFromProdDB(attachment["code"])
                        if filetype == "zip":
                            zip_path = os.path.join(self.cache_dir,"zip_cache/fechip{}_{}".format(str(i+1),attachment["filename"]))
                            super().writeBinaryData(zip_path,bfile)
                            with zipfile.ZipFile(zip_path) as existing_zip:
                                existing_zip.extractall("{}/{}".format(self.cache_dir,filename))

        return

    def __modifyDbCfg(self):
        with open(self.cache_dir + "/dbCfg.json") as f:
            df = json.load(f)
        df["hostIp"] = args.host
        df["hostPort"] = args.port
        df["ssl"]["CAFile"] = args.sslCAFile
        df["ssl"]["PEMKeyFile"] = args.sslPEMKeyFile
        df["ssl"]["enabled"] = args.ssl
        df["tls"]["CAFile"] = args.tlsCAFile
        df["tls"]["CertificateKeyFile"] = args.tlsCertificateKeyFile
        df["tls"]["enabled"] = args.tls
        with open(self.cache_dir + "/modified_dbCfg.json", 'w') as f:
            json.dump(df, f, indent=4)

        return

    def __uploadScansToLdb(self):
        scan_dir_path = [os.path.join(self.cache_dir,f) for f in self.__getScanDirs()]
        for dir_ in scan_dir_path:
            command = [
              "{}/itkpd-interface/localdb-interface/bin/localdbtool-upload".format(VIEWER_DIR),
              "scan", dir_,
              "--database", "{}/{}".format(self.cache_dir, "modified_dbCfg.json"),
              "--user", "{}/{}".format(self.cache_dir, "userCfg.json"),
              "--site", "{}/{}".format(self.cache_dir, "siteCfg.json"),
            ]
            subprocess.call(command)
        return

    def __createElectricalTestDoc(self, module_doc, tr_doc):
        return { "scans": self.__getScanIds(module_doc), "analysis": self.__getAnalysisResultDoc(tr_doc) }

    def __getSettingTemp(self, properties):
        temp = None
        for prop in properties:
            if prop["code"] == "ENVIRONMENT_TEMPERATURE": temp = prop["value"]
        return temp

    def __getScanIds(self, module_doc):
        files = os.listdir(self.cache_dir)
        scan_dirs = self.__getScanDirs()
        scan_ids = []
        this_tr_ids = [{"_id":ObjectId(l["testRun"])} for l in list(localdb.componentTestRun.find({"name": module_doc["serialNumber"]}, {"testRun": 1, "_id": 0}))]
        for scanType in scan_dirs:
            query = {"$and": [{"$or": this_tr_ids}, {"testType": scanType}]}
            latest_testRun = list(localdb.testRun.find(query,{"_id":1}).sort([("$natural", -1)]).limit(1))
            if latest_testRun: scan_ids.append({"name":scanType, "runId":str(latest_testRun[0]["_id"])})
        return scan_ids

    def __getAnalysisResultDoc(self, tr_doc):
        ## The current function does not retrieve analysis results from proddb. Analyze data with local functions.
        return []

    def __getScanDirs(self):
        files = os.listdir(self.cache_dir)
        return [f for f in files if os.path.isdir(os.path.join(self.cache_dir, f)) and "zip_cache" != f]

###################
## main function ##
    def download_results(self, msn):
        download_list = []
        if not os.path.exists(IF_DIR): os.makedirs(IF_DIR)
        do_filepath = IF_DIR + "/doing_download_result.txt"
        with open(do_filepath, "w") as f: f.write("doing now")

        if msn != None:
            logger.info("Module name: " + msn)
            module_doc = super().getCompFromProdDB(msn)
            if super().getCompFromLocalDB(msn):
                logger.info("The component is confirmed in LocalDB. Keep running the process.")
                logger.info("Get list of testRuns...")
                trs = self.pd_client.get("listTestRunsByComponent", json={"component":module_doc["code"]})
                for tr in trs:
                    if tr["state"] == "ready":
                        tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                        logger.info("Test Item: " + str(tr_doc["testType"]["code"]))
                        new_result = self.__download_functions(module_doc, tr_doc) # Execute downloading process in this line

                        if new_result != "skip":
                            download_list.append({"stage": new_result["currentStage"], "testType": new_result["testType"], "_id": new_result["_id"], "date": setTime(new_result["sys"]["cts"], session.get("timezone", str(get_localzone())) ), "scans": new_result["results"]})

                logger.info("Check if module properties are uploaded...")
                if module_doc["attachments"]:
                    self.__download_props(module_doc)
                else: logger.info("There is no property in ITkPD")

            else: logger.warning("The component does not exist in LocalDB. Please download the id at first.")

        logger.info("Finished!!\n")
        os.remove(do_filepath)

        return download_list

    def download_all_results(self):
        if not os.path.exists(IF_DIR):
            os.makedirs(IF_DIR)
        do_filepath = IF_DIR + '/doing_download.txt'
        with open(do_filepath, 'w') as f:
            f.write('doing now')

        module_find = localdb.component.find()
        for i in range(module_find.count()):
            if module_find[i]['componentType'] == 'module':
                try:
                    self.download_results(module_find[i]['name'])
                except:
                    logger.info("Something is wrong. Skip downloading QC results for this module.")

        logger.info("Finish downloading all results!!!")

        if os.path.exists(IF_DIR + '/doing_download.txt'):
             os.remove(IF_DIR + '/doing_download.txt')
