import os, sys, pwd, json, gridfs, shutil, time, uuid, pytz, base64, itkdb, ssl
import logging, logging.config
import coloredlogs
from getpass import getpass
from functions.arguments import *  # Pass command line arguments into app.py
from pymongo import MongoClient, DESCENDING, errors  # use mongodb scheme
from flask import url_for, session  # use Flask scheme
from datetime import datetime, timezone, timedelta
from pdf2image import convert_from_path  # convert pdf to image
from binascii import a2b_base64  # convert a block of base64 data back to binary
from bson.objectid import ObjectId  # handle bson format
from tzlocal import get_localzone
from urllib.parse import urlencode

#####################
### Database Settings
def readKey(i_path):
    file_text = open(i_path, "r")
    file_keys = file_text.read().split()
    keys = {"username": file_keys[0], "password": file_keys[1]}
    file_text.close()
    return keys


def setDb():
    max_server_delay = 10
    url = "mongodb://{0}:{1}".format(args.host, args.port)
    username = None
    password = None
    authSource = args.db

    ### check ssl
    db_tls = args.tls
    db_ssl = args.ssl
    if db_tls:
        db_ca_certs = args.tlsCAFile
        db_certfile = args.tlsCertificateKeyFile
    elif db_ssl:
        db_ca_certs = args.sslCAFile
        db_certfile = args.sslPEMKeyFile

    ### check tls
    params = {}
    if db_ssl or db_tls:
        params['ssl'] = 'true'
        if db_ca_certs:
            params['ssl_ca_certs'] = db_ca_certs
        if db_certfile:
            params['ssl_certfile'] = db_certfile
        if args.matchHostname:
            params['ssl_match_hostname'] = 'true'
        url += f"/?{urlencode(params)}"
        ### check auth mechanism
        db_auth = args.auth
        if db_auth == "x509":
            url += "&authMechanism=MONGODB-X509"
            authSource = "$external"

    client = MongoClient(url, serverSelectionTimeoutMS=max_server_delay,)
    localdb = client[args.db]

    try:
        localdb.list_collection_names()

    except errors.ServerSelectionTimeoutError as err:
        ### Connection failed
        print("The connection of Local DB {} is BAD.".format(url))
        print(err)
        sys.exit(1)

    except errors.OperationFailure as err:
        ### Need user authentication
        print("Need user authentication.")
        ### check user and password
        if args.KeyFile:
            keys = readKey(args.KeyFile)
            username = keys["username"]
            password = keys["password"]
        if args.username:
            username = args.username
        if args.password:
            password = args.password
        through = False
        while through == False:
            if not username or not password:
                answer = input("Continue? [y/n(skip)]\n> ")
                print("")
                if answer.lower() == "y":
                    username = None
                    password = None
                else:
                    sys.exit(1)
                username = input("User name > ")
                password = getpass("Password > ")
            try:
                localdb.authenticate(username, password)
                print("Authentication succeeded.")
                through = True
            except errors.OperationFailure as err:
                args.KeyFile = None
                print("Authentication failed.")
                answer = input("Try again? [y/n(skip)]\n> ")
                print("")
                if answer.lower() == "y":
                    username = input("User name > ")
                    password = getpass("Password > ")
                else:
                    sys.exit(1)

    client = MongoClient(
        url, username=username, password=password, authSource=authSource
    )

    return client

def getpwuid():
    euid = os.geteuid()
    try:
        return pwd.getpwuid(euid).pw_name
    except KeyError:
        # the uid can't be resolved by the system
        return str(euid)

###################
## global variables
if not "set_variables" in globals():
    args = getArgs()

    localdb_url = "://{0}:{1}/localdb".format(args.fhost, args.fport)

    client = setDb()
    localdb = client[args.db]
    userdb = client[args.userdb]
    fs = gridfs.GridFS(localdb)

    dbv = 1.01
    proddbv = 1.02

    VIEWER_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    TMP_DIR = "/var/tmp/{}".format(getpwuid())
    THUMBNAIL_DIR = "{}/thumbnail".format(TMP_DIR)
    JSON_DIR = "{}/json".format(TMP_DIR)
    CACHE_DIR = "{}/cache".format(TMP_DIR)
    IF_DIR = "{}/itkpd-interface".format(TMP_DIR)
    JSROOT_DIR = "{}/jsroot".format(TMP_DIR)
    AT_DIR = "{}/analysis-tool".format(TMP_DIR)

    if os.path.isdir(TMP_DIR):
        shutil.rmtree(TMP_DIR)

    with open("{}/json-lists/scan_datafile_list.json".format(VIEWER_DIR)) as f:
        DATA_SELECTION_LIST = json.load(f)

    EXTENSIONS = ["png", "jpeg", "jpg", "JPEG", "jpe", "jfif", "pjpeg", "pjp", "gif"]

    DAT_MIMETYPE = "application/octet-stream"
    JSON_MIMETYPE = "application/json"
    ZIP_MIMETYPE = "application/zip"
    ROOT_MIMETYPE = "application/root"

    set_variables = True

# Prefix
class PrefixMiddleware(object):
    def __init__(self, app, prefix=""):
        self.app = app
        self.prefix = prefix

    def __call__(self, environ, start_response):

        if environ["REQUEST_URI"].startswith(self.prefix):
            if environ["PATH_INFO"].startswith(self.prefix):
                environ["PATH_INFO"] = environ["PATH_INFO"][len(self.prefix) :]
            environ["SCRIPT_NAME"] = self.prefix
            return self.app(environ, start_response)
        else:
            start_response("404", [("Content-Type", "text/plain")])
            return ["This url does not belong to the app.".encode()]


# Python logging
# https://stackoverflow.com/questions/17743019/flask-logging-cannot-get-it-to-write-to-a-file
class Logger:
    def setupLogging(self, logfile="localdbtool.log"):
        # Create log directory if need
        if len(logfile.split("/")) > 1:
            log_directory = logfile.rsplit("/", 1)[0]
            if not os.path.exists(log_directory):
                os.makedirs(log_directory)
        logging.basicConfig(
            level=logging.DEBUG,
            format="%(asctime)s %(levelname)-8s %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
            filename="%s" % (logfile),
            filemode="a",
        )
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        formatter = logging.Formatter("%(levelname)-8s %(message)s")
        console.setFormatter(formatter)
        logging.getLogger("").addHandler(console)
        # color logging
        coloredlogs.install()

    def setFuncName(self, funcname):
        self.funcname = funcname

    def info(self, message):
        logging.info(self.toolname + " " + message)

    def warning(self, message):
        logging.warning(self.toolname + " " + message)

    def debug(self, message):
        logging.debug(self.toolname + self.funcname + " " + message)

    def error(self, message, exit_code=100):
        logging.error(self.toolname + self.funcname + " " + message)
        exit(exit_code)

    def __init__(self, toolname=""):
        self.toolname = toolname
        self.funcname = ""


# Setup logging
logger = Logger("[LDB]")
if args.logfile:
    logger.setupLogging(logfile=args.logfile)
else:
    logger.setupLogging()


#################
### Functions ###
#################
def makeDir():
    if not os.path.isdir(TMP_DIR):
        os.mkdir(TMP_DIR)
    user_dir = "{0}/{1}".format(TMP_DIR, session["uuid"])
    _DIRS = [THUMBNAIL_DIR, JSON_DIR, user_dir, JSROOT_DIR]
    for dir_ in _DIRS:
        if not os.path.isdir(dir_):
            os.mkdir(dir_)


def cleanDir(dir_name):
    if os.path.isdir(dir_name):
        shutil.rmtree(dir_name)
    os.mkdir(dir_name)


def cleanDirs(dir_name):
    if os.path.isdir(dir_name):
        shutil.rmtree(dir_name)
    os.makedirs(dir_name)


def bin2image(i_type, i_binary):
    if i_type in EXTENSIONS:
        data = "data:image/png;base64,{}".format(i_binary)
    elif i_type == "pdf":
        pdf_file = open("{}/image.pdf".format(TMP_DIR), "wb")
        bin_data = a2b_base64(i_binary)
        pdf_file.write(bin_data)
        pdf_file.close()
        path = "{}/image.pdf".format(TMP_DIR)
        image = convert_from_path(path)
        image[0].save("{}/image.png".format(TMP_DIR), "png")
        png_file = open("{}/image.png".format(TMP_DIR), "rb")
        binary = base64.b64encode(png_file.read()).decode()
        png_file.close()
        data = "data:image/png;base64,{}".format(binary)
    return data


def setTime(date, zone):
    # zone = session.get('timezone','UTC')
    # zone = session.get('timezone',str(get_localzone()))
    converted_time = date.replace(tzinfo=timezone.utc).astimezone(pytz.timezone(zone))
    time = converted_time.strftime("%Y/%m/%d %H:%M:%S")
    return time


def initPage():
    if not "uuid" in session:
        session["uuid"] = str(uuid.uuid4())
    if not "timezone" in session:
        # session['timezone'] = 'UTC'
        session["timezone"] = str(get_localzone())
    makeDir()


def updateData(i_col, i_oid):
    query = {"_id": ObjectId(i_oid)}
    this = localdb[i_col].find_one(query)
    now = datetime.utcnow()
    if not this:
        return
    if this.get("sys", {}) == {}:
        doc_value = {"$set": {"sys": {"cts": now, "mts": now, "rev": 0}}}
    else:
        doc_value = {
            "$set": {
                "sys": {
                    "cts": this["sys"].get("sys", now),
                    "mts": now,
                    "rev": this["sys"]["rev"] + 1,
                }
            }
        }
    localdb[i_col].update_one(query, doc_value)


def setDatetime(date):
    zone = session.get("timezone", "UTC")
    converted_time = date.replace(tzinfo=timezone.utc).astimezone(pytz.timezone(zone))
    return converted_time

def get_user(code1, code2):
    try:
        u = itkdb.core.User(accessCode1=code1, accessCode2=code2)
    except:
        logger.warning(
            "Not authorized. Please login for ITkPD by using itkpd-interface/authenticate.sh"
        )
        u = None
    return u

def process_request(code1, code2):
    try:
        u = itkdb.core.User(accessCode1=code1, accessCode2=code2)
        pd_client = itkdb.Client(user=u)
        docs = pd_client.get("listInstitutions", json={})
        logger.info("Authorized.")
        request = 1
    except:
        logger.warning(
            "Not authorized. Please login for ITkPD by using itkpd-interface/authenticate.sh"
        )
        request = 0
    return request

### remove after checking
#def user_institution(code1, code2):
#    u = itkdb.core.User(accessCode1=code1, accessCode2=code2)
#    u.authenticate()
#    client = itkdb.Client(user=u)
#    userinfo = client.get("getUser", json={"userIdentity":u.identity})
#    institution = userinfo["institutions"][0]["code"]
#    return institution
#
#def check_assemble(module_type, child_type, serialnumber, look_up_table):
#    check = "-1"
#    if look_up_table["MODULE_TYPES"][module_type][child_type] == serialnumber[3:7]:
#        check = "1"
#    else:
#        check = "0"
#    return check
#
#def check_parent(serialnumber, code1, code2):
#    u = itkdb.core.User(accessCode1=code1, accessCode2=code2)
#    u.authenticate()
#    client = itkdb.Client(user=u)
#    check = "-1"
#    try:
#        module_ins = client.get("getComponent", json={"component": serialnumber})
#        parent = module_ins["parents"]
#        if parent == []:
#            check = "1"
#        else:
#            check = "0"
#    except:
#        check = "2"
#    return check
#
#def check_FEchips(code1, code2, bare_sn):
#    u = itkdb.core.User(accessCode1=code1, accessCode2=code2)
#    u.authenticate()
#    client = itkdb.Client(user=u)
#    try:
#        bare_doc = client.get("getComponent",json={ "project": "P", "component": bare_sn})
#        for fe_info in bare_doc["children"]:
#            if fe_info["componentType"]["code"] == "FE_CHIP":
#                if fe_info["component"] == None:
#                    return False
#    except:
#        return True
#    return True
#
#def register_Module(code1, code2, config, look_up_table):
#    check = 0
#    u = itkdb.core.User(accessCode1=code1, accessCode2=code2)
#    pd_client = itkdb.Client(user=u)
#
#    json = {
#      "project":"P",
#      "subproject":look_up_table["MODULE_TYPES"][config["type"]]["XX"],
#      "institution":config["institution"],
#      "componentType":"MODULE",
#      "type":config["type"],
#      "properties":{
#        "FECHIP_VERSION":config["FECHIP"],
#        "ORIENTATION":eval("True")
#      },
#      "serialNumber":config["serialNumber"]
#    }
#    module_ins = pd_client.post("registerComponent", json=json)
#    logger.info("Register module!!")
#
#    module_doc = pd_client.get('getComponent', json={"component": module_ins["component"]["code"]})
#
#    if "BARE_MODULE" in config["child"]:
#        for j in range(len(config["child"]["BARE_MODULE"])):
#            json = {
#              "parent":module_ins["component"]["code"],
#              "slot":module_doc["children"][j+1]["id"],
#              "child":config["child"]["BARE_MODULE"][j]
#            }
#            if check_assemble(config["type"], "BARE_MODULE", json["child"], look_up_table) == 0:
#                logger.info("Couldn't assemble BARE_MODULE because the BARE_MODULE and the module are not the correct combination. Please check them and assemble on the Web page.")
#                check = 1
#            elif check_FEchips(code1,code2,config["child"]["BARE_MODULE"][j]):
#                logger.info("Couldn't assemble BARE_MODULE because the BARE_MODULE does not have the all FE chips as its children. Please check them and assemble on the Web page.")
#                check = 1
#            else:
#                # assemble the bare module to the module
#                pd_client.post("assembleComponentBySlot", json=json)
#                logger.info("Assemble BARE_MODULE!!")
#
#    if "PCB" in config["child"]:
#        json = {
#          "parent":module_ins["component"]["code"],
#          "child":config["child"]["PCB"]
#        }
#        if check_assemble(config["type"], "PCB", json["child"], look_up_table) == 0:
#            logger.info("Couldn't assemble PCB because the PCB and the module are not the correct combination. Please check them and assemble on the Web page.")
#            if check == 0:
#                check = 2
#            if check == 1:
#                check = 3
#        else:
#            # assemble the PCB to the module
#            pd_client.post("assembleComponent", json=json)
#            logger.info("Assemble PCB!!")
#
#    if "CARRIER" in config["child"]:
#        json = {
#          "parent":module_ins["component"]["code"],
#          "child":config["child"]["CARRIER"]
#        }
#        # assemble the module carrier to the module if need it
#        pd_client.post("assembleComponent", json=json)
#        logger.info("Assemble MODULE_CARRIER!!")

def createScanCache(entry):

    this_run = entry

    ### user
    query = {"_id": ObjectId(this_run["user_id"])}
    this_user = localdb.user.find_one(query)
    user_name = this_user["userName"]

    ### site
    query = {"_id": ObjectId(this_run["address"])}
    this_site = localdb.institution.find_one(query)
    site_name = this_site["institution"]

    ### put tags
    query = {"runId": str(entry["_id"])}
    testRun_tags = userdb.viewer.tag.docs.find(query)
    tags = []
    for testRun_tag in testRun_tags:
        tags.append(testRun_tag)

    ### component
    query = {"testRun": str(entry["_id"])}
    ctr_entries = localdb.componentTestRun.find(query)
    components = []
    for this_ctr in ctr_entries:
        if not this_ctr["component"] == "...":
            cmp_id = this_ctr["component"]
            collection = "component"
        else:
            cmp_id = this_ctr["chip"]
            collection = "chip"
        components.append(
            {"name": this_ctr.get("name", "NONAME"),}
        )

    query_targets = []
    for component in components:
        query_targets.append(component["name"])
    query_targets.append(this_run["testType"])
    query_targets.append(user_name)
    query_targets.append(site_name)
    query_targets.append(this_run["startTime"].strftime('%Y/%m/%d'))
    for tag in tags:
        query_targets.append(tag["name"])

    docs = {
        "runId": str(entry["_id"]),
        "timeStamp": this_run["startTime"],
        "data": query_targets,
    }

    return docs

def getScanSummary(run_id):

    query = {"_id": ObjectId(run_id)}
    this_run = localdb.testRun.find_one(query)

    ### user
    query = {"_id": ObjectId(this_run["user_id"])}
    this_user = localdb.user.find_one(query)
    user_name = this_user["userName"]

    ### site
    query = {"_id": ObjectId(this_run["address"])}
    this_site = localdb.institution.find_one(query)
    site_name = this_site["institution"]

    ### tags
    query = {}
    testRun_tag_candidates = userdb.viewer.tag.categories.find(query)
    tag_candidate = []
    for testRun_tag in testRun_tag_candidates:
        tag_candidate.append(testRun_tag)
    ### put tags
    query = {"runId": run_id}
    testRun_tags = userdb.viewer.tag.docs.find(query)
    tag = []
    for testRun_tag in testRun_tags:
        tag.append(testRun_tag)

    ### component
    query = {"testRun": run_id}
    ctr_entries = localdb.componentTestRun.find(query)
    components = []
    for this_ctr in ctr_entries:
        if not this_ctr["component"] == "...":
            cmp_id = this_ctr["component"]
            collection = "component"
        else:
            cmp_id = this_ctr["chip"]
            collection = "chip"
        components.append(
            {
                "name": this_ctr.get("name", "NONAME"),
                "enabled": this_ctr.get("enable", 1) == 1,
                "_id": cmp_id,
                "chip_id": this_ctr["chip"],
                "collection": collection,
            }
        )
    run_data = {
        "_id": run_id,
        "datetime": setTime(
            this_run["startTime"], session.get("timezone", str(get_localzone()))
        ),
        "testType": this_run["testType"],
        "runNumber": this_run["runNumber"],
        "stage": this_run["stage"],
        "plots": this_run["plots"] != [],
        "components": components,
        "user": user_name,
        "site": site_name,
        "testRun_tag_candidate": tag_candidate,
        "testRun_tag": tag,
    }

    return run_data

