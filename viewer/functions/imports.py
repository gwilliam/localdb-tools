# ==============================
# Default modules
# ==============================
import os, sys, datetime, dateutil.tz
import glob
import re
import ast
import hashlib
import shutil
import uuid  # Get mac address
import base64  # Base64 encoding scheme
import gridfs  # gridfs system
import io
import json
import string
import secrets
import pytz
import zipfile
import requests
import threading
import subprocess
import glob
import traceback
from getpass import getpass

# ==============================
# Log
# ==============================
import logging, logging.config
import coloredlogs

# ==============================
# For input
# ==============================
import yaml, argparse

# ==============================
# Pymongo and flask
# ==============================
from flask import (
    Flask,
    request,
    redirect,
    url_for,
    render_template,
    session,
    make_response,
    jsonify,
    Blueprint,
    current_app,
    send_file,
    abort
)
from pymongo import (
    MongoClient,
    DESCENDING,
    ASCENDING,
    errors,
)  # Pymongo, oh why not use flask-pymongo?
from flask_mail import Mail, Message

# Bson
from bson.objectid import ObjectId

# ?
from PIL import Image

# ==============================
# Plot
# ==============================
import plotly
import plotly.graph_objs as plotlygo

# ==============================
# Add python path
# ==============================
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# ==============================
# functions
# ==============================
# from functions.logging import *
from functions.common import *
from functions import listset
from functions import plot_root, plot_dcs
from functions.SensorIV import *

sys.path.insert(
    0,
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "../itkpd-interface/lib"),
)
from download_QC_info import *
from download_stage_info import *
from download_institution import *
from download_ModuleType import *
from register_Module import *
from upload_results import *
from download_results import *
from PDInterface import *

sys.path.insert(
    0,
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "../analysis-tool"),
)
