# Author : Shohei Yamagata(Osaka univ.)

from functions.common import *
from datetime import datetime, timezone
from functions import pyPlot as plt


environment_notData=[
    '_id',
    'sys',
    'dbVersion'
]

dcs_key_list = ["vddd", "vdda", "hv"]

iv_key_list = [
    {"name": "vddd", "key_v": "vddd_voltage", "key_i": "vddd_current", "num": 0},
    {"name": "vdda", "key_v": "vdda_voltage", "key_i": "vdda_current", "num": 0},
    {"name": "hv", "key_v": "hv_voltage", "key_i": "hv_current", "num": 0},
]


other_key_list=[
    {
        'key' : 'temperature',
        'num' : 0
    }
]
Graph_yrange = {
    "vddd_voltage": [-0.2, 2.0, 0.1],
    "vddd_current": [-0.2, 1.0, 0.1],
    "vdda_voltage": [-0.2, 2.0, 0.1],
    "vdda_current": [-0.2, 1.0, 0.1],
    "hv_voltage": [-100.0, 10.0, 1.0],
    "hv_current": [-7.0e-6, 1.0e-6, 1.0e-6],
    "temperature": [-40.0, 40.0, 1.0],
}
dat_row = ["dcsType", "num", "chipname", "description"]


class DCS_type(object):
    def __init__(self, data_block=None):
        self.startTime = 0
        self.finishTime = 0
        if data_block:
            self.__dcs_data = data_block

    def get_entry(self, key, num):
        try:
            return len(self.__dcs_data[key][num]["data"])
        except:
            return None

    def get_description(self, key, num):
        return self.__dcs_data[key][num]["description"]

    def get_single_data(self, key, num, i):
        data_time = 0
        try:
            data = float(self.__dcs_data[key][num]["data"][i]["value"])
        except:
            data = None
        else:
            data_time = self.__dcs_data[key][0]["data"][i]["date"]
            data_time = time.mktime(data_time.timetuple())
        return data_time, data

    def set_RunTime(self, start, finish):
        self.startTime = start
        self.finishTime = finish

    def set_timeRange(self, time1, time2):
        self.timeRange = [time1, time2]


def writeDcsDat(chipname, envId, dcsPlotList):
    query = {"_id": ObjectId(envId)}
    environments = localdb.environment.find_one(query)
    for candidate in environments :
        if candidate in environment_notData :
            continue
        if environments.get(candidate) is None:
            continue
        env_data_num = environments.get(candidate)
        if not env_data_num is None:
            if dcsPlotList.get(candidate) is None:
                dcsPlotList[candidate] = {}
            num = 0
            for env_data_list in env_data_num:
                if dcsPlotList[candidate].get(num) is None:
                    dcsPlotList[candidate][num] = {}
                file_path = (
                    TMP_DIR
                    + "/"
                    + str(session.get("uuid", "localuser"))
                    + "/dcs/dat/"
                    + str(chipname)
                    + "_"
                    + str(candidate)
                    + "_"
                    + str(num)
                    + ".dat"
                )
                if not env_data_list.get("data") is None:
                    with open(file_path, "w") as f:
                        description = env_data_list["description"]
                        f.write(candidate + "\n")
                        f.write(str(num) + "\n")
                        f.write(chipname + "\n")
                        f.write(description + "\n")

                        for env_data in env_data_list["data"]:
                            date = env_data["date"]
                            date = date.replace(tzinfo=timezone.utc)
                            # text=str(time.mktime(env_data['date'].timetuple()))+' '+str(env_data['value'])+'\n'
                            text = (
                                str(date.timestamp())
                                + " "
                                + str(env_data["value"])
                                + "\n"
                            )
                            f.write(text)

                    dcsPlotList[candidate][num][chipname] = {"dat": file_path}
                num += 1
    return dcsPlotList


def make_dcsplot(runId):
    dcsPlotList = {}
    collection = session["collection"]
    chip_oids = []
    if session["unit"] == "module":
        query = {"parent": session["this"]}
        chip_oids.append(session["this"])
        cpr_entries = localdb.childParentRelation.find(query)
        for this_cpr in cpr_entries:
            chip_oids.append(this_cpr["child"])
    elif session["this"]:
        chip_oids.append(session["this"])
    else:
        query = {"testRun": runId}
        ctr_entries = localdb.componentTestRun.find(query)
        for this_ctr in ctr_entries:
            chip_oids.append(this_ctr["chip"])
    for cmp_oid in chip_oids:
        query = {"testRun": runId, collection: cmp_oid}
        environmentIds = []
        comp_this_run = localdb.componentTestRun.find_one(query)

        environmentId = comp_this_run.get("environment")
        if not environmentId == "..." and not environmentId is None:
            envId = environmentId
            chipname = comp_this_run.get("name")
            dcsPlotList = writeDcsDat(chipname, envId, dcsPlotList)
    startTime = localdb.testRun.find_one({"_id": ObjectId(runId)}).get("startTime")
    finishTime = localdb.testRun.find_one({"_id": ObjectId(runId)}).get("finishTime")
    startTime = time.mktime(startTime.timetuple())
    finishTime = time.mktime(finishTime.timetuple())

    if not session["dcsStat"].get("timeRange"):
        session["dcsStat"].update({"timeRange": [startTime - 10, finishTime + 10]})
    if not session["dcsStat"].get("RunTime"):
        session["dcsStat"].update({"RunTime": [startTime, finishTime]})

    session["dcsPlot"] = dcs_plot(dcsPlotList)


def make_dir(DIR):
    if not os.path.isdir(DIR):
        os.mkdir(DIR)


# matplotlib
def make_dcsGraph_pyPlot(dat, num):
    timeRange = session["dcsStat"].get("timeRange")
    with open(dat, "r") as f:
        datStatus = {}
        timestamp_list = []
        value_list = []
        for line, readline in enumerate(f.readlines()):
            if line < len(dat_row):
                datStatus.update({dat_row[line]: readline})
            elif line >= len(dat_row):
                data = readline.split()
                timestamp_list.append(float(data[0]))
                value_list.append(float(data[1]))
    entry_num = len(timestamp_list)
    dcsType = datStatus.get("dcsType").rstrip("\n")
    if not entry_num == len(value_list):
        return None
    graph = plt.pyGraph()

    title = (
        str(datStatus.get("dcsType"))
        + ";Time("
        + session.get("timezone", "UTC")
        + ");"
        + str(datStatus.get("description"))
    )
    chipname = datStatus.get("chipname")
    graph.SetTitle(title)
    entry = 0
    data_min = 0
    data_max = 0
    for timestamp, value in zip(timestamp_list, value_list):
        time_point = datetime.utcfromtimestamp(timestamp)
        time_point.replace(tzinfo=timezone.utc)
        time_point = setDatetime(time_point)
        graph.SetPoint(time_point, value)
        if data_max < value:
            data_max = value
        if data_min > value:
            data_min = value

        entry += 1

    if session["dcsStat"].get(dcsType + "-" + str(num)):
        y_min = float(session["dcsStat"][dcsType + "-" + str(num)].get("min"))
        y_max = float(session["dcsStat"][dcsType + "-" + str(num)].get("max"))
        step = float(session["dcsStat"][dcsType + "-" + str(num)].get("step"))
    elif Graph_yrange.get(dcsType):
        y_min = Graph_yrange[dcsType][0]
        y_max = Graph_yrange[dcsType][1]
        step = Graph_yrange[dcsType][2]
    else:
        y_min = data_min
        y_max = data_max
        step = 1

    graph.SetYRange(y_min, y_max)
    graph.SetXAxis_Time()
    graph.SetXRange(
        datetime.fromtimestamp(timeRange[0]), datetime.fromtimestamp(timeRange[1])
    )

    RunTime = session["dcsStat"].get("RunTime")
    graph.SetXRegion(
        datetime.fromtimestamp(RunTime[0]), datetime.fromtimestamp(RunTime[1])
    )

    graph_stat = {
        "graph": graph,
        "chip": chipname,
        "min": y_min,
        "max": y_max,
        "step": step,
    }
    return graph_stat


def create_dcsPlot_pyPlot(num, dcsType, dat_list):
    # if dat_list==[] :
    #    return {}

    RunTime = session["dcsStat"].get("RunTime")
    picture_DIR = "/var/tmp/{0}/{1}/dcs/plot/".format(
        pwd.getpwuid(os.geteuid()).pw_name, session.get("uuid", "localuser")
    )
    picture_type = ".png"
    make_dir(picture_DIR)

    graph_list = []
    chip_list = []
    dat_num = 0
    canvas = plt.pyCanvas()
    for dat in dat_list:
        graph_stat = make_dcsGraph_pyPlot(dat, num)
        graph = graph_stat.get("graph")
        graph.SetCanvas(canvas)
        chipname = graph_stat.get("chip")
        if not graph is None:
            graph_list.append(graph)
            chip_list.append(chipname)
        if dat_num == 0:
            y_min = graph_stat.get("min")
            y_max = graph_stat.get("max")
            step = graph_stat.get("step")
        dat_num += 1
    chip_num = len(graph_list)
    gr_num = 1
    for gr, chip in zip(graph_list, chip_list):
        # gr.SetLineColor(gr_num)
        # gr.SetMarkerColor(gr_num)
        gr.SetName(chip)
        if gr_num == 1:
            gr.Draw()
        else:
            gr.Draw("same")
        gr_num += 1
    canvas.SetLegend()
    picture_path = str(picture_DIR) + str(dcsType) + "-" + str(num) + str(picture_type)
    canvas.Print(picture_path)
    GraphStat = {"filename": picture_path, "max": y_max, "min": y_min, "step": step}

    return GraphStat


## This comment out is  tool to plot Graph of dcs data with matplotlib
## comment out : 2019/07/07 by Yamagaya
"""
def make_Graph_with_matplotlib(DCS, key, num):
    picture_DIR='/var/tmp/{0}/{1}/dcs/'.format( pwd.getpwuid( os.geteuid() ).pw_name , session.get('uuid','localuser') )
    picture_type='.png'
    make_dir( picture_DIR )

    GraphStat={}

    i_Entry=0
    data=''
    data_min=0;
    data_max=0;

    if DCS.get_entry(key,num) == None :
        return None
    c=plt.pyCanvas()
    g=plt.pyGraph()
    g.SetCanvas(c)
    while data != None :
        data_time,data=DCS.get_single_data(key,num,i_Entry)
        if data == None :
            break
        data_time=datetime.fromtimestamp(data_time)
        g.SetPoint(data_time,data)
        if data_max < data :
            data_max=data
        if data_min > data :
            data_min=data
        i_Entry=i_Entry+1
    g.SetTitle(key)
    g.SetXAxisTitle('time')
    g.SetYAxisTitle(DCS.get_description(key,num))

    if session['dcsStat'].get( key ) :
        y_min=float(session['dcsStat'][key].get('min'))
        y_max=float(session['dcsStat'][key].get('max'))
        step =float(session['dcsStat'][key].get('step'))
    elif Graph_yrange.get(key) :
        y_min=Graph_yrange[key][0]
        y_max=Graph_yrange[key][1]
        step =Graph_yrange[key][2]
    else :
        y_min=data_min
        y_max=data_max
        step =1

    g.SetYRange(y_min,y_max)
    g.SetXRange(datetime.fromtimestamp(DCS.timeRange[0]),datetime.fromtimestamp(DCS.timeRange[1]))
    g.SetXAxis_Time()
    g.SetXRegion(datetime.fromtimestamp(DCS.startTime),datetime.fromtimestamp(DCS.finishTime))
    g.Draw()

    filename=picture_DIR+key+picture_type
    c.Print(filename)

    GraphStat={ 'filename' : filename,
                'max'      : y_max,
                'min'      : y_min,
                'step'     : step
    }

    return GraphStat
"""

def dcs_plot(dcsPlotList):
    dcsPlot = {}
    keyList=[]
    for plot_key in dcsPlotList :
        for key_num in dcsPlotList[plot_key]:
            keyList.append(plot_key)

    for i_key in iv_key_list:
        PlotType = i_key["name"]
        key_v = i_key["key_v"]
        key_i = i_key["key_i"]
        key_num = i_key["num"]
        if not dcsPlotList.get(key_v) is None and not dcsPlotList.get(key_i) is None:
            for num in dcsPlotList[key_v]:
                v_dat_list = []
                i_dat_list = []
                for chip in dcsPlotList[key_v][num]:
                    v_dat_list.append(dcsPlotList[key_v][num][chip].get("dat"))
                    i_dat_list.append(dcsPlotList[key_i][num][chip].get("dat"))
                GraphStat_v = create_dcsPlot_pyPlot(num, key_v, v_dat_list)
                GraphStat_i = create_dcsPlot_pyPlot(num, key_i, i_dat_list)
                keyList.remove(key_v)
                keyList.remove(key_i)

                if GraphStat_v != None and GraphStat_i != None:
                    dcsPlot.update(
                        {
                            PlotType
                            + "-"
                            + str(num): {
                                "file_num": 2,
                                "filename": [
                                    GraphStat_v.get("filename"),
                                    GraphStat_i.get("filename"),
                                ],
                                "keyName": [key_v + "-" + str(num), key_i + "-" + str(num)],
                                "v_min": GraphStat_v.get("min"),
                                "v_max": GraphStat_v.get("max"),
                                "v_step": GraphStat_v.get("step"),
                                "i_min": GraphStat_i.get("min"),
                                "i_max": GraphStat_i.get("max"),
                                "i_step": GraphStat_i.get("step"),
                            }
                        }
                    )
    #for i_key in other_key_list:
    #        key = i_key["key"]
    #        key_num = i_key["num"]
    for key in keyList :
        if not dcsPlotList.get(key) is None:
            for num in dcsPlotList[key]:
                dat_list = []
                for chip in dcsPlotList[key][num]:
                    dat_list.append(dcsPlotList[key][num][chip].get("dat"))
                GraphStat = create_dcsPlot_pyPlot(num, key, dat_list)
                if not GraphStat is None:
                    dcsPlot.update(
                        {
                            key
                            + "-"
                            + str(num): {
                                "file_num": 1,
                                "filename": [GraphStat.get("filename")],
                                "min": GraphStat.get("min"),
                                "max": GraphStat.get("max"),
                                "step": GraphStat.get("step"),
                            }
                        }
                    )
    return dcsPlot


if __name__ == "__main__":
    dcs_plot()
